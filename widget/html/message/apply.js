define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var photoBrowser = api.require('photoBrowser');
    var type = api && api.pageParam.type;
    var Http = require('U/http');
    var apply = new Vue({
        el: '#apply',
        template: _g.getTemplate('message/apply-main-V'),
        data: {
            // list: [{
            //     senderName: '',
            //     sendTime: '',
            //     content: '',
            //     senderId: -1,
            //     id: -1,
            //     isChecked: -1,
            //     type: "0",
            //     userId: 0
            // }]
            list: []
        },
        created: function () {

        },
        methods: {
            onLineOneTap: function (index) {
                winUrl(index);
            },
            checkDetail(data) {
                _g.openWin({
                    header: {
                        data: {
                            title: data.senderName + '的详情',
                        },
                        template: 'common/header-other-V',
                    },
                    name: 'other-index',
                    url: '../other/index.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        checkUserId: data.senderId
                    }
                });
            },
            handleMessage(item, type) {
                var apply = type ? '允许' : '拒绝';
                api && api.confirm({
                    msg: '确定' + apply + '该用户的申请吗？',
                    buttons: ['确定', '取消']
                }, function (ret, err) {
                    if (ret.buttonIndex === 1) {
                        dealCheckApply(item, type);
                    }
                    // todo 付款成功后调用接口更改状态
                })

            },
            checkImg(item) {
                photoBrowser.open({
                    images: [item.sendImage],
                    placeholderImg: 'widget://res/img/apicloud.png',
                    bgColor: '#000'
                }, function (ret, err) {
                    if (ret) {
                        setTimeout(function () {
                            photoBrowser.close()
                            burnSendImage(item);
                        }, 2000);
                        if (ret.eventType === 'click') {
                            setTimeout(function () {
                                photoBrowser.close()
                                item.isBurned = 1
                            }, 2000);
                        } else {
                            // todo 调用阅后即焚接口
                        }
                    } else {
                        _g.toast('图片打开失败')
                    }
                })
            }
        },
    });

    // 发挥查看申请发送的照片
    function burnSendImage(data) {
        Http.ajax({
            data: {},
            method: 'put',
            isSync: false,
            url: '/message/burnSendImage/' + data.id,
            success: function (ret) {
                if (ret.code == 200) {
                    data.isBurned = 1
                }
            },
            error(err) {
                _g.toast('系统错误：' + ret.msg)
            }
        });
    }

    function dealCheckApply(item, type) {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                checkerId: item.senderId,
                messageId: item.id,
                isAllowed: type
            },
            isSync: false,
            url: '/message/dealCheckApply',
            success: function (ret) {
                if (ret.code == 200) {
                    getData()
                } else _g.toast('操作失败：' + ret.msg)
            },
            error(err) {
                _g.toast('系统错误：' + ret.msg)
            }
        });
    }

    // 获取个人资料接口
    function getData() {
        Http.ajax({
            data: {},
            isSync: false,
            method: 'get',
            url: '/message/findMsgByType/' + type + '?userId=' + UserInfo.id + '&page=1&size=999',
            success: function (ret) {
                if (ret.code == 200 || ret.code == '200') {
                    apply.list = ret.data
                    api && api.sendEvent({
                        name: 'message-refresh'
                    });
                }
            }
        });
    };

    getData();
    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getData();
    });

    module.exports = {};

});