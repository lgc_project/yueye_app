define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var msg = new Vue({
        el: '#msg',
        template: _g.getTemplate('message/index-main-V'),
        data: {
            checkDetail: {
                content: '',
                isChecked: 1,
                sendTime: '',
                senderName: ''
            },
            systemDetail: {
                content: '',
                isChecked: 1,
                sendTime: '',
                senderName: ''
            },
            profitDetail: {
                content: '',
                isChecked: 1,
                sendTime: '',
                senderName: ''
            },
            commentDetail: {
                content: '',
                isChecked: 1,
                sendTime: '',
                senderName: ''
            },
            sex: UserInfo.sex
        },
        created: function () {

        },
        methods: {
            onWinTap: function (param) {
                winUrl(param);
            },
        },
    });

    // 获取未读消息数
    function getUnreadMsg() {
        Http.ajax({
            data: {},
            method: 'get',
            isSync: false,
            url: '/message/findNewestMessages/' + UserInfo.id,
            success: function (ret) {
                // todo
                if (ret.code === '200' || ret.code === 200) {
                    var data = ret.data;
                    if (data['0'].newestMessage) {
                        msg.checkDetail = {
                            content: data['0'].newestMessage.content || '',
                            isChecked: data['0'].newestMessage.isChecked || 0,
                            sendTime: data['0'].newestMessage.sendTime || '',
                            senderName: data['0'].newestMessage.senderName || '',
                        }
                    }
                    if (data['1'].newestMessage) {
                        msg.systemDetail = {
                            content: data['1'].newestMessage.content || '',
                            isChecked: data['1'].newestMessage.isChecked || 0,
                            sendTime: data['1'].newestMessage.sendTime || '',
                            senderName: data['1'].newestMessage.senderName || '',
                        }
                    }
                    if (data['2'].newestMessage) {
                        msg.profitDetail = {
                            content: data['2'].newestMessage.content || '',
                            isChecked: data['2'].newestMessage.isChecked || 0,
                            sendTime: data['2'].newestMessage.sendTime || '',
                            senderName: data['2'].newestMessage.senderName || '',
                        }
                    }
                    if (data['3'].newestMessage) {
                        msg.commentDetail = {
                            content: data['3'].newestMessage.content || '',
                            isChecked: data['3'].newestMessage.isChecked || 0,
                            sendTime: data['3'].newestMessage.sendTime || '',
                            senderName: data['3'].newestMessage.senderName || '',
                        }
                    }

                }
            },
            error: function (err) {
                _g.toast('获取消息失败' + JSON.stringify(err));
            }
        })
    };
    getUnreadMsg();

    function winUrl(param) {
        switch (param) {
            case 'apply':
                openWin('查看申请', 'message-apply', 'message/apply');
                break;
            case 'msg':
                openWin('系统消息', 'message-message', 'message/message');
                break;
            case 'coming':
                openWin('收益提醒', 'message-profit', 'message/profit');
                break;
            case 'comment':
                openWin('评价通知', 'message-commnet', 'message/commnet');
                break;
        }
    };

    function openWin(_title, _name, _url) {
        var type = 0;
        switch (_title) {
            case '查看申请':
                type = 0;
                break;
            case '系统消息':
                type = 1;
                break;
            case '收益提醒':
                type = 2;
                break;
            case '评价通知':
                type = 3;
                break;
            default:
                break;
        }
        _g.openWin({
            header: {
                data: {
                    title: _title,
                },
                template: 'common/header-base-V',
            },
            name: _name,
            url: '../' + _url + '.html',
            bounces: true,
            slidBackEnabled: false,
            pageParam: {
                type: type
            }
        });
    };
    // 获取个人资料接口
    function getData() {
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: false,
            url: '/user/getUserInfo',
            success: function (ret) {
                if (ret.code == 200) {
                    // _g.rmLS('UserInfo');
                    var data = ret.data;
                    data.token = _token;
                    _g.setLS('UserInfo', data);
                    user.member = data;
                }
            }
        });
    };

    api && api.addEventListener({
        name: 'message-refresh'
    }, function (ret, err) {
        getUnreadMsg();
    });

    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getUnreadMsg();
    });
    // getInfoData();

    module.exports = {};

});