define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var type = api && api.pageParam.type;
    var Http = require('U/http');
    var message = new Vue({
        el: '#message',
        template: _g.getTemplate('message/message-main-V'),
        data: {
            list: []
        },
        created: function () {

        },
        methods: {
            onLineOneTap: function (index) {
                winUrl(index);
            },
            checkDetail(data) {
                _g.openWin({
                    header: {
                        data: {
                            title: data.senderName + '的详情',
                        },
                        template: 'common/header-other-V',
                    },
                    name: 'other-index',
                    url: '../other/index.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        checkUserId: data.userId
                    }
                });
            }
        },
    });
    // 获取个人资料接口
    function getData() {
        Http.ajax({
            data: {},
            isSync: false,
            method: 'get',
            url: '/message/findMsgByType/' + type + '?userId=' + UserInfo.id + '&page=1&size=999',
            success: function (ret) {
                if (ret.code == 200 || ret.code == '200') {
                    message.list = ret.data
                    api && api.sendEvent({
                        name: 'message-refresh'
                    })
                }
            }
        });
    };

    getData();
    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getData();
    });
    // getInfoData();

    module.exports = {};

});