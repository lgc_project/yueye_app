define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var type = api && api.pageParam.type;
    var Http = require('U/http');
    var commnet = new Vue({
        el: '#commnet',
        template: _g.getTemplate('message/commnet-main-V'),
        data: {
            list: [],
            showComment: false,
            commentList: [],
            femaleCommentList: [{
                number: 0,
                title: '漂亮',
                select: false,
            }, {
                number: 0,
                title: '身材好',
                select: false,
            }, {
                number: 0,
                title: '好玩',
                select: false,
            }, {
                number: 0,
                title: '难约',
                select: false,
            }, {
                number: 0,
                title: '要红包',
                select: false,
            }, {
                number: 0,
                title: '照骗',
                select: false,
            }, ],
            maleCommentList: [{
                number: 0,
                title: '帅气',
                select: false,
            }, {
                number: 0,
                title: '身材好',
                select: false,
            }, {
                number: 0,
                title: '好玩',
                select: false,
            }, {
                number: 0,
                title: '大方',
                select: false,
            }, {
                number: 0,
                title: '不拖拉',
                select: false,
            }, {
                number: 0,
                title: '口嗨',
                select: false,
            }, ],
            active: {},
            selectedIndex: -1,
            hasCommented: false
        },
        created: function () {

        },
        methods: {
            onLineOneTap: function (index) {
                winUrl(index);
            },
            checkDetail(data) {
                _g.openWin({
                    header: {
                        data: {
                            title: data.senderName + '的详情',
                        },
                        template: 'common/header-other-V',
                    },
                    name: 'other-index',
                    url: '../other/index.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        checkUserId: data.senderId
                    }
                });
            },
            handleComment(item) {
                this.active = item
                getComment(item);
            },
            hideComment: function () {
                this.showComment = false;
            },
            onCommentTap: function (index) {
                if (commnet.hasCommented) return
                if (this.selectedIndex === -1) {
                    this.commentList[index].select = true;
                    this.commentList[index].number++;
                    this.selectedIndex = index
                } else {
                    this.commentList[this.selectedIndex].select = false;
                    this.commentList[this.selectedIndex].number--;
                    this.commentList[index].select = true;
                    this.commentList[index].number++;
                    this.selectedIndex = index;
                }
            },
            onPostCommentTap: function () {
                if (commnet.hasCommented) return
                postComment();
            },
        },
    });
    // 获取个人资料接口
    function getData() {
        Http.ajax({
            data: {},
            isSync: false,
            method: 'get',
            url: '/message/findMsgByType/' + type + '?userId=' + UserInfo.id + '&page=1&size=999',
            success: function (ret) {
                if (ret.code == 200 || ret.code == '200') {
                    commnet.list = ret.data
                    api && api.sendEvent({
                        name: 'message-refresh'
                    })
                    commnet.commentList = UserInfo.sex ? commnet.maleCommentList : commnet.femaleCommentList
                }
            }
        });
    };

    // 提交评论
    function postComment() {
        var content = '';
        for (var i = 0; i < commnet.commentList.length; i++) {
            if (commnet.commentList[i].select) {
                if (content == '') {
                    content = commnet.commentList[i].title
                } else {
                    content = content + ',' + commnet.commentList[i].title
                }
            }
        }
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: commnet.active.senderId,
                content: content
            },
            isSync: false,
            url: '/relationship/evaluate',
            success: function (ret) {
                if (ret.code == 200) {
                    _g.toast('评论成功');
                    commnet.showComment = false;
                } else {
                    _g.toast('评论失败');
                }
            }
        });
    }

    // 是否评论过
    function jugdeCommented(item) {
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: item.senderId
            },
            isSync: false,
            url: '/relationship/evaluateContent',
            success: function (ret) {
                if (ret.code == 200) {
                    if (!ret.data.content) {
                        commnet.hasCommented = true
                    } else commnet.hasCommented = false
                }
            }
        });
    };

    function getComment(item) {
        Http.ajax({
            data: {
                userId: item.senderId
            },
            isSync: false,
            lock: false,
            url: '/relationship/evaluateList',
            success: function (ret) {
                if (ret.code == 200) {
                    var data = ret.data;
                    commnet.showComment = true;
                    for (var index in data) {
                        for (var i = 0; i < commnet.commentList.length; i++) {
                            if (index == commnet.commentList[i].title) {
                                commnet.commentList[i].select = true;
                                commnet.commentList[i].number = data[index];
                            }
                        }
                    }
                    jugdeCommented(item)
                }
            }
        });
    };

    getData();

    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getData();
    });
    // getInfoData();

    module.exports = {};

});