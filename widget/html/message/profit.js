define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var type = api && api.pageParam.type;
    var profit = new Vue({
        el: '#profit',
        template: _g.getTemplate('message/profit-main-V'),
        data: {
            list: []
        },
        created: function () {

        },
        methods: {
            onLineOneTap: function (index) {
                winUrl(index);
            },
            checkDetail(data) {
                _g.openWin({
                    header: {
                        data: {
                            title: data.senderName + '的详情',
                        },
                        template: 'common/header-other-V',
                    },
                    name: 'other-index',
                    url: '../other/index.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        checkUserId: data.senderId
                    }
                });
            }
        },
    });
    // 获取个人资料接口
    function getData() {
        Http.ajax({
            data: {},
            isSync: false,
            method: 'get',
            url: '/message/findMsgByType/' + type + '?userId=' + UserInfo.id + '&page=1&size=999',
            success: function (ret) {
                if (ret.code == 200 || ret.code == '200') {
                    profit.list = ret.data
                    api && api.sendEvent({
                        name: 'message-refresh'
                    })
                }
            }
        });
    };

    getData();
    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getInfoData();
    });
    // getInfoData();

    module.exports = {};

});