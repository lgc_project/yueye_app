define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var set = new Vue({
        el: '#set',
        template: _g.getTemplate('message/set-main-V'),
        data: {
            pass: false,
            applyCode: false,
            radio: false

        },
        created: function() {

        },
        methods: {
            onPassTap: function(event) {
                if($(event.target).closest('.ui-select').length > 0) {
                    if(this.pass == false) {
                        this.pass = true
                    } else {
                        this.pass = false
                    }
                }
            },
            onCodeTap: function(event) {
                if($(event.target).closest('.ui-select').length > 0) {
                    if(this.applyCode == false) {
                        this.applyCode = true
                    } else {
                        this.applyCode = false
                    }
                }
            },
            onRadioTap: function(event) {
                if($(event.target).closest('.ui-select').length > 0) {
                    if(this.radio == false) {
                        this.radio = true
                    } else {
                        this.radio = false
                    }
                }
            },
            onLineOneTap: function(index) {
                winUrl(index);
            }
        },
    });
    // 获取个人资料接口
    function getData() {
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: false,
            url: '/user/getUserInfo',
            success: function(ret) {
                if(ret.code == 200) {
                    // _g.rmLS('UserInfo');
                    var data = ret.data;
                    data.token = _token;
                    _g.setLS('UserInfo', data);
                    user.member = data;
                }
            }
        });
    };
    // 下拉刷新
    _g.setPullDownRefresh(function() {
        getInfoData();
    });
    // getInfoData();
    
    module.exports = {};

});
