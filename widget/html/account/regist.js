define(function (require, exports, module) {
    var winHeight = api.winHeight;
    var Http = require('U/http');
    var Vcode = require('U/vcode');
    var regist = new Vue({
        el: '#regist',
        template: _g.getTemplate('account/regist-main-V'),
        data: {
            telephone: '',
            password: '',
            checkcode: '',
            applyCodeInfo: '',
            codeText: '获取验证码',
            sex: 1,
            isAgree: false,
        },
        ready: function () {
            Vcode.init({
                onInit: this.onInit,
                onAction: this.onAction,
            });
        },
        created: function () {
            $('input').focus(function() {
                $('.ui-login__option').css('display', 'none');
            });
            $('input').blur(function() {
                $('.ui-login__option').css('display', 'block');

            });
            $(window).on('resize', function() {
                // alert(window.innerHeight);
                if (window.innerHeight == winHeight) {
                    $('.ui-login__option').css('display', 'block');
                } else {
                    $('.ui-login__option').css('display', 'none');
                }
            });
        },
        methods: {
            onInit: function(nowText) {
                this.codeText = nowText;
            },
            onAction: function(nowText) {
                this.codeText = nowText;
            },
            //  获取验证码
            onGetCodeTap: function() {
                // _g.toast('获取验证码!');
                if (Vcode.isAllow()) {
                    var mobileReg = /^1[0-9]{10}$/;
                    if(this.telephone == '') {
                        _g.toast('手机号不能为空');
                        return false;
                    }
                    if (!mobileReg.test(this.telephone)) {
                        _g.toast('请输入11位数的手机号码');
                        return
                    }
                    Vcode.start();
                    this.vcode = Vcode.getRandom(6);
                    Http.ajax({
                        data: {
                            phone: regist.telephone,
                            type: 1
                        },
                        lock: false,
                        url: '/common/sendVerfiCode',
                        success: function(ret) {
                            if (ret.code == 200) {
                                // _g.toast(ret.msg);
                            } else {
                                _g.toast(ret.msg);
                            }
                        },
                        error: function(err) {},

                    });
                } else {
                    _g.toast('请在倒计时结束再点击获取新的验证码!');
                }
            },
            onClearPhoneTap: function () {
                this.telephone = '';
                return
            },
            onClearCodeTap: function () {
                this.codeText = '';
                return
            },
            onClearPasswordTap: function () {
                this.password = '';
                return
            },
            onClearTextTap: function () {
                this.applyCodeInfo = '';
                return
            },
            onClearPasswordSubmitTap: function () {
                this.passwordSubmit = '';
                return
            },
            onSexTap: function (param) {
                this.sex = param;
            },
            onCheckTap: function() {
                this.isAgree = !this.isAgree;
            },
            onShowAgreeTap: function() {
                _g.openWin({
                    header: {
                        data: {
                            title: '服务协议'
                        },
                        template: 'common/header-me-V',
                    },
                    name: 'me-agreement',
                    url: '../me/agreement.html',
                    bounces: false,
                    slidBackEnabled: false,
                    pageParam: {

                    }
                });
            },
            onNextTap: function () {
                var mobileReg = /^1[0-9]{10}$/;
                if (this.telephone == '') {
                    _g.toast('手机不能为空');
                    return
                }
                if (this.checkcode == '') {
                    _g.toast('验证码不能为空');
                    return
                }
                if (!mobileReg.test(this.telephone)) {
                    _g.toast('请输入11位数的手机号码');
                    return
                }
                if (this.password == '') {
                    _g.toast('登录密码不能为空');
                    return
                }
                if (this.applyCodeInfo == '') {
                    _g.toast('了解悦夜来源不能为空');
                    return
                }
                if (this.password != '') {
                    var reg = /^[A-Za-z0-9]{6,16}$/;
                    isok = reg.test(this.password);
                }
                if (!isok) {
                    _g.toast('密码需6位至16位,不能含有标点或其他特殊符号');
                }
                if (!this.isAgree) {
                    _g.toast('请勾选同意服务协议');
                    return
                }
                postRegist();
            }
        },
    });
    // 注册接口
    function postRegist() {
        Http.ajax({
            data: {
                username: regist.telephone,
                password: regist.password,
                verifCode: regist.checkcode,
                sex: regist.sex,
                applyCodeInfo: regist.applyCodeInfo
            },
            isSync: true,
            url: '/user/register',
            success: function(ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    var data = ret.data;
                    data.identity = 0;
                    _g.toast('注册成功');
                    _g.setLS('UserInfo', data);
                    setTimeout(function() {
                        if(regist.sex == 0) {
                            _g.openWin({
                                header: {
                                    data: {
                                        title: '完善个人信息',
                                        rightText: '申请邀请码'
                                    },
                                    template: 'common/header-base-V',
                                },
                                name: 'account-editInfo',
                                url: '../account/editInfo.html',
                                bounces: true,
                                slidBackEnabled: false,
                                pageParam: {
                                }
                            });
                            setTimeout(function() {
                                api.closeWin();
                            }, 500);
                        } else {
                            _g.openWin({
                                header: {
                                    data: {
                                        title: '完善个人信息',
                                        rightText: '保存'
                                    },
                                    template: 'common/header-base-V',
                                },
                                name: 'account-editInfo',
                                url: '../account/editInfo.html',
                                bounces: true,
                                slidBackEnabled: false,
                                pageParam: {
                                }
                            });
                            setTimeout(function() {
                                api.closeWin();
                            }, 500);
                        }
                    }, 1000);
                } else if (ret.code == 202){
                    _g.toast(ret.msg);
                } else {
                    _g.toast(ret.codeDesc);
                }
            }
        });
    };
    // 打开主页面
    function openSexPage() {
        // 注册
        _g.openWin({
            header: {
                data: {
                    title: '性别选择',
                },
                template: 'common/header-base-V',
            },
            name: 'account-sex-win',
            url: '../account/sex.html',
            bounces: true,
            slidBackEnabled: false,
            pageParam: {
            }
        });
    };
    module.exports = {};

});