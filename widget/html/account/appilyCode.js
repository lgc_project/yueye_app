define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var appilyCode = new Vue({
        el: '#appilyCode',
        template: _g.getTemplate('account/appilyCode-main-V'),
        data: {

        },
        created: function() {
        },
        methods: {
            onRechargeTap: function() {
                _g.openWin({
                    header: {
                        data: {
                            title: '会员中心',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'me-member-win',
                    url: '../me/member.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {

                    }
                });
            },
            onAppilyCodeTap: function() {
                _g.openWin({
                    header: {
                        data: {
                            title: '完善个人信息',
                            rightText: '申请邀请码'
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'account-editInfo',
                    url: '../account/editInfo.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                    }
                });
            }
        },
    });
    module.exports = {};

});
