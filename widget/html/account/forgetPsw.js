define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var Vcode = require('U/vcode');
    var forgetPsw = new Vue({
        el: '#forgetPsw',
        template: _g.getTemplate('account/forgetPsw-main-V'),
        data: {
            telephone: '',
            password: '',
            verifCode: '',
            codeText: '获取验证码',
        },
        created: function() {

        },
        ready: function() {
            Vcode.init({
                onInit: this.onInit,
                onAction: this.onAction,
            });
        },
        methods: {
            onInit: function(nowText) {
                this.codeText = nowText;
            },
            onAction: function(nowText) {
                this.codeText = nowText;
            },
            //  获取验证码
            onGetCodeTap: function() {
                // _g.toast('获取验证码!');
                if (Vcode.isAllow()) {
                    var mobileReg = /^1[0-9]{10}$/;
                    if(this.telephone == '') {
                        _g.toast('手机号不能为空');
                        return false;
                    }
                    if (!mobileReg.test(this.telephone)) {
                        _g.toast('请输入11位数的手机号码');
                        return
                    }
                    Vcode.start();
                    this.vcode = Vcode.getRandom(6);
                    Http.ajax({
                        data: {
                            phone: forgetPsw.telephone,
                            type: 3
                        },
                        lock: false,
                        url: '/common/sendVerfiCode',
                        success: function(ret) {
                            if (ret.code == 200) {
                                // _g.toast(ret.msg);
                            } else {
                                _g.toast(ret.msg);
                            }
                        },
                        error: function(err) {},

                    });
                } else {
                    _g.toast('请在倒计时结束再点击获取新的验证码!');
                }
            },
            onBindTap: function() {
                var mobileReg = /^1[0-9]{10}$/;
                if(this.telephone == '') {
                    _g.toast('手机号不能为空');
                    return false;
                }
                if (!mobileReg.test(this.telephone)) {
                    _g.toast('请输入11位数的手机号码');
                    return
                }
                if(this.checkcode == '') {
                    _g.toast('验证码不能为空');
                    return false;
                }
                if(this.password == '') {
                    _g.toast('新密码不能为空');
                    return false;
                }
                if (this.password != '') {
                    var reg = /^[A-Za-z0-9]{6,16}$/;
                    isok = reg.test(this.password);
                }
                if (!isok) {
                    _g.toast('请输入6~16位数字/字母组合的密码');
                    return
                }
                postData();
            }
        },
    });
    // 提交绑定手机号码接口
    function postData() {
        Http.ajax({
            data: {
                phone: forgetPsw.telephone,
                newPassword: forgetPsw.password,
                verifCode: forgetPsw.verifCode,
            },
            isSync: true,
            url: '/user/forgetPassword',
            success: function(ret) {
                if (ret.code == 200) {
                    _g.toast('修改成功');
                    setTimeout(function() {
                        api && api.closeWin();
                    }, 2000);
                } else {
                    _g.toast(ret.msg);
                }
            }
        });
    };
    
    module.exports = {};

});
