define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var verificate = new Vue({
        el: '#verificate',
        template: _g.getTemplate('account/verificate-main-V'),
        data: {
            inviteCode: ""
        },
        created: function () {},
        methods: {
            onServiceTap: function() {
                _g.openWin({
                    header: {
                        data: {
                            title: '客服',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'me-service-win',
                    url: '../me/service.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {

                    }
                });
            },
            handleSave: function() {
                if (!this.inviteCode) {
                    _g.toast('邀请码不能为空！');
                    return;
                }
                this.inviteCode = this.inviteCode.replace(/^\s*|\s*$/g,"");
                Http.ajax({
                    data: {
                        userId: UserInfo.id,
                        inviteCode: verificate.inviteCode
                    },
                    isSync: false,
                    url: '/inviteCode/activateCode',
                    success: function (ret) {
                        if (ret.code == 200) {
                            _g.toast("验证成功，请重新登录！")
                            setTimeout(function () {
                                api.closeWin();
                            }, 1000);
                        } else if (ret.code == 201) {
                            _g.toast("验证失败")
                        } else if (ret.code == 202) {
                            _g.toast("邀请码不存在，请联系客服")
                        } else if (ret.code == 203) {
                            _g.toast("邀请码已经使用，不能重复使用")
                        }
                    }
                });
            },
            handleCancel: function() {
                _g.openWin({
                    header: {
                        data: {
                            title: '会员中心',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'me-member',
                    url: '../me/member.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {

                    }
                });
            }
        },
    });

    module.exports = {};
});