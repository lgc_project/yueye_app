define(function(require, exports, module) {
    var winHeight = api.winHeight;
    var Http = require('U/http');
    var bMap = api.require('bMap');
    var deviceId = api.deviceId;  // 设备唯一标识，字符串类型
    var systemType = api.systemType; // 系统类型，字符串类型
    var UserLocation = '';
    var isPass = 1;
    var IOS_CHECK = _g.getLS('IOS_CHECK');
    var login = new Vue({
        el: '#login',
        template: _g.getTemplate('account/login-main-V'),
        data: {
            telephone: '',
            password: '',
            lon: 0,
            lat: 0,
            openGPS: false,
        },
        created: function() {
            api.setStatusBarStyle({
                style: 'dark',
                color: 'rgba(0,0,0,0.0)'
            });
            $('input').focus(function() {
                $('.ui-login__option').css('display', 'none');
            });
            $('input').blur(function() {
                $('.ui-login__option').css('display', 'block');

            });
            $(window).on('resize', function() {
                // alert(window.innerHeight);
                if (window.innerHeight == winHeight) {
                    $('.ui-login__option').css('display', 'block');
                } else {
                    $('.ui-login__option').css('display', 'none');
                }
            });
        },
        methods: {
            onDelPhoneTap: function() {
                this.telephone = '';
                this.password = '';
                return false;
            },
            onDelTap: function() {
                this.password = '';
                return false;
            },
            onCodeTap: function() {
                _g.openWin({
                    header: {
                        data: {
                            title: '邀请码验证'
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'account-verificate',
                    url: '../account/verificate.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                    }
                });
            },
            onLoginTap: function() {
                var mobileReg = /^1[0-9]{10}$/;
                if(this.telephone == '') {
                    _g.toast('手机号不能为空');
                    return false;
                }
                this.telephone = this.telephone.replace(/^\s*|\s*$/g,"");
                if (!mobileReg.test(this.telephone)) {
                    _g.toast('请输入11位数的手机号码');
                    return
                }
                if(this.password == '') {
                    _g.toast('密码不能为空');
                    return false;
                }
                this.password = this.password.replace(/^\s*|\s*$/g,"");
                if (this.password != '') {
                    var reg = /^[A-Za-z0-9]{6,16}$/;
                    isok = reg.test(this.password);
                }
                if (!isok) {
                    _g.toast('请输入6~16位数字/字母组合的密码');
                    return
                }
                // postLoginData();
                bMap.getLocationServices(function(ret, err) {
                    // alert(_g.j2s(ret));
                    if (ret.enable) {
                        if(login.openGPS) {
                            if(UserLocation.lon > 73 && UserLocation.lon < 135 && UserLocation.lat > 4.25 && UserLocation.lat < 53.5) {
                                // 在中国境内
                                isPass = 1;
                                postLoginData();
                            } else {
                                // 不在中国境内
                                isPass = 0;
                                postLoginData();
                            }
                        } else {
                            // 未开启GPS定位功能
                            isPass = 0;
                            postLoginData();
                        }
                    } else {
                        // 未授权定位功能
                        isPass = 0;
                        postLoginData();
                    }
                });
            },
            onRegistTap: function() {
                _g.openWin({
                    header: {
                        data: {
                            title: '手机号码注册',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'account-regist',
                    url: '../account/regist.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {

                    }
                });
            },
            // 忘记密码
            onForgetPswTap: function() {
                _g.openWin({
                    header: {
                        data: {
                            title: '忘记密码',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'account-forgetPsw',
                    url: '../account/forgetPsw.html',
                    bounces: false,
                    slidBackEnabled: false
                });
            },
            onThirdTap: function(type) {
                if (type == 'weixin') {
                    _g.toast('微信登录');
                    // console.log('当前设备未安装微信客户端');
                    return
                }
                if (type == 'weibo') {
                    _g.toast('微博登录');
                    // console.log('未安装新浪微博客户端');
                    return
                }
                if (type == 'qq') {
                    _g.toast('QQ登录');
                    console.log('未安装QQ客户端');
                    return
                }
            }
        },
    });
    // 登录请求
    function postLoginData() {
        _g.closeWins(['main-night-win', 'main-index-win']);
        _g.rmLS('UserInfo');
        _g.rmLS('peopleCity');
        Http.ajax({
            data: {
                username: login.telephone,
                password: login.password,
                deviceCode: deviceId,
            },
            isSync: true,
            url: '/user/login',
            success: function(ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    var data = ret.data;
                    data.deviceId = deviceId;
                    _g.setLS('UserInfo', data);
                    var UserInfo = data;
                    if(UserInfo.nickname == '') {
                        // _g.rmLS('UserInfo');
                        var rightText = '';
                        if(UserInfo.sex == 0) {
                            rightText = '申请邀请码';
                        } else if(UserInfo.sex == 1) {
                            rightText = '保存';
                        }
                        _g.openWin({
                            header: {
                                data: {
                                    title: '完善个人信息',
                                    rightText: rightText
                                },
                                template: 'common/header-base-V',
                            },
                            name: 'account-editInfo',
                            url: '../account/editInfo.html',
                            bounces: true,
                            slidBackEnabled: false,
                            pageParam: {
                            }
                        });
                    } else if(UserInfo.sex == 0 && UserInfo.identity == 0) {
                        _g.openWin({
                            header: {
                                data: {
                                    title: '邀请码验证'
                                },
                                template: 'common/header-base-V',
                            },
                            name: 'account-verificate',
                            url: '../account/verificate.html',
                            bounces: true,
                            slidBackEnabled: false,
                            pageParam: {
                            }
                        });
                    } else {
                        _g.toast('登录成功');
                        setTimeout(function() {
                            if(systemType == 'ios') {
                                if(isPass == 1) {
                                    if(IOS_CHECK == 1) {
                                        openNightPage();
                                    } else if(login.telephone == 18578790007) {
                                        openNightPage();
                                    } else {
                                        openMainPage();
                                    }
                                } else {
                                    openNightPage();
                                }
                            } else {
                                // 安卓登录
                                openMainPage();
                            }
                        }, 1000);
                    }
                } else {
                    _g.toast(ret.msg);
                }
            }
        });
    };
    // 打开主页面
    function openMainPage() {
        _g.setLS('noGPS', 0);
        api.setStatusBarStyle({
            style : 'light',
            color : 'rgba(0,0,0,0.0)'
        });
        api.openWin({
            name: 'main-index-win',
            url: '../main/index.html',
            bounces: false,
            slidBackEnabled: false,
            animation: { type: 'none' }
        });
        setTimeout(function () {
            api.closeWin();
        }, 1000);
    };
    // 不开定位打开星座页面
    function openNightPage() {
        _g.setLS('noGPS', 1);
        api.setStatusBarStyle({
            style : 'light',
            color : 'rgba(0,0,0,0.0)'
        });
        api && api.openWin({
            name: 'main-night-win',
            url: '../main/night.html',
            bounces: false,
            slidBackEnabled: false,
            bgColor: '#eee',
            animation: { type: 'none' }
        });
        setTimeout(function () {
            api.closeWin();
        }, 1000);
    };
    // 百度地图获取定位
    function checkBmap() {
        var data = {
            province: '广东省',
            city: '广州市',
        }
        _g.setLS('UserLocation', data);
        setTimeout(function() {
            bMap.getLocationServices(function(ret, err) {
                // alert(JSON.stringify(ret));
                if (ret.enable) {
                    if(systemType == 'ios') {
                        bMap.initMapSDK(function(ret) {
                            if (ret.status) {
                                getLocation();
                            }
                        });
                    } else {
                        getLocation();
                    }

                } else {
                    login.openGPS = false;
                    isPass = 0;
                    // alert('授权定位咯');
                }
            });
        }, 0);
    };
    
    function getLocation() {
        bMap.getLocation({
            accuracy: '100m',
            autoStop: true,
            filter: 1
        }, function(ret, err) {
            if (ret.status) {
                // alert(JSON.stringify(ret));
                var lon = ret.lon.toFixed(6);
                var lat = ret.lat.toFixed(6);
                setTimeout(function() {
                    bMap.getNameFromCoords({
                        lon: lon,
                        lat: lat
                    }, function(ret, err) {
                        login.openGPS = true;
                        // alert(JSON.stringify(ret));
                        if (ret.status) {
                            isPass = 1;
                            var data = {
                                lon: ret.lon,
                                lat: ret.lat,
                                province: ret.province,
                                city: ret.city,
                                district: ret.district
                            }
                            UserLocation = data;
                            _g.setLS('UserLocation', data);
                            // alert(JSON.stringify(ret));
                        }
                    });
                }, 0);
            } else {
                login.openGPS = false;
                isPass = 0;
                // alert('开启GPS定位咯');
                // getLocation();
                // alert(err.code);
            }
        });
    }
    checkBmap();
    // 监听安卓返回按钮事件
    api && api.addEventListener({
        name: 'keyback'
    }, function(ret, err) {
        api.closeWidget();
    });
    module.exports = {};

});
