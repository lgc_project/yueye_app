define(function(require, exports, module) {
    var state =api && api.pageParam.state;
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var changePsw = new Vue({
        el: '#changePsw',
        template: _g.getTemplate('account/changePsw-main-V'),
        data: {
            avatar: '../../image/account/logo-vrc.png',
            nickname: '腹黑到爆的怪大叔',
            userIntro: '这家伙很懒，什么都没有留下',
            telephone: '',
            password: '',
            inputNewPwd: '',
            confirmNewPwd: '',
            state: '',
        },
        created: function() {
            this.nickname = UserInfo.nickName;
            this.telephone = UserInfo.phone;
            this.state = state;
        },
        methods: {
            onConfirmChangeTap: function() {
                if (this.password == '') {
                    _g.toast('旧密码不能为空');
                    return
                }
                if (this.inputNewPwd == '') {
                    _g.toast('新密码不能为空');
                    return
                }
                if(state == 'loginPsw') {
                    if (this.inputNewPwd != '') {
                        var reg = /^[A-Za-z0-9]{6,16}$/;
                        isok = reg.test(this.inputNewPwd);
                    }
                    if (!isok) {
                        _g.toast('密码格式不正确,请输入6到16位数字或字母组合的密码!');

                    }
                } else if(state == 'payPsw') {
                    if (this.inputNewPwd != '') {
                        var reg = /^[0-9]{6}$/;
                        isok = reg.test(this.inputNewPwd);
                    }
                    if (!isok) {
                        _g.toast('密码格式不正确,请输入6位数字的密码!');

                    }
                }

                if (this.confirmNewPwd != this.inputNewPwd) {
                    _g.toast('确认密码与新密码不一致')
                    return
                }
                postPassword();
            }
        },
    });
    // 修改登录密码
    function postPassword() {
        var _url = '';
        if(state == 'loginPsw') {
            _url = '/user/changeLoginPass';
        } else if(state == 'payPsw') {
            _url = '/user/changeTradePass';
        }
        Http.ajax({
            data: {
                userId: UserInfo.id,
                oldPass: changePsw.password,
                newPass: changePsw.inputNewPwd,
            },
            isSync: true,
            url: _url,
            success: function(ret) {
                _g.toast(ret.codeDesc);
                if(ret.code == 200 && state == 'loginPsw') {
                    _g.rmLS('UserInfo');
                    api && api.openWin({
                        name: 'account-login-win',
                        url: '../account/login.html',
                        pageParam: {
                            from: 'root'
                        },
                        bounces: false,
                        slidBackEnabled: false,
                        animation: {
                            type: 'none'
                        }
                    });
                    setTimeout(function() {
                        _g.closeWins(['main-index-win','account-changePsw-win'])
                    }, 1000);
                }
                if(ret.code == 200 && state == 'payPsw') {
                    api && api.sendEvent({
                        name: 'work-index-changeInfo',
                        extra: {

                        }
                    });
                }
            }
        });
    };
    module.exports = {};

});
