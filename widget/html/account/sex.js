define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var sex = new Vue({
        el: '#sex',
        template: _g.getTemplate('account/sex-main-V'),
        data: {
            sex: 1
        },
        ready: function () {

        },
        created: function () {

        },
        methods: {
            onSexTap: function (param) {
                this.sex = param;
            },
            onNextTap: function () {
                Http.ajax({
                    data: {
                        userId: UserInfo.id,
                        sex: sex.sex,
                    },
                    isSync: true,
                    url: '/user/updateUserSex',
                    success: function (ret) {
                        if (ret.code == 200) {
                            var data = ret.data;
                            _g.toast('注册成功');
                            if(sex.sex == 0) {
                                UserInfo.sex = 0;
                                _g.setLS('UserInfo', UserInfo);
                                _g.openWin({
                                    header: {
                                        data: {
                                            title: '完善个人信息',
                                            rightText: '申请邀请码'
                                        },
                                        template: 'common/header-base-V',
                                    },
                                    name: 'account-editInfo',
                                    url: '../account/editInfo.html',
                                    bounces: true,
                                    slidBackEnabled: false,
                                    pageParam: {
                                    }
                                });
                            } else {
                                setTimeout(function() {
                                    openMainPage();
                                }, 0);
                            }
                        } else {
                            _g.toast(ret.msg);
                        }
                    }
                });
            },
        },
    });
    // 打开主页面
    function openMainPage() {
        api.setStatusBarStyle({
            style : 'light',
            color : 'rgba(0,0,0,0.0)'
        });
        api.openWin({
            name: 'main-index-win',
            url: '../main/index.html',
            bounces: false,
            slidBackEnabled: false,
            animation: { type: 'none' }
        });
    };
    module.exports = {};

});