define(function (require, exports, module) {
    var Http = require('U/http');
    var UserInfo = _g.getLS('UserInfo');
    var UIActionSelector = api.require('UIActionSelector');
    var citySelector = api.require('citySelector');
    var UIMultiSelector = api.require('UIMultiSelector');
    var editInfo = new Vue({
        el: '#editInfo',
        template: _g.getTemplate('account/editInfo-main-V'),
        data: {
            avatar: UserInfo.avatar || '../../image/account/avatar.png',
            nickname: UserInfo.nickname || '',
            province: UserInfo.province || '',
            city: UserInfo.city || '',
            county: '',
            sex: UserInfo.sex || 0,
            age: UserInfo.age || '',
            occupation: UserInfo.occupation || '',
            height: UserInfo.height || '',
            weight: UserInfo.weight || '',
            style: UserInfo.style || '',
            bust: UserInfo.bust,
            cupSize: UserInfo.cupSize || '',
            language: UserInfo.language || '',
            relationship: UserInfo.relationship || '',
            datingProgram: UserInfo.datingProgram || '',
            datingCodition: UserInfo.datingCodition || '',
            datingMoney: UserInfo.datingMoney || 1,
            wechat: UserInfo.wechat || '',
            qq: UserInfo.qq || '',
            tapList: [{
                title: '大长腿',
                selected: false,
                belong: 3
            }, {
                title: '身材棒',
                selected: false,
                belong: 1
            }, {
                title: '厨艺好',
                selected: false,
                belong: 0
            }, {
                title: '开心果',
                selected: false,
                belong: 1
            }, {
                title: '腹肌',
                selected: false,
                belong: 0
            }, {
                title: '土豪',
                selected: false,
                belong: 0
            }, {
                title: '萌萌哒',
                selected: false,
                belong: 1
            }, {
                title: '大叔',
                selected: false,
                belong: 0
            }, {
                title: '小鲜肉',
                selected: false,
                belong: 0
            }, {
                title: '游戏大神',
                selected: false,
                belong: 0
            }, {
                title: 'Cosplay',
                selected: false,
                belong: 1
            }, {
                title: '丝袜',
                selected: false,
                belong: 1
            }, {
                title: '素颜',
                selected: false,
                belong: 1
            }, {
                title: '酒神',
                selected: false,
                belong: 3
            }, {
                title: '白富美',
                selected: false,
                belong: 1
            }, {
                title: '高富帅',
                selected: false,
                belong: 0
            }, {
                title: '走肾不走心',
                selected: false,
                belong: 3
            }, ],
            occupationList: ['老板', '主播', '白领OL', '学生', '设计师', '程序员']
        },
        ready: function () {
            if (UserInfo) {
                this.sex = UserInfo.sex;
            }
        },
        created: function () {
            // this.sex = UserInfo.sex;
            // this.nickname = UserInfo.nickname || '';
            // this.province = UserInfo.province || '';
            // this.city = UserInfo.city || '';
            // this.occupation = UserInfo.occupation || '';
            // this.age = UserInfo.age || '';
            // this.avatar = UserInfo.avatar || '../../image/account/avatar.png'
            var list = UserInfo.introduce && UserInfo.introduce.split(',') || [];
            for (var i = 0; i < list.length; i++) {
                for (var j = 0; j < this.tapList.length; j++) {
                    if (list[i] == this.tapList[j].title) {
                        this.tapList[j].selected = true
                    }
                }
            }
        },
        watch: {
            tapList: function (val, old) {
                alert(JSON.stringify(val));
            }
        },
        methods: {
            onListTap: function (index) {
                var n = 1;
                for (var i = 0; i < this.tapList.length; i++) {
                    if (this.tapList[i].selected) {
                        n++;
                    }
                }
                if (n > 5 && this.tapList[index].selected == false) {
                    _g.toast('最多只能选中5个哦');
                    return;
                }
                this.tapList[index].selected = !this.tapList[index].selected
            },
            onAvatarTap: function () {
                _g.openPicActionSheet({
                    allowEdit: true,
                    suc: function (ret) {
                        postAvatar(ret.data);
                    }
                });
            },
            onCityTap: function () {
                // citySelector.open({
                //     y: api.frameHeight - 270,
                //     height: 270,
                //     // titleImg: 'widget://res/cityTitle.png',
                //     // cancelImg: 'widget://res/cityTitle.png',
                //     // enterImg: 'widget://res/cityTitle.png',
                //     fixedOn: api.frameName,
                //     fontColor: '#333333',
                //     selectedColor: '#ea39b9',
                //     anim: true,
                // }, function(ret) {
                //     if (ret) {
                //         // alert(JSON.stringify(ret));
                //         editInfo.province = ret.province;
                //         editInfo.city = ret.city;
                //         editInfo.county = ret.county;
                //     } 
                // });
                UIActionSelector.open({
                    datas: 'widget://res/Regions.json', //拿到APP原本省市县数据
                    layout: {
                        row: 5,
                        col: 2,
                        height: 32,
                        size: 12,
                        sizeActive: 14,
                        rowSpacing: 5,
                        colSpacing: 10,
                        maskBg: 'rgba(0,0,0,0.2)',
                        bg: '#fff',
                        color: '#888',
                        colorActive: '#ea39b9',
                        colorSelected: '#ea39b9'
                    },
                    animation: true,
                    cancel: {
                        text: '取消',
                        size: 12,
                        w: 90,
                        h: 35,
                        bg: '#ccc',
                        bgActive: '#ccc',
                        color: '#fff',
                        colorActive: '#fff'
                    },
                    ok: {
                        text: '确定',
                        size: 12,
                        w: 90,
                        h: 35,
                        bg: '#571A92',
                        bgActive: '#ccc',
                        color: '#fff',
                        colorActive: '#fff'
                    },
                    title: {
                        text: '请选择省市',
                        size: 12,
                        h: 53,
                        bg: '#eee',
                        color: '#571A92'
                    },
                }, function (ret, err) {
                    if (ret && ret.eventType == 'ok') {
                        editInfo.province = ret.level1;
                        editInfo.city = ret.level2;
                    } else {
                        UIActionSelector.close();
                    }
                    if (err) {
                        _g.toast('系统出错,请稍后再试!');
                    }
                    UIActionSelector.close();
                });
            },
            onOccupationListTap() {
                api && api.actionSheet({
                    title: '职业选择',
                    buttons: editInfo.occupationList
                }, function (ret, err) {
                    editInfo.occupation = editInfo.occupationList[ret.buttonIndex - 1]
                })
            },
            onCupSizeTap() {
                var cupSizeList = ['A', 'B', 'C', 'D', 'E+']
                api && api.actionSheet({
                    title: '罩杯',
                    buttons: cupSizeList
                }, function (ret, err) {
                    editInfo.cupSize = cupSizeList[ret.buttonIndex - 1]
                })
            },
            onStyleTap() {
                UIMultiSelector.open({
                    rect: {
                        h: 244
                    },
                    text: {
                        title: '打扮风格',
                        leftBtn: '取消',
                        rightBtn: '确定',
                        selectAll: 'ALL'
                    },
                    max: 3,
                    singleSelection: false,
                    styles: {
                        bg: '#fff',
                        mask: 'rgba(0,0,0,0.2)',
                        title: {
                            bg: '#eee',
                            color: '#571A92',
                            size: 12,
                            h: 44
                        },
                        leftButton: {
                            w: 80,
                            h: 35,
                            marginT: 5,
                            marginL: 8,
                            color: '#fff',
                            bg: '#ccc',
                            size: 12
                        },
                        rightButton: {
                            w: 80,
                            h: 35,
                            marginT: 5,
                            marginR: 8,
                            color: '#fff',
                            bg: '#571A92',
                            size: 12
                        },
                        item: {
                            h: 44,
                            bg: '#fff',
                            bgActive: '#571A92',
                            bgHighlight: '#571A92',
                            color: '#888',
                            active: '#fff',
                            highlight: '#fff',
                            size: 14,
                            lineColor: 'rgb(78,57,255)',
                            textAlign: 'center'
                        },
                        icon: {
                            w: 0,
                            h: 0,
                            marginT: 11,
                            marginH: 8,
                            bg: '#fff',
                            align: 'center'
                        }
                    },
                    maskClose: true,
                    animation: true,
                    items: [{
                        text: '黑丝',
                        status: 'normal'
                    }, {
                        text: '长筒靴',
                        status: 'normal'
                    }, {
                        text: '吊带袜',
                        status: 'normal'
                    }, {
                        text: '蕾丝',
                        status: 'normal'
                    }, {
                        text: '超短裙',
                        status: 'normal'
                    }, {
                        text: '牛仔裤',
                        status: 'normal'
                    }]
                }, function (ret, err) {
                    if (ret && ret.eventType === 'clickRight') {
                        var res = []
                        if (ret.items.length) {
                            ret.items.forEach(function (ele) {
                                if (ele.status === 'selected') {
                                    res.push(ele.text)
                                }
                            })
                            editInfo.style = res.join(',')
                            UIMultiSelector.close();
                        }
                    } else if (ret && ret.eventType === 'clickLeft') UIMultiSelector.close();

                });
            },
            onLangTap() {
                UIMultiSelector.open({
                    rect: {
                        h: 244
                    },
                    text: {
                        title: '语言',
                        leftBtn: '取消',
                        rightBtn: '确定',
                        selectAll: 'ALL'
                    },
                    max: 0,
                    singleSelection: false,
                    styles: {
                        bg: '#fff',
                        mask: 'rgba(0,0,0,0.2)',
                        title: {
                            bg: '#eee',
                            color: '#571A92',
                            size: 12,
                            h: 44
                        },
                        leftButton: {
                            w: 80,
                            h: 35,
                            marginT: 5,
                            marginL: 8,
                            color: '#fff',
                            bg: '#ccc',
                            size: 12
                        },
                        rightButton: {
                            w: 80,
                            h: 35,
                            marginT: 5,
                            marginR: 8,
                            color: '#fff',
                            bg: '#571A92',
                            size: 12
                        },
                        item: {
                            h: 44,
                            bg: '#fff',
                            bgActive: '#571A92',
                            bgHighlight: '#571A92',
                            color: '#888',
                            active: '#fff',
                            highlight: '#fff',
                            size: 14,
                            lineColor: 'rgb(78,57,255)',
                            textAlign: 'center'
                        },
                        icon: {
                            w: 0,
                            h: 0,
                            marginT: 11,
                            marginH: 8,
                            bg: '#fff',
                            align: 'center'
                        }
                    },
                    maskClose: true,
                    animation: true,
                    items: [{
                        text: '方言',
                        status: 'normal'
                    }, {
                        text: '普通话',
                        status: 'normal'
                    }, {
                        text: '粤语',
                        status: 'normal'
                    }, {
                        text: '英语',
                        status: 'normal'
                    }, {
                        text: '日语',
                        status: 'normal'
                    }]
                }, function (ret, err) {
                    if (ret && ret.eventType === 'clickRight') {
                        var res = []
                        if (ret.items.length) {
                            ret.items.forEach(function (ele) {
                                if (ele.status === 'selected') {
                                    res.push(ele.text)
                                }
                            })
                            editInfo.language = res.join(',')
                            UIMultiSelector.close();
                        }
                    } else if (ret && ret.eventType === 'clickLeft') UIMultiSelector.close();
                });
            },
            onRelationshipTap() {
                var relationshipList = ['单身', '有男朋友', '已婚', '离异', '求包养']
                api && api.actionSheet({
                    title: '感情',
                    buttons: relationshipList
                }, function (ret, err) {
                    editInfo.relationship = relationshipList[ret.buttonIndex - 1]
                })
            },
            onDatingProgramTap() {
                UIMultiSelector.open({
                    rect: {
                        h: 244
                    },
                    text: {
                        title: '约会节目',
                        leftBtn: '取消',
                        rightBtn: '确定',
                        selectAll: 'ALL'
                    },
                    max: 0,
                    singleSelection: false,
                    styles: {
                        bg: '#fff',
                        mask: 'rgba(0,0,0,0.2)',
                        title: {
                            bg: '#eee',
                            color: '#571A92',
                            size: 12,
                            h: 44
                        },
                        leftButton: {
                            w: 80,
                            h: 35,
                            marginT: 5,
                            marginL: 8,
                            color: '#fff',
                            bg: '#ccc',
                            size: 12
                        },
                        rightButton: {
                            w: 80,
                            h: 35,
                            marginT: 5,
                            marginR: 8,
                            color: '#fff',
                            bg: '#571A92',
                            size: 12
                        },
                        item: {
                            h: 44,
                            bg: '#fff',
                            bgActive: '#571A92',
                            bgHighlight: '#571A92',
                            color: '#888',
                            active: '#fff',
                            highlight: '#fff',
                            size: 14,
                            lineColor: 'rgb(78,57,255)',
                            textAlign: 'center'
                        },
                        icon: {
                            w: 0,
                            h: 0,
                            marginT: 11,
                            marginH: 8,
                            bg: '#fff',
                            align: 'center'
                        }
                    },
                    maskClose: true,
                    animation: true,
                    items: [{
                        text: 'KTV',
                        status: 'normal'
                    }, {
                        text: '酒吧',
                        status: 'normal'
                    }, {
                        text: '逛街',
                        status: 'normal'
                    }, {
                        text: '电影',
                        status: 'normal'
                    }, {
                        text: '吃饭',
                        status: 'normal'
                    }, {
                        text: '运动',
                        status: 'normal'
                    }, {
                        text: '其他',
                        status: 'normal'
                    }]
                }, function (ret, err) {
                    if (ret && ret.eventType === 'clickRight') {
                        var res = []
                        if (ret.items.length) {
                            ret.items.forEach(function (ele) {
                                if (ele.status === 'selected') {
                                    res.push(ele.text)
                                }
                            })
                            editInfo.datingProgram = res.join(',')
                            UIMultiSelector.close();
                        }
                    } else if (ret && ret.eventType === 'clickLeft') UIMultiSelector.close();
                });
            },
            onDatingConditionTap() {
                var bustList = ['跟钱没关系', '收费约会']
                api && api.actionSheet({
                    title: '约会条件',
                    buttons: bustList
                }, function (ret, err) {
                    if (ret.buttonIndex >= 1 || ret.buttonIndex <= 2) editInfo.datingCodition = bustList[ret.buttonIndex - 1]
                })
            }
        },
    });

    function postAvatar(img) {
        api.ajax({
            url: CONFIG.HOST + '/common/uploadPhoto',
            method: 'post',
            data: {
                values: {
                    token: UserInfo.token
                },
                files: {
                    image: img,
                }
            }
        }, function (ret, err) {
            if (ret.code == 200) {
                editInfo.avatar = ret.data;
                // _g.toast('头像上传成功');
            } else {
                _g.toast(ret.codeDesc);
            }
        });
    };

    function updateInfo() {
        var nameReg = /^[A-Za-z0-9\u4e00-\u9fa5]+$/
        var tapList = editInfo.tapList;
        var list = [];
        tapList.forEach(function (item) {
            if (item.selected) list.push(item.title)
        });
        if(!nameReg.test(editInfo.nickname)) {
            _g.toast('昵称只允许输入中文，字母和数字！');
            return
        }
        if (editInfo.avatar == '../../image/account/avatar.png') {
            _g.toast('请上传头像');
            return;
        }
        if (editInfo.nickname == '') {
            _g.toast('用户名不能为空');
            return;
        }
        if (editInfo.wechat == '' && editInfo.qq == '') {
            _g.toast('至少填写一种联系方式哦');
            return;
        }

        if (editInfo.province == '') {
            editInfo.province = '广东省';
            editInfo.city = '广州市';
            // _g.toast('城市不能为空');
            // return;
        }
        if (editInfo.age == '') {
            if(UserInfo.sex == 1) {
                editInfo.age = '18';
            } else {
                editInfo.age = '22';
            }
            // _g.toast('年龄不能为空');
            // return;
        }
        if (editInfo.height == '') {
            if(UserInfo.sex == 1) {
                editInfo.height = '165';
            } else {
                editInfo.height = '170';
            }
            // _g.toast('身高不能为空');
            // return;
        }
        if (editInfo.weight == '') {
            if(UserInfo.sex == 1) {
                editInfo.weight = '45';
            } else {
                editInfo.weight = '55';
            }
            // _g.toast('体重不能为空');
            // return;
        }
        if (UserInfo.sex == 1 && editInfo.bust == '') {
            editInfo.bust = '32';
            // _g.toast('请输入您的胸围');
            // return;
        }
        if (UserInfo.sex == 1 && editInfo.cupSize == '') {
            editInfo.cupSize = 'B';
            // _g.toast('请输入您的罩杯');
            // return;
        }
        if (UserInfo.sex == 0 && editInfo.occupation == '') {
            editInfo.occupation = '老板';
            // _g.toast('职业不能为空');
            // return;
        }


        // if (editInfo.height > 300 || editInfo.height < 80) {
        //     _g.toast('身高数值异常！');
        //     return;
        // }
        // if (editInfo.weight > 300 || editInfo.weight < 35) {
        //     _g.toast('体重数值异常！');
        //     return;
        // }
        // if (list.join(',').length == 0) {
        //     _g.toast('个人简介不能为空');
        //     return;
        // }
        // if (UserInfo.sex == 1 && editInfo.bust == '') {
        //     _g.toast('请输入您的胸围');
        //     return;
        // }
        // if (UserInfo.sex == 1 && (editInfo.bust < 160 && editInfo.bust > 60)) {
        //     _g.toast('请输入正确的胸围(60-160)');
        //     return;
        // }
        // if (UserInfo.sex == 1 && editInfo.style == '') {
        //     _g.toast('请选择您的打扮风格');
        //     return;
        // }
        // if (UserInfo.sex == 1 && editInfo.language == '') {
        //     _g.toast('请选择您的语言');
        //     return;
        // }
        // if (UserInfo.sex == 1 && editInfo.relationship == '') {
        //     _g.toast('情感不能为空');
        //     return;
        // }
        // if (UserInfo.sex == 1 && editInfo.datingProgram == '') {
        //     _g.toast('约会节目不能为空');
        //     return;
        // }
        // if (UserInfo.sex == 1 && editInfo.datingCodition == '') {
        //     _g.toast('约会条件不能为空');
        //     return;
        // }
        var data = {
            id: UserInfo.id,
            // sex: editInfo.sex,
            nickname: editInfo.nickname,
            province: editInfo.province,
            city: editInfo.city,
            occupation: editInfo.occupation,
            age: editInfo.age,
            height: editInfo.height,
            weight: editInfo.weight,
            introduce: list != [] ? list.join(',') : '',
            avatar: editInfo.avatar,
            wechat: editInfo.wechat,
            qq: editInfo.qq
        };
        if (UserInfo.sex == 1) {
            data.bust = editInfo.bust;
            data.cupSize = editInfo.cupSize;
            data.language = editInfo.language;
            data.style = editInfo.style;
            data.relationship = editInfo.relationship;
            data.datingProgram = editInfo.datingProgram;
            data.datingCodition = editInfo.datingCodition;
            data.datingMoney = editInfo.datingMoney;
        }
        Http.ajax({
            data: data,
            isSync: true,
            url: '/user/updateUserInfo',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    api.sendEvent({
                        name: 'me-index-editInfo',
                    });
                    if (UserInfo.sex == 0 && UserInfo.identity == 0) {
                        applyCode();
                    } else {
                        _g.toast('个人信息保存成功');
                        setTimeout(function () {
                            _g.closeWins(['account-regist-win', 'account-editInfo-win']);
                        }, 1000);
                    }
                } else {
                    _g.toast(ret.codeDesc);
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    };
    // 申请邀请码
    function applyCode() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                type: 1
            },
            isSync: false,
            url: '/inviteCode/applyCode',
            success: function (ret) {
                if (ret.code == 200) {
                    _g.toast('提交成功，请耐性等候客服人员审批');
                    setTimeout(function () {
                        _g.closeWins(['account-regist-win', 'account-appilyCode-win', 'account-editInfo-win']);
                    }, 1000);
                } else {
                    _g.toast('提交失败');
                }
            }
        });
    }

    api && api.addEventListener({
        name: 'account-editInfo-save',
    }, function (ret, err) {
        updateInfo();
        // api.setStatusBarStyle({
        //     style: 'light',
        //     color: 'rgba(0,0,0,0.0)'
        // });
        // api.openWin({
        //     name: 'main-index-win',
        //     url: '../main/index.html',
        //     bounces: false,
        //     slidBackEnabled: false,
        //     animation: {
        //         type: 'none'
        //     }
        // });
    });
    module.exports = {};

});