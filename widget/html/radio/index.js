define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var checkUserId = api && api.pageParam.checkUserId
    var Http = require('U/http');
    var radio = new Vue({
        el: '#radio',
        template: _g.getTemplate('radio/index-main-V'),
        data: {
            avatar: '',
            date: "",
            images: "",
            city: "",
            sex: 1,
            description: "",
            type: "1",
            userId: 5,
            likeNum: 0,
            signNum: 0,
            createTime: "",
            identity: 0,
            nickname: "",
            id: 3,
            time: "",
            isEmpty: true,
            isOwn: !checkUserId || checkUserId === UserInfo.id,
            isSigner: 0,
            isLiker: 0,
            isDating: 0,
            identityMark: '',
        },
        created: function () {
            api.setStatusBarStyle({
                style: 'dark',
                color: 'rgba(0,0,0,0.0)'
            });
        },
        methods: {
            onApplyTap: function () {
                if (!this.isOwn) return
                _g.openWin({
                    header: {
                        data: {
                            title: '已报名名单',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'station-applier-win',
                    url: '../station/applier.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        broadCastId: radio.id
                    }
                });
            },
            onTap: function () {
                console.log('123');
            },
            delBroadcast: function () {
                api.confirm({
                    title: '确认删除该广播',
                    buttons: ['确定', '取消']
                }, function (ret, err) {
                    var index = ret.buttonIndex;
                    if (index === 1) {
                        Http.ajax({
                            data: {},
                            method: 'DELETE',
                            isSync: false,
                            url: '/broadCast/deleteMyBroadCast/' + UserInfo.id + '?token=' + UserInfo.token,
                            success: function (ret) {
                                if (ret.code == 200) {
                                    getData();
                                    api.sendEvent({
                                        name: 'station-index-main',
                                    });
                                    _g.toast('删除广播成功');

                                } else _g.toast(ret.msg);
                            },
                            error: function (err) {
                                _g.toast(err.msg);
                            }
                        });
                    }
                });
            },
            onSignUpTap: function () {
                if (this.isSigner) {
                    _g.toast('您已报名成功，无需重复报名');
                    return
                }
                if (this.sex === UserInfo.sex) {
                    _g.toast('不能报名同性的广播喔~')
                    retutn
                }
                api.confirm({
                    msg: '确定报名该广播？',
                    buttons: ['取消', '确定']
                }, function (ret, err) {
                    if (ret.buttonIndex === 2) {
                        Http.ajax({
                            data: {
                                broadCastId: radio.id,
                                signerId: UserInfo.id
                            },
                            isSync: false,
                            url: '/broadCast/signUp',
                            success: function (ret) {
                                if (ret.code == 200) {
                                    _g.toast('报名成功！');
                                    radio.isSigner = 1;
                                } else {
                                    _g.toast(ret.msg);
                                }
                            },
                            error: function (err) {
                                _g.toast(err.msg);
                            }
                        });
                    }
                })
            },
            onLikeTap(data, index) {
                if (this.isLiker) return
                Http.ajax({
                    data: {
                        broadCastId: radio.id,
                        likerId: UserInfo.id
                    },
                    isSync: false,
                    url: '/broadCast/like',
                    success: function (ret) {
                        if (ret.code == 200) {
                            radio.isLiker = 1;
                            radio.likeNum++;
                            _g.toast('点赞成功！')
                        } else {
                            _g.toast(ret.msg);
                        }
                    },
                    error(err) {
                        _g.toast(err.msg);
                    }
                });
            },
        },
    });

    function getData() {
        var checkId = checkUserId || UserInfo.id
        Http.ajax({
            data: {
                userId: checkId,
                viewerId: UserInfo.id
            },
            method: 'GET',
            isSync: false,
            url: '/broadCast/findByUserId',
            success: function (ret) {
                if (ret.code == 200) {
                    if (ret.data.id) {
                        radio.isEmpty = false
                        radio.date = ret.data.date;
                        radio.images = ret.data.images || [];
                        radio.city = ret.data.city || '';
                        radio.sex = ret.data.sex || UserInfo.sex;
                        radio.description = ret.data.description || '';
                        radio.userId = ret.data.userId || -1;
                        radio.likeNum = ret.data.likeNum || 0;
                        radio.signNum = ret.data.signNum || 0;
                        radio.createTime = ret.data.createTime || '';
                        radio.identity = ret.data.identity;
                        radio.nickname = ret.data.nickname || '';
                        radio.id = ret.data.id || -1;
                        radio.time = ret.data.time;
                        radio.isSigner = ret.data.isSigner;
                        radio.isLiker = ret.data.isLiker;
                        radio.identityMark = formatIdentityMark(ret.data)
                        radio.avatar = ret.data.avatar || '../../image/me/avatar.png';
                    } else radio.isEmpty = true
                } else _g.toast(ret.msg);
            },
            error: function (err) {
                _g.toast(err.msg);
            }
        });
    }

    function formatIdentityMark(data) {
        var res = '';
        if (data.sex == 0) {
            if (data.identity == 1) {
                res = '会员'
            } else {
                res = '非会员'
            }
        }
        if (data.sex == 1) {
            if (data.identity == 2) {
                res = '真实'
            } else {
                res = '未认证'
            }
        }
        return res
    }

    getData();
    api && api.addEventListener({
        name: 'station-index-main',
    }, function (ret, err) {
        getData();
    });
    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getData();
    });

    module.exports = {};

});