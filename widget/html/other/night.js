define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var night = new Vue({
        el: '#night',
        template: _g.getTemplate('other/night-main-V'),
        data: {
            images: [],
            keyword: '',
            list: [{
                id: '2',
                icon: '../../image/other/baiyang.png',
                name: '白羊座',
                date: '3.21-4.19'
            }, {
                id: '3',
                icon: '../../image/other/jinniu.png',
                name: '金牛座',
                date: '4.20-5.20'
            }, {
                id: '4',
                icon: '../../image/other/shuangyu.png',
                name: '双子座',
                date: '5.21-6.21'
            }, {
                id: '5',
                icon: '../../image/other/juxie.png',
                name: '巨蟹座',
                date: '6.22-7.22'
            }, {
                id: '6',
                icon: '../../image/other/shizi.png',
                name: '狮子座',
                date: '7.23-8.22'
            }, {
                id: '7',
                icon: '../../image/other/chunv.png',
                name: '处女座',
                date: '8.23-9.22'
            }, {
                id: '8',
                icon: '../../image/other/tianping.png',
                name: '天枰座',
                date: '9.23-10.23'
            }, {
                id: '9',
                icon: '../../image/other/tianxie.png',
                name: '天蝎座',
                date: '10.24-11.22'
            }, {
                id: '10',
                icon: '../../image/other/sheshou.png',
                name: '射手座',
                date: '11.23-12.21'
            }, {
                id: '11',
                icon: '../../image/other/mojie.png',
                name: '魔蝎座',
                date: '12.22-1.19'
            }, {
                id: '12',
                icon: '../../image/other/shuiping.png',
                name: '水瓶座',
                date: '1.20-2.18'
            }, {
                id: '1',
                icon: '../../image/other/shuangyu.png',
                name: '双鱼座',
                date: '2.19-3.20'
            }]
        },
        created: function () {

        },
        methods: {
            search() {
                _g.openWin({
                    header: {
                        data: {
                            title: '都市笑口组',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'other-consult',
                    url: '../other/consult.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        keyword: night.keyword
                    }
                });
            },
            clear() {
                this.keyword = ''
            },
            onItemTap: function(index) {
                _g.openWin({
                    header: {
                        data: {
                            title: '今日运势',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'other-starDetail',
                    url: '../other/starDetail.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        consName: night.list[index].name,
                        avatar: night.list[index].icon,
                        consId: night.list[index].id,
                    }
                });
            }
        },
    });
    var mySwiper = new Swiper('.swiper-container', {
        direction: 'horizontal',
        loop: true,

        // 如果需要前进后退按钮
        prevButton: '.swiper-button-prev',
        nextButton: '.swiper-button-next',
    })
    module.exports = {};

});