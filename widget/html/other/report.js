define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var checkUserId = api && api.pageParam.checkUserId;
    var Http = require('U/http');
    var UIMediaScanner = api.require('UIMediaScanner');
    var report = new Vue({
        el: '#report',
        template: _g.getTemplate('other/report-main-V'),
        data: {
            description: '',
            images: [],
            type: 1,
            selectImageNum: 1
        },
        created: function () {
            api.setStatusBarStyle({
                style: 'dark',
                color: 'rgba(0,0,0,0.0)'
            });
        },
        methods: {
            deleImg: function (index) {
                this.images.splice(index, 1);
            },
            addImg: function () {
                if (this.images.length >= 6) {
                    _g.toast('最多6张');
                    return
                }
                UIMediaScanner.open({
                    max: 6 - this.images.length,
                    type: 'picture',
                    bg: '#000',
                    texts: {
                        stateText: '已选择*项', //（可选项）字符串类型；状态文字内容；*号会被替换为已选择个数；默认：'已选择*项'
                        cancelText: '取消', //（可选项）字符串类型；取消按钮文字内容；默认：'取消'
                        finishText: '确定', //（可选项）字符串类型；完成按钮文字内容；默认：'完成'
                        selectedMaxText: '只能选择*个了！', //（可选项）字符串类型；最多显示提示语，*号会被替换为已选择个数;默认：'最多显示*个资源'
                        classifyTitle: '系统相册'
                    },
                    styles: {
                        bg: '#fff',
                        mark: {
                            icon: '',
                            position: 'bottom_left',
                            size: 26
                        },
                        nav: {
                            bg: '#000',
                            stateColor: '#fff',
                            stateSize: 18,
                            cancelBg: 'rgba(0,0,0,0)',
                            cancelColor: '#fff',
                            cancelSize: 18,
                            finishBg: 'rgba(0,0,0,0)',
                            finishColor: '#fff',
                            finishSize: 18
                        }
                    },
                    exchange: true,
                }, function (ret) {
                    if (ret.eventType === 'confirm') {
                        if (ret.list.length) {
                            var list = ret.list;
                            report.selectImageNum = list.length
                            list.forEach(function (ele) {
                                _g.showProgress();
                                postPicture(ele.path)
                            })
                        }
                    } else if (ret.eventType === 'albumError') _g.toast('访问相册失败')
                })
            },
            onReportTap: function () {
                if (!this.description) {
                    _g.toast('请填写你举报的原因');
                    return
                }
                reportOther();
            }
        },
    });

    function reportOther() {
        var images = '';
        if (report.images.length) images = report.images.join(',');
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: checkUserId,
                images: images,
                content: report.description
            },
            isSync: true,
            url: '/user/report',
            success: function (ret) {
                if (ret.code == 200) {
                    _g.toast('举报成功');
                    setTimeout(function () {
                        api && api.closeWin();
                    }, 500);
                } else {
                    _g.toast('举报失败')
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    }

    function postPicture(img) {
        api.ajax({
            url: CONFIG.HOST + '/common/uploadPhoto',
            method: 'post',
            isSync: true,
            lock: false,
            data: {
                values: {
                    token: UserInfo.token
                },
                files: {
                    image: img,
                }
            }
        }, function (ret, err) {
            if (ret.code == 200) {
                _g.hideProgress();
                report.images.push(ret.data);
            } else {
                _g.toast(ret.msg);
            }
        });
    }

    module.exports = {};

});