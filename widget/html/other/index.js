define(function (require, exports, module) {
    var checkUserId = api && api.pageParam.checkUserId;
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var weiXin = api.require('weiXin');
    var aliPayPlus = api.require('aliPayPlus');
    var clipBoard = api.require('clipBoard');
    var photoBrowser = api.require('photoBrowser');
    
    var winWidth = api.winWidth;
    var proportion = 1;
    var other = new Vue({
        el: '#other',
        template: _g.getTemplate('other/index-main-V'),
        data: {
            checkPermission: false,
            member: {
                avatar: '',
                avatarStatus: false,
                nickname: '郑小姐姐',
                distance: '48km',
                city: '广州市',
                age: '26',
                occupation: '学生',
                height: '170',
                weight: '45',
                bust: '34C',
                intoduce: '身材棒',
                wechat: '',
                style: '牛仔裤/裙子',
                language: '普通话',
                relationship: '单身',
                datingProgram: '',
                datingCodition: '',
                wechat: '',
                qq: '',
                checkLimit: 0,
                sex: 0,
                identity: 0,
                albumAmount: 0,
                datingMoney: 1
            },
            redpacketAmount: 1,
            contactWayAmount: 1,
            identityMark: '未认证',
            checkType: 1, // 表示是否可以查看
            checkImg: 0,
            imageNotEmpty: true,
            commentList: [],
            femaleCommentList: [{
                number: 0,
                title: '漂亮',
                select: false,
            }, {
                number: 0,
                title: '身材好',
                select: false,
            }, {
                number: 0,
                title: '好玩',
                select: false,
            }, {
                number: 0,
                title: '难约',
                select: false,
            }, {
                number: 0,
                title: '要红包',
                select: false,
            }, {
                number: 0,
                title: '照骗',
                select: false,
            }, ],
            maleCommentList: [{
                number: 0,
                title: '帅气',
                select: false,
            }, {
                number: 0,
                title: '身材好',
                select: false,
            }, {
                number: 0,
                title: '好玩',
                select: false,
            }, {
                number: 0,
                title: '大方',
                select: false,
            }, {
                number: 0,
                title: '不拖拉',
                select: false,
            }, {
                number: 0,
                title: '口嗨',
                select: false,
            }, ],
            showComment: false,
            sendImage: '',
            notNeedPay: true,
            hasContactWayPermission: false,
            images: [],
            checkContactWayNum: 0,
            hasCommented: false,
            hasAppointment: false,
            selectedIndex: -1,
            showDatingMoney: false
        },
        created: function () {
            api.setStatusBarStyle({
                style: 'light',
                color: 'rgba(0,0,0,0.0)'
            });
        },
        methods: {
            onWinTap: function (param) {
                // winUrl(param);
                _g.openWin({
                    header: {
                        data: {
                            title: '她的广播',
                        },
                        template: 'common/header-me-V',
                    },
                    name: 'radio-index',
                    url: '../radio/index.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        checkUserId: checkUserId
                    }
                });
            },
            getCopyTap: function (copyText) {
                if (this.hasContactWayPermission && copyText != '') {
                    clipBoard.set({
                        value: copyText
                    }, function (ret, err) {
                        if (ret) {
                            _g.toast('复制成功');
                        } else {
                            _g.toast('复制失败');
                        }
                    });
                }
            },
            getContacts() {
                if (this.hasContactWayPermission) return
                if (UserInfo.identity === 1) {
                    checkContactWayNum()
                } else {
                    checkContactPay()
                }

            },
            commentOther: function () {
                // checkAppointment()
                this.showComment = true;
            },
            hideComment: function () {
                this.showComment = false;
            },
            onCommentTap: function (index) {
                if (!this.hasAppointment) return
                // if (!other.hasAppointment) {
                //     _g.toast('您未和该用户约会过，不能评价哦');
                //     return;
                // }
                if (other.hasCommented) return
                if (this.selectedIndex === -1) {
                    this.commentList[index].select = true;
                    this.commentList[index].number++;
                    this.selectedIndex = index
                } else {
                    this.commentList[this.selectedIndex].select = false;
                    this.commentList[this.selectedIndex].number--;
                    this.commentList[index].select = true;
                    this.commentList[index].number++;
                    this.selectedIndex = index;
                }
            },
            onPostCommentTap: function () {
                if (other.hasCommented) return
                postComment();
            },
            onPayTap: function (payStyle) {
                if (payStyle == 'ali') {
                    getAliOrder();
                }
                if (payStyle == 'wx') {
                    getWechatOrder('contact');
                }
                // }
            },
            checkImages() {
                _g.openWin({
                    header: {
                        data: {
                            title: '她的图库',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'me-images',
                    url: '../me/images.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        checkUserId: checkUserId,
                        notNeedPay: other.notNeedPay
                    }
                });
            },
            pay4Img() {
                if (!this.imageNotEmpty) return
                if (UserInfo.identity === 1) {
                    checkAlbumNum()
                } else check4Image()
            },
            applyCheck() {
                api && api.confirm({
                    msg: '申请查看需要给对方发一张你的照片。放心，你的照片会在对方长按屏幕查看的2秒内焚毁',
                    buttons: ['选择照片', '取消']
                }, function (ret, err) {
                    if (ret.buttonIndex === 1) {
                        _g.openPicActionSheet({
                            allowEdit: true,
                            suc: function (ret) {
                                postAvatar(ret.data);
                            }
                        });
                    }
                })
            },
            previewImage(images, index) {
                if (images[index].status) return  // 非会员阅读后，阻止在看
                _g.showProgress();
                var img = new Image();
                img.src = images[index].imagePath;
                img.onload = function () {
                    proportion = img.height / img.width;
                    _g.hideProgress();

                    setTimeout(function() {
                        openPhotoSwipe(images, index);
                    }, 200);
                }
            },
            showDatingMoneyTap() {
                this.showDatingMoney = true
            },
            openAvatar(image) {
                photoBrowser.open({
                    images: [image],
                    placeholderImg: 'widget://res/img/apicloud.png',
                    bgColor: '#000',
                    mode: 2
                }, function (ret, err) {
                    if (ret) {
                        if (ret.eventType === 'click') {
                            photoBrowser.close()
                        }
                    } else {
                        _g.toast('图片打开失败')
                    }
                })
            }
        }
    });

    // 付费查看相册
    function check4Image() {
        api && api.confirm({
            msg: '付费相册需要支付金额查看',
            buttons: ['确定', '取消']
        }, function (ret, err) {
            if (ret.buttonIndex === 1) {
                getWechatOrder('album');
            }
        })
    }

    function formatIdentityMark(data) {
        var res = '';
        if (data.sex == 0) {
            if (data.identity == 1) {
                res = '会员'
            } else {
                res = '非会员'
            }
        }
        if (data.sex == 1) {
            if (data.identity == 2) {
                res = '真实'
            } else {
                res = '未认证'
            }
        }
        return res
    }

    function checkContactPay() {
        var femaleBtns = ['付费查看(' + other.contactWayAmount + '元)']
        var maleBtns = ['付费查看(' + other.contactWayAmount + '元)', '成为会员，免费查看']
        var isFirstBtn = UserInfo.identity === 1
        api && api.actionSheet({
            title: '查看全部资料',
            buttons: isFirstBtn ? femaleBtns : maleBtns
        }, function (ret, err) {
            if (isFirstBtn) {
                if (ret.buttonIndex === 1) {
                    other.onPayTap('wx');
                }
            } else {
                if (ret.buttonIndex === 1) {
                    other.onPayTap('wx');
                } else if (ret.buttonIndex === 2) {
                    _g.openWin({
                        header: {
                            data: {
                                title: '会员中心',
                            },
                            template: 'common/header-base-V',
                        },
                        name: 'me-member',
                        url: '../me/member.html',
                        bounces: true,
                        slidBackEnabled: false,
                        pageParam: {}
                    });
                }
            }
        })
    }

    // 图片上传
    function postAvatar(img) {
        api.ajax({
            url: CONFIG.HOST + '/common/uploadPhoto',
            method: 'post',
            data: {
                values: {
                    token: UserInfo.token
                },
                files: {
                    image: img,
                }
            }
        }, function (ret, err) {
            if (ret.code == 200) {
                other.sendImage = ret.data;
                Http.ajax({
                    data: {
                        userId: checkUserId,
                        type: 0,
                        senderId: UserInfo.id,
                        content: '请求查看你的资料',
                        sendImage: other.sendImage
                    },
                    isSync: false,
                    url: '/message/save',
                    success: function (ret) {
                        if (ret.code == 200) {
                            _g.toast('发送成功，请等待')
                        }
                    },
                    error(err) {
                        _g.toast(err.msg)
                    }
                });
            } else {
                _g.toast(ret.codeDesc);
            }
        });
    }

    var openPhotoSwipe = function (images, index) {
        var image = images[index];
        if (!image.checkStatus && image.level === 1) {
            if (!UserInfo.identity) {
                api && api.confirm({
                    msg: '照片已经销毁，升级会员延长至10秒',
                    buttonIndex: ['确定', '取消']
                }, function (ret, err) {
                    _g.openWin({
                        header: {
                            data: {
                                title: '会员中心',
                            },
                            template: 'common/header-base-V',
                        },
                        name: 'me-member',
                        url: '../me/member.html',
                        bounces: false,
                        slidBackEnabled: false,
                        pageParam: {}
                    });
                })
            } else {
                _g.toast('照片已销毁了')
            }
            return
        }
        if (image.level === 2 && !image.checkStatus && !other.notNeedPay) {
            api && api.confirm({
                msg: '红包图片需要支付金额查看',
                buttons: ['确定', '取消']
            }, function (ret, err) {
                if (ret.buttonIndex === 1) {
                    getWechatOrder('redImage');
                }
            })
            return
        }
        if (image.level === 1) {
            var time = UserInfo.identity === 1 ? 10000 : 3000;
            markStatus(image);
            setTimeout(function () {
                gallery.close()
            }, time);
        }
        var items = [{
            src: image.imagePath,
            w: 0,
            h: 0
        }];
        var isReady = false;
        var pswpElement = document.querySelectorAll('.pswp')[0];
        var options = {
            index: index,
            mainClass: 'pswp--minimal--dark',
            barsSize: {
                top: 0,
                bottom: 0
            },
            captionEl: false,
            fullscreenEl: false,
            shareEl: false,
            history: false,
            focus: false,
            showAnimationDuration: 200,
            hideAnimationDuration: 200,
            bgOpacity: 0.85,
            showHideOpacity: true,
            tapToToggleControls: false,
            closeOnVerticalDrag: true,
            tapToClose: true,
            closeEl: true
        };
        var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.listen('gettingData', function (index, item) {
            if (item.html === undefined && item.onloading === undefined && (item.w < 1 || item.h < 1)) { // unknown size
                var img = new Image();
                item.onloading = true;
                img.onload = function () { // will get size after load
                    if (img.width > winWidth) {
                        item.w = winWidth;
                        item.h = winWidth * img.height / img.width;
                    } else {
                        item.w = img.width; // set image width
                        item.h = img.height; // set image height
                    }
                    // item.w = img.width; // set image width
                    // item.h = img.height; // set image height
                    gallery.updateSize(true); // reinit Items
                };
                img.src = item.src; // let's download image
            } else isReady = true
        });
        gallery.init();
    };

    // 状态标记
    function markStatus(data) {
        Http.ajax({
            data: {
                userId: checkUserId,
                viewerId: UserInfo.id,
                photoId: data.id,
                type: data.level === 1 ? 1 : 2
            },
            isSync: false,
            url: '/photoViewer/save',
            success: function (ret) {
                if (ret.code == 200) {
                    data.checkStatus = data.level === 2
                } else _g.toast('图片标记失败：' + ret.msg)
            },
            error(err) {
                _g.toast('图片标记错误：' + ret.msg)
            }
        });
    }

    // 获取个人资料接口
    function getInfoData() {
        Http.ajax({
            data: {
                userId: checkUserId
            },
            isSync: true,
            lock: false,
            url: '/user/getUserInfo',
            success: function (ret) {
                getAllComment();
                if (ret.code == 200) {
                    other.member = ret.data;
                    other.identityMark = formatIdentityMark(ret.data)
                    if (other.member.checkLimit === 2) {
                        hasCheckPermission();
                    } else if (other.member.checkLimit === 1) {
                        getImagesData();
                        hasContactWayPermission();
                    } else if (other.member.checkLimit === 0) {
                        getImagesData();
                        hasContactWayPermission();
                    } else {
                        getImagesData();
                        // hasContactWayPermission();
                        other.checkImg = true;
                    }
                    other.commentList = other.member.sex ? other.femaleCommentList : other.maleCommentList
                    checkAppointment();
                    getAllSysAmount();
                }
            }
        });
    };

    //否拥有查看付费相册的权限
    function hasAlbumPermission() {
        Http.ajax({
            data: {
                userId: checkUserId,
                unlockerId: UserInfo.id
            },
            isSync: false,
            lock: false,
            method: 'get',
            url: '/user/hasAlbumPermission',
            success: function (ret) {
                if (ret.code == 200) {
                    if (ret.hasAlbumPermission) {
                        other.notNeedPay = true
                        other.checkImg = 1
                    } else {
                        if (other.member.checkLimit === 0) {
                            other.checkImg = 1
                        } else if (other.member.checkLimit === 1) {
                            other.checkImg = 0
                        } else if (other.member.checkLimit === 2) {
                            other.checkImg = 1
                        }
                        other.notNeedPay = false
                    }
                }
            },
            error(err) {
                _g.toast(err.msg)
            }
        });
    }

    // 会员查看付费相册当天次数
    function checkAlbumNum() {
        Http.ajax({
            data: {},
            isSync: false,
            lock: false,
            method: 'get',
            url: '/user/checkAlbumNum/?unlockerId=' + UserInfo.id,
            success: function (ret) {
                if (ret.code == 200) {
                    var num = ret.checkAlbumNum
                    if (num < 3) {
                        signAlbum()
                    } else {
                        check4Image()
                    }
                }
            },
            error(err) {
                _g.toast(err.msg)
            }
        });
    };

    // 会员当天已查看联系方式次数
    function checkContactWayNum() {
        Http.ajax({
            data: {},
            isSync: false,
            lock: false,
            method: 'get',
            url: '/user/checkContactWayNum/?checkerId=' + UserInfo.id,
            success: function (ret) {
                if (ret.code == 200) {
                    var num = ret.checkContactWayNum
                    if (num < 3) {
                        signContactWay()
                    } else {
                        checkContactPay()
                    }
                }
            },
            error(err) {
                _g.toast(err.msg)
            }
        });
    };

    //否拥有查看联系方式的权限
    function hasContactWayPermission() {
        Http.ajax({
            data: {
                userId: checkUserId,
                checkerId: UserInfo.id
            },
            isSync: true,
            lock: false,
            method: 'get',
            url: '/user/hasContactWayPermission',
            success: function (ret) {
                if (ret.code == 200) {
                    if (ret.hasContactWayPermission) {
                        other.hasContactWayPermission = true
                    } else {
                        other.hasContactWayPermission = false
                    }
                }
            },
            error(err) {
                _g.toast(err.msg)
            }
        });
    }
    // 是否有查看权限
    function hasCheckPermission() {
        Http.ajax({
            data: {
                userId: checkUserId,
                checkerId: UserInfo.id
            },
            isSync: false,
            method: 'get',
            url: '/user/hasCheckPermission',
            success: function (ret) {
                if (ret.code == 200) {
                    if (ret.hasCheckPermission) {
                        other.checkType = 1;
                        getImagesData();
                        hasContactWayPermission();
                    } else other.checkType = 0
                }
            },
            error(err) {
                _g.toast(err.msg)
            }
        });
    }

    function getAllSysAmount() {
        getSysAmount('REDPACKET_AMOUNT');
        getSysAmount('CONTACTWAY_AMOUNT');
    }

    // 获取红包或者联系方式金额
    function getSysAmount(key) {
        Http.ajax({
            data: {
                key: key
            },
            method: 'get',
            isSync: false,
            lock: false,
            url: '/sys/config/findByKey',
            success: function (ret) {
                if (ret.code === 200 || ret.code === '200') {
                    if (key === 'REDPACKET_AMOUNT') other.redpacketAmount = ret.data || 3
                    else if (key === 'CONTACTWAY_AMOUNT') other.contactWayAmount = ret.data || 10
                }
            }
        });
    }

    // 图片获取
    function getImagesData() {
        Http.ajax({
            data: {
                userId: checkUserId,
                viewerId: UserInfo.id
            },
            isSync: true,
            lock: false,
            url: '/userPhoto/getUserPhoto',
            success: function (ret) {
                if (ret.code == 200) {
                    var data = ret.data;
                    if (data.length) {
                        if (other.member.sex === 1) {
                            hasAlbumPermission();
                        } else other.checkImg = 1;
                        other.imageNotEmpty = true;
                        other.images = data
                    } else other.imageNotEmpty = false;
                }
            }
        });
    };
    // 获取个人对用户评论
    function getComment() {
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: checkUserId
            },
            isSync: false,
            url: '/relationship/evaluateContent',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    if (ret.data.content != '') {
                        other.hasCommented = true;
                        var comment = ret.data.content;
                        for (var j = 0; j < other.commentList.length; j++) {
                            if (comment == other.commentList[j].title) {
                                other.commentList[j].select = true;
                            }
                        }
                    }
                }
            }
        });
    };
    // 获取所有人对用户的评价
    function getAllComment() {
        Http.ajax({
            data: {
                userId: checkUserId
            },
            isSync: false,
            lock: false,
            url: '/relationship/evaluateList',
            success: function (ret) {
                getComment();
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    var data = ret.data;
                    for (var index in data) {
                        for (var i = 0; i < other.commentList.length; i++) {
                            if (index == other.commentList[i].title) {
                                other.commentList[i].select = true;
                                other.commentList[i].number = data[index];
                            }
                        }
                    }
                }
            }
        });
    };
    // 两者之间是否约会过，可以评论
    function checkAppointment() {
        Http.ajax({
            data: {},
            isSync: true,
            lock: false,
            method: 'get',
            url: '/broadCast/isDated?activeId=' + UserInfo.id + '&passiveId=' + checkUserId,
            success: function (ret) {
                if (ret.code == 200) {
                    other.hasAppointment = ret.isDated;
                }
            },
            error(err) {
                _g.toast(err.msg)
            }
        });
    }
    // 提交评论
    function postComment() {
        var content = '';
        for (var i = 0; i < other.commentList.length; i++) {
            if (other.commentList[i].select) {
                if (content == '') {
                    content = other.commentList[i].title
                } else {
                    content = content + ',' + other.commentList[i].title
                }
            }
        }
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: checkUserId,
                content: content
            },
            isSync: false,
            url: '/relationship/evaluate',
            success: function (ret) {
                if (ret.code == 200) {
                    _g.toast('评论成功');
                    other.showComment = false;
                    other.hasCommented = true;
                } else {
                    _g.toast('评论失败');
                }
            }
        });
    };

    // 支付宝
    function getAliOrder() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                payMoney: 0.01
            },
            isSync: true,
            url: '/pay/getAlipayOrder',
            success: function (ret) {
                // alert(_g.j2s(ret.data));
                // member.isTouched = true;
                if (ret.code == 200) {
                    var data = ret.data;
                    // var param = "partner=2088231486933302&subject=" + data.subject + "seller_id=2088231486933302"
                    postAlipayRecharge(data);
                } else {
                    alert(_g.j2s(ret));
                }
            }
        });
    };

    function postAlipayRecharge(data) {
        // alert(data);
        aliPayPlus.payOrder({
            orderInfo: data
        }, function (ret, err) {
            api.alert({
                title: '支付结果',
                msg: ret,
                buttons: ['确定']
            });
            alert(_g.j2s(ret));
        });
    }

    // 获取微信预支付订单
    function getWechatOrder(type) {
        var payType = 0;
        switch (type) {
            case 'contact':
                payType = 0;
                break;
            case 'redImage':
                payType = 1;
                break;
            case 'album':
                payType = 2;
                break;
            default:
                break;
        }
        var data = {
            userId: UserInfo.id,
            collecterId: checkUserId,
            payMoney: 1,
            type: payType
        }
        if (type === 'album') data.payMoney = (Number(other.member.albumAmount) || 1) * 100
        if (type === 'redImage') {
            for (var i in other.images) {
                if (other.images[i].level === 2) {
                    data.serviceId = other.images[i].id
                    break;
                }
            }
            data.payMoney = Number(other.redpacketAmount) * 100
        }
        if (type === 'contact') data.payMoney = Number(other.contactWayAmount) * 100
        Http.ajax({
            data: data,
            isSync: true,
            url: '/pay/getWxPayOrder',
            success: function (ret) {
                if (ret.code == 200) {
                    postWechatRecharge(ret.data, type);
                } else {
                    alert(_g.j2s(ret));
                }
            }
        });
    };

    // 调起微信支付
    function postWechatRecharge(data, type) {
        weiXin.payOrder({
            key: data.appid,
            orderId: data.prepayid,
            partnerId: data.partnerid,
            nonceStr: data.noncestr,
            timeStamp: data.timestamp,
            sign: data.sign,
            package: data.package
        }, function (ret, err) {
            if (ret.status) {
                if (type === 'contact') {
                    api && api.sendEvent({
                        name: 'work-index-changeInfo',
                    });
                    // signContactWay()
                    other.hasContactWayPermission = true
                } else if (type === 'album') {
                    // signAlbum()
                    other.checkImg = 1
                    other.notNeedPay = true
                } else if (type === 'redImage') {
                    for (var i in other.images) {
                        if (other.images[i].level === 2) {
                            other.images[i].checkStatus = true
                            break;
                        }
                    }
                }
            } else {
                api.alert('支付失败');
            }
        });
    };

    // 付费查看联系人
    function signContactWay() {
        Http.ajax({
            data: {
                userId: checkUserId,
                checkerId: UserInfo.id
            },
            isSync: false,
            url: '/user/signContactWay',
            success: function (ret) {
                if (ret.code == 200) {
                    other.hasContactWayPermission = true
                }
            }
        });
    };
    // 付费后解锁相册
    function signAlbum() {
        Http.ajax({
            data: {
                userId: checkUserId,
                unlockerId: UserInfo.id
            },
            isSync: false,
            url: '/user/signAlbum',
            success: function (ret) {
                if (ret.code == 200) {
                    other.checkImg = 1
                    other.notNeedPay = true
                    _g.toast('解锁成功')
                } else {
                    _g.toast('解锁失败')
                }
            }
        });
    };

    // 拉黑个人接口
    function pullBlackOther() {
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: checkUserId,
                defriend: true
            },
            isSync: false,
            url: '/relationship/defriend',
            success: function (ret) {
                if (ret.code == 200) {
                    api && api.sendEvent({
                        name: 'people-people-reload'
                    });
                    _g.toast('拉黑成功');
                } else {
                    _g.toast('拉黑失败')
                }
            }
        });
    };

    getInfoData();
    api && api.addEventListener({
        name: 'other-index-more',
    }, function (ret, err) {
        api && api.actionSheet({
            cancelTitle: '取消',
            buttons: ['拉黑（屏蔽对方）', '举报']
        }, function (ret, err) {
            var index = ret.buttonIndex;
            if (index === 1) {
                pullBlackOther()
            } else if (index == 2) {
                if (other.hasAppointment) {
                    _g.openWin({
                        header: {
                            data: {
                                title: '举报他人',
                            },
                            template: 'common/header-base-V',
                        },
                        name: 'other-report',
                        url: '../other/report.html',
                        bounces: false,
                        slidBackEnabled: false,
                        pageParam: {
                            checkUserId: checkUserId
                        }
                    });
                } else {
                    _g.toast('约过会，才能举报对方哦');
                }
            }
        })
    });
    api && api.addEventListener({
        name: 'other-index-share',
    }, function (ret, err) {
        alert('分享');
    });
    // 监听头部变白色
    api && api.addEventListener({
        name: 'other-index-title',
    }, function (ret, err) {
        api.setStatusBarStyle({
            style: 'light',
            color: 'rgba(0,0,0,0.0)'
        });
    });
    module.exports = {};

});