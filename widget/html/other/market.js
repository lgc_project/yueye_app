define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var market = new Vue({
        el: '#market',
        template: _g.getTemplate('other/market-main-V'),
        data: {
            list: [{
                id: '100033',
                model: '../../image/other/chunv.png',
                name: '菠菜',
                introduce: '便靓正',
                price: '3.2',
                timeVie: '2018-06-11 12:43',
            }, ],
            goodsID: '',
            address: '',
            phone: '',
            receiver: '',
            showPopup: false
        },
        created: function() {
            this.list = [];
        },
        methods: {
            onBuyTap: function(index) {
                this.goodsID = this.list[index].id;
                this.showPopup = true;
            },
            onCancelData: function() {
                // this.receiver = '';
                // this.phone = '';
                // this.address = '';
                this.showPopup = false;
            },
            onPostData: function() {
                if(this.receiver == '') {
                    _g.toast('收货人不能为空');
                    return;
                }
                if(this.phone == '') {
                    _g.toast('联系方式不能为空');
                    return;
                }
                if(this.address == '') {
                    _g.toast('收货地址不能为空');
                    return;
                }
                this.showPopup = false;
                postOrder();
            },
        },
    });
    // 获取所有商品
    function getData() {
        Http.ajax({
            data: {

            },
            isSync: true,
            url: '/commodity/getAll',
            success: function(ret) {
                if (ret.code == 200) {
                    var data = ret.data;
                    market.list = listData(data);
                }
            }
        });
    };
    // 数据转换
    function listData(data) {
        var list = data ? data : [];
        return _.map(list, function(item) {
            return {
                id: item.id || '',
                model: item.imgPath || '',
                name: item.name || '',
                introduce: item.introduce || '',
                price: item.price || '',
            }
        });
    };
    // 购买商品
    function postOrder() {
        if(UserInfo) {
            Http.ajax({
                data: {
                    userId: UserInfo.id,
                    commodityId: market.goodsID,
                    receiver: market.receiver,
                    phone: market.phone,
                    address: market.address
                },
                isSync: true,
                url: '/commodity/saveDeal',
                success: function(ret) {
                    if (ret.code == 200) {
                        _g.toast('下单成功');
                    } else {
                        _g.toast('下单失败，请重新下单');
                    }
                }
            });
        } else {
            api && api.openWin({
                name: 'account-login-win',
                url: '../account/login.html',
                pageParam: {
                    from: 'root'
                },
                bounces: false,
                slidBackEnabled: false,
                animation: {
                    type: 'none'
                }
            });
            setTimeout(function () {
                _g.closeWins(['other-market-win', 'main-night-win']);
            }, 1000);
        }
    };
    getData();
    
    module.exports = {};

});
