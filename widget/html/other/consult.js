define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var propKeyword = api && api.pageParam.keyword;
    var timestamp = Date.parse(new Date()) / 1000;
    var Http = require('U/http');
    var consult = new Vue({
        el: '#consult',
        template: _g.getTemplate('other/consult-main-V'),
        data: {
            list: [],
            keyword: propKeyword,
            items: [],
            page: 1,
            size: 20
        },
        created() {
            this.list = []
        },
        methods: {
            clear() {
                this.keyword = ''
            }
        }
    })

    if (propKeyword) getSearchData();
    else getDayData();

    function getSearchData() {
        if (!consult.keyword) {
            _g.toast('搜索不能为空!')
            return
        }
        api.ajax({
            url: 'http://route.showapi.com/1601-2',
            method: 'get',
            data: {
                values: {
                    showapi_appid: '81951',
                    showapi_sign: 'c21dc8bd6f13438696d18fd5007dc172',
                    showapi_res_gzip: '1',
                    keyWords: consult.keyword
                }
            }
        }, function (ret, err) {
            if (ret.showapi_res_code == 0) {
                if (ret.showapi_res_body.contentlist.length) {
                    consult.list = ret.showapi_res_body.contentlist;
                } else {
                    _g.toast('搜索没有数据')
                }
            } else {
                _g.toast(ret.showapi_res_error)
            }
        });
    }

    function getDayData() {
        // api.ajax({
        //     url: 'http://v.juhe.cn/joke/content/list.php',
        //     method: 'post',
        //     data: {
        //         values: {
        //             sort: 'asc',
        //             page: consult.page,
        //             pageSize: consult.size,
        //             time: timestamp,
        //             key: '2d7fd6f3e1e71301dbda386461a256ff',
        //         }
        //     }
        // }, function (ret, err) {
        //     if (ret.error_code == 0) {
        //         if (ret.result.data.length) {
        //             consult.items = consult.items.concat(ret.result.data);
        //         } else {
        //             _g.toast('搜索没有数据')
        //         }
        //     } else {
        //         _g.toast(ret.reason)
        //     }
        // });
        Http.ajax({
            data: {
                page: consult.page,
                pageSize: consult.size,
                time: timestamp
            },
            isSync: true,
            url: '/zodiac/getJuhe',
            success: function (ret) {
                if (ret.code == 200) {
                    if (ret.data.data.length) {
                        consult.items = consult.items.concat(ret.data.data);
                    } else {
                        _g.toast('搜索没有数据')
                    }
                } else {
                    _g.toast(ret.reason)
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    }

    api.addEventListener({
        name: 'scrolltobottom',
        extra: {
            threshold: 20
        }
    }, function (ret, err) {
        if (!propKeyword) {
            consult.page++;
            getDayData();
        }
    });
    module.exports = {};

});