define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo') || '';
    var myDate = new Date();
    var nowYear = myDate.getFullYear();
    var nowMonth = myDate.getMonth() + 1;
    var nowDay = myDate.getDate();
    var Http = require('U/http');
    var AppKey = "ddce16ce791fa916251e29fb97939540";
    var consName = api && api.pageParam.consName;
    var consId = api && api.pageParam.consId;
    var avatar = api && api.pageParam.avatar;
    var starDetail = new Vue({
        el: '#starDetail',
        template: _g.getTemplate('other/starDetail-main-V'),
        data: {
            avatar: '',
            name: '',
            year: '',
            month: '',
            day: '',
            probability: '80',
            number: '4',
            health: '80',
            color: '白色',
            love: '80',
            partner: '水瓶座',
            money: '80',
            summary: '金牛座今天可以强烈感受到有关领导方面的事情，以及要注意口舌是非，说话要经过大脑，看准时机，切不可轻易采取行动。工作环境友善。让自己节奏慢下来多做事情，会有所收获。',
            sendText: '',
            list: [{
                nickname: '昵称',
                content: '金牛座今天可以强烈感受到有关领导方面的事情，以及要注意口舌是非，让自己节奏慢下来多做事情，会有所收获。'
            }]
        },
        created: function () {
            this.name = consName;
            this.year = nowYear;
            this.month = nowMonth;
            this.day = nowDay;
            this.avatar = avatar;
            this.list = []
        },
        methods: {
            onSendTap: function() {
                if(this.sendText == '') {
                    api.alert({
                        title: '提示',
                        msg: '评论内容不能为空',
                    }, function(ret, err) {

                    });
                } else {
                    postContent();
                }
            }
        },
    });
    function getData() {
        // var dataUrl = "consName=" + consName + "&key=" + AppKey + "&type=today";
        // api.ajax({
        //     url: "http://web.juhe.cn:8080/constellation/getAll?" + dataUrl,
        //     method: 'get',
        //     data: {}
        // }, function (ret, err) {
        //     if (ret.error_code == 0) {
        //         starDetail.probability = ret.all;
        //         starDetail.color = ret.color;
        //         starDetail.health = ret.health;
        //         starDetail.love = ret.love;
        //         starDetail.money = ret.money;
        //         starDetail.number = ret.number;
        //         starDetail.partner = ret.QFriend;
        //         starDetail.summary = ret.summary;
        //         // starDetail.summary = ret.summary;
        //         // _g.toast('头像上传成功');
        //     } else {
        //         _g.toast(ret.codeDesc);
        //     }
        // });
        Http.ajax({
            data: {
                name: consName,
                // key: AppKey,
                type: 'today'
            },
            method: 'post',
            isSync: true,
            url: '/zodiac/getZodiacFortune',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    starDetail.probability = ret.data.all;
                    starDetail.color = ret.data.color;
                    starDetail.health = ret.data.health;
                    starDetail.love = ret.data.love;
                    starDetail.money = ret.data.money;
                    starDetail.number = ret.data.number;
                    starDetail.partner = ret.data.QFriend;
                    starDetail.summary = ret.data.summary;
                } else {
                    _g.toast(ret.codeDesc);
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    }
    function postContent() {
        if(!UserInfo) {
            api && api.setStatusBarStyle({
                style: 'dark'
            });
            api && api.openWin({
                name: 'account-login-win',
                url: '../account/login.html',
                pageParam: {
                    from: 'root'
                },
                bounces: false,
                slidBackEnabled: false,
                animation: {
                    type: 'none'
                }
            });
            setTimeout(function () {
                _g.closeWins(['other-consult-win', 'other-index-win', 'other-starDetail-win', 'main-night-win']);
            }, 1000);
            return false;
        }
        Http.ajax({
            data: {
                zodiacId: consId,
                userId: UserInfo.id,
                content: starDetail.sendText,
            },
            isSync: true,
            url: '/zodiac/evaluate',
            success: function (ret) {
                if (ret.code == 200) {
                    getContent();
                } else {
                    _g.toast(ret.codeDesc);
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    }
    function getContent() {
        Http.ajax({
            data: {
                zodiacId: consId,
            },
            isSync: true,
            url: '/zodiac/evaluateList',
            success: function (ret) {
                if (ret.code == 200) {
                    starDetail.list = ret.data;
                } else {
                    _g.toast(ret.codeDesc);
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    }
    
    getContent();
    getData();
    module.exports = {};

});