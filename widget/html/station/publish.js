define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var UIActionSelector = api.require('UIActionSelector');
    var weiXin = api.require('weiXin');
    var publish = new Vue({
        el: '#publish',
        template: _g.getTemplate('station/publish-main-V'),
        data: {
            city: '',
            cityTemp: '点击选择城市',
            date: '',
            time: '',
            images: [],
            type: 0
        },
        created: function () {

        },
        methods: {
            onCityTab: function () {
                UIActionSelector.open({
                    datas: 'widget://res/Regions.json', //拿到APP原本省市县数据
                    layout: {
                        row: 5,
                        col: 2,
                        height: 32,
                        size: 12,
                        sizeActive: 14,
                        rowSpacing: 5,
                        colSpacing: 10,
                        maskBg: 'rgba(0,0,0,0.2)',
                        bg: '#fff',
                        color: '#888',
                        colorActive: '#ea39b9',
                        colorSelected: '#ea39b9'
                    },
                    animation: true,
                    cancel: {
                        text: '取消',
                        size: 12,
                        w: 90,
                        h: 35,
                        bg: '#ccc',
                        bgActive: '#ccc',
                        color: '#fff',
                        colorActive: '#fff'
                    },
                    ok: {
                        text: '确定',
                        size: 12,
                        w: 90,
                        h: 35,
                        bg: '#571A92',
                        bgActive: '#ccc',
                        color: '#fff',
                        colorActive: '#fff'
                    },
                    title: {
                        text: '请选择省市',
                        size: 12,
                        h: 53,
                        bg: '#eee',
                        color: '#571A92'
                    },
                }, function (ret, err) {
                    if (ret && ret.eventType == 'ok') {
                        publish.city = ret.level2;
                    } else {
                        UIActionSelector.close();
                    }
                    if (err) {
                        _g.toast('系统出错,请稍后再试!');
                    }
                });
            },
            onTimeTap: function () {
                api && api.actionSheet({
                    title: '时间',
                    cancelTitle: '取消',
                    buttons: ['上午', '中午', '晚上', '通宵', '一整天']
                }, function (ret, err) {
                    switch (ret.buttonIndex) {
                        case 1:
                            publish.time = '上午'
                            break;
                        case 2:
                            publish.time = '中午'
                            break;
                        case 3:
                            publish.time = '晚上'
                            break;
                        case 4:
                            publish.time = '通宵'
                            break;
                        case 5:
                            publish.time = '一整天'
                            break;
                        default:
                            break;
                    }
                })
            },
            deleImg: function (index) {
                this.images.splice(index, 1);
            },
            addImg: function () {
                if (this.images.length > 6) {
                    _g.toast('最多6张');
                    return
                }
                _g.openPicActionSheet({
                    allowEdit: true,
                    suc: function (ret) {
                        postAvatar(ret.data);
                    }
                });
            },
            handleSend: function () {
                if (!this.city || !this.date || !this.time) {
                    _g.toast('请完善信息');
                    return
                }
                if (UserInfo.sex === 0 && UserInfo.identity === 0 || UserInfo.sex === 1 && UserInfo.identity === 0) {
                    api && api.confirm({
                        msg: '非会员发布需要支付金额',
                        buttons: ['确定', '取消']
                    }, function (ret, err) {
                        if (ret.buttonIndex === 1) {
                            getWechatOrder();
                        }
                    })
                } else handleSend()


            }
        },
    });

    function handleSend() {
        var images = '';
        if (publish.images.length) images = publish.images.join(',');
        Http.ajax({
            data: {
                userId: UserInfo.id,
                type: publish.type,
                city: publish.city,
                date: publish.date,
                time: publish.time,
                images: images
            },
            isSync: true,
            url: '/broadCast/saveBroadCast',
            success: function (ret) {
                if (ret.code == 200 || ret.code == '200') {
                    _g.toast('发布成功');
                    api.sendEvent({
                        name: 'station-index-main',
                    });
                    setTimeout(function () {
                        api && api.closeWin();
                    }, 300);
                } else {
                    _g.toast(ret.codeDesc);
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    }

    function getWechatOrder() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                payMoney: 1,
                type: 3
            },
            isSync: true,
            url: '/pay/getWxPayOrder',
            success: function (ret) {
                if (ret.code == 200) {
                    postWechatRecharge(ret.data);
                } else {
                    alert(_g.j2s(ret));
                }
            }
        });
    };

    function postWechatRecharge(data, redImg) {
        weiXin.payOrder({
            key: data.appid,
            orderId: data.prepayid,
            partnerId: data.partnerid,
            nonceStr: data.noncestr,
            timeStamp: data.timestamp,
            sign: data.sign,
            package: data.package
        }, function (ret, err) {
            if (ret.status) {
                handleSend()
            } else {
                api.alert('支付失败');
            }
        });
    };

    function postAvatar(img) {
        api.ajax({
            url: CONFIG.HOST + '/common/uploadPhoto',
            method: 'post',
            isSync: true,
            data: {
                values: {
                    token: UserInfo.token
                },
                files: {
                    image: img,
                }
            }
        }, function (ret, err) {
            if (ret.code == 200) {
                publish.images.push(ret.data);
            } else {
                _g.toast(ret.msg);
            }
        });
    }

    api && api.addEventListener({
        name: 'station-index-main',
    }, function (ret, err) {
        // getData();
    });

    module.exports = {};

});