define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var UIActionSelector = api.require('UIActionSelector');
    var citySelector = api.require('citySelector');
    var photoBrowser = api.require('photoBrowser');

    var station = new Vue({
        el: '#station',
        template: _g.getTemplate('station/index-main-V'),
        data: {
            province: '',
            city: '',
            sex: 1,
            sortKey: 'create_time',
            list: [{
                avatar: null,
                city: "广州市",
                createTime: "2018-,12-12 09:09:09",
                date: "2018-,12-12",
                description: "约会要求",
                id: 2,
                identity: 0,
                images: null,
                isLiker: 1,
                isSigner: 1,
                likeNum: 14,
                nickname: "超超",
                sex: 1,
                signNum: 0,
                time: "早上",
                type: "0",
                identityMark: '',
                isDating: 0
            }],
            page: 1,
            size: 30,
            woman: '../../image/station/woman-icon.png',
            man: '../../image/station/man-icon.png',
        },
        created: function () {
            this.list = [];
        },
        methods: {
            onPublishTap: function () {
                api && api.actionSheet({
                    title: '广播发布',
                    cancelTitle: '取消',
                    buttons: ['报名约会广播']
                }, function (ret, err) {
                    var index = ret.buttonIndex;
                    if (index === 1) {
                        // _g.openWin({
                        //     header: {
                        //         data: {
                        //             title: '发布普通约会广播',
                        //         },
                        //         template: 'common/header-base-V',
                        //     },
                        //     name: 'station-publish-win',
                        //     url: '../station/publish.html',
                        //     bounces: true,
                        //     slidBackEnabled: false,
                        //     pageParam: {

                        //     }
                        // });
                        _g.openWin({
                            header: {
                                data: {
                                    title: '发布报名约会广播',
                                },
                                template: 'common/header-base-V',
                            },
                            name: 'station-signUp-win',
                            url: '../station/signUp.html',
                            bounces: false,
                            slidBackEnabled: false,
                            pageParam: {}
                        });
                    }
                    // else if (index === 2) {
                    //     _g.openWin({
                    //         header: {
                    //             data: {
                    //                 title: '发布报名约会广播',
                    //             },
                    //             template: 'common/header-base-V',
                    //         },
                    //         name: 'station-signUp-win',
                    //         url: '../station/signUp.html',
                    //         bounces: true,
                    //         slidBackEnabled: false,
                    //         pageParam: {

                    //         }
                    //     });
                    // }
                })

            },
            onSignUpTap: function (data) {
                if (data.isSigner || data.turnSigned) {
                    _g.toast('您已报名成功，无需重复报名');
                    return
                }
                if (data.userId === UserInfo.id) {
                    _g.toast('不能报名自己的广播哦！');
                    return
                }
                if (data.sex === UserInfo.sex) {
                    _g.toast('只能报名异性的广播哦！');
                    return
                }
                api.confirm({
                    msg: '确定报名该约会？',
                    buttons: ['取消', '确定']
                }, function (ret, err) {
                    if (ret.buttonIndex === 2) {
                        Http.ajax({
                            data: {
                                broadCastId: data.id,
                                signerId: UserInfo.id
                            },
                            isSync: false,
                            url: '/broadCast/signUp',
                            success: function (ret) {
                                if (ret.code == 200) {
                                    _g.toast('报名成功！');
                                    data.turnSigned = true;
                                } else {
                                    _g.toast(ret.msg);
                                }
                            },
                            error: function (err) {
                                _g.toast(err.msg);
                            }
                        });
                    }
                })
            },
            onLikeTap(data, index) {
                if (data.isLiker === 1 || data.likeTap) return
                Http.ajax({
                    data: {
                        broadCastId: data.id,
                        likerId: UserInfo.id
                    },
                    isSync: false,
                    url: '/broadCast/like',
                    success: function (ret) {
                        if (ret.code == 200) {
                            data.likeTap = true;
                            data.likeNum++;
                            _g.toast('点赞成功！')
                        } else {
                            _g.toast(ret.msg);
                        }
                    },
                    error(err) {
                        _g.toast(err.msg);
                    }
                });
            },
            onApplierTap: function (data) {
                if (data.userId !== UserInfo.id) {
                    _g.toast('不能查看别人的报名情况哦');
                    return
                }
                _g.openWin({
                    header: {
                        data: {
                            title: '已报名名单',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'station-applier-win',
                    url: '../station/applier.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        broadCastId: data.id
                    }
                });
            },
            onCityTap: function () {
                UIActionSelector.open({
                    datas: 'widget://res/Regions-all.json', //拿到APP原本省市县数据
                    layout: {
                        row: 5,
                        col: 2,
                        height: 32,
                        size: 12,
                        sizeActive: 14,
                        rowSpacing: 5,
                        colSpacing: 10,
                        maskBg: 'rgba(0,0,0,0.2)',
                        bg: '#fff',
                        color: '#888',
                        colorActive: '#ea39b9',
                        colorSelected: '#ea39b9'
                    },
                    animation: true,
                    cancel: {
                        text: '取消',
                        size: 12,
                        w: 90,
                        h: 35,
                        bg: '#ccc',
                        bgActive: '#ccc',
                        color: '#fff',
                        colorActive: '#fff'
                    },
                    ok: {
                        text: '确定',
                        size: 12,
                        w: 90,
                        h: 35,
                        bg: '#571A92',
                        bgActive: '#ccc',
                        color: '#fff',
                        colorActive: '#fff'
                    },
                    title: {
                        text: '请选择省市',
                        size: 12,
                        h: 53,
                        bg: '#eee',
                        color: '#571A92'
                    },
                    fixedOn: 'station-index-frame',
                }, function (ret, err) {
                    if (ret && ret.eventType == 'ok') {
                        station.province = ret.level1;
                        station.city = ret.level2;
                        if (ret.level2 === '全部') station.city = ''
                        if (ret.level2 === '附近') station.city = UserInfo.city
                        station.page = 1;
                        station.list = [];
                        getData();
                    } else {
                        UIActionSelector.close();
                    }
                    if (err) {
                        _g.toast('系统出错,请稍后再试!');
                    }
                });
            },
            checkInfo(data) {
                if (data.userId === UserInfo.id) {
                    api && api.sendEvent({
                        name: 'check-own-info'
                    })
                    return
                }
                if (data.sex === UserInfo.sex) {
                    _g.toast('不能查看同性的资料喔')
                    return
                }
                _g.openWin({
                    header: {
                        data: {
                            title: data.nickname + '的详情',
                        },
                        template: 'common/header-other-V',
                    },
                    name: 'other-index',
                    url: '../other/index.html',
                    bounces: false,
                    slidBackEnabled: false,
                    pageParam: {
                        checkUserId: data.userId
                    }
                });
            },
            delCity() {
                if (!this.city) return
                this.city = ''
                station.page = 1;
                station.list = [];
                getData();
            },
            openImage(images, index) {
                if (images[index].forbidden) return
                var imgArr = [];
                images.forEach(function (img) {
                    imgArr.push(img.url)
                })
                var data = {
                    images: imgArr,
                    placeholderImg: 'widget://res/img/apicloud.png',
                    bgColor: '#000',
                    activeIndex: index
                };
                photoBrowser.open(data, function (ret, err) {
                    if (ret) {
                        if (ret.eventType === 'click') {
                            photoBrowser.close()
                        }
                    } else {
                        _g.toast('图片打开失败')
                    }
                })
            }
        },
    });

    // 获取数据
    function getData() {
        Http.ajax({
            data: {
                sortKey: station.sortKey,
                sex: '',
                city: station.city,
                page: station.page,
                size: station.size,
                userId: UserInfo.id
            },
            method: 'get',
            isSync: false,
            url: '/broadCast/findList',
            success: function (ret) {
                if (ret.code == 200) {
                    station.list = station.list.concat(getDetail(ret.data))
                }
            }
        });
    }

    function getDetail(result) {
        var list = result ? result : [];
        return _.map(list, function (item) {
            return {
                likeTap: item.isLiker || false,
                turnSigned: item.isSigner || false,
                avatar: item.avatar || null,
                city: item.city || "",
                createTime: item.createTime || "2018-,12-12 09:09:09",
                date: item.date || "2018-,12-12",
                description: item.description || "",
                id: item.id || 0,
                identity: item.identity || 0,
                images: item.images || [],
                isLiker: item.isLiker || 0,
                isSigner: item.isSigner || 0,
                likeNum: item.likeNum || 0,
                nickname: item.nickname || "未命名",
                sex: item.sex,
                signNum: item.signNum || 0,
                time: item.time || "早上",
                type: item.type || "0",
                userId: item.userId || 0,
                isDating: item.isDating,
                identityMark: formatIdentityMark(item)
            }
        });
    };

    function formatIdentityMark(data) {
        var res = '';
        if (data.sex == 0) {
            if (data.identity == 1) {
                res = '会员'
            } else {
                res = '非会员'
            }
        }
        if (data.sex == 1) {
            if (data.identity == 2) {
                res = '真实'
            } else {
                res = '未认证'
            }
        }
        return res
    }

    function str2arr(res) {
        if (!res) return []
        var images = res.split(',');
        console.log(images);
        return images;
    }
    getData();
    api && api.addEventListener({
        name: 'station-index-main',
    }, function (ret, err) {
        station.page = 1;
        station.list = [];
        getData();
    });

    api && api.addEventListener({
        name: 'station-sortKey'
    }, function (ret, err) {
        station.sortKey = ret.value.sortKey;
        station.page = 1;
        station.list = [];
        getData();
    });
    // api && api.addEventListener({
    //     name: 'station-sex'
    // }, function (ret, err) {
    //     station.sex = ret.value.sex;
    //     station.page = 1;
    //     station.list = [];
    //     getData();
    // });

    api && api.addEventListener({
        name: 'radio-index-save'
    }, function (ret, err) {
        station.onPublishTap();
    });

    // 下拉刷新
    _g.setPullDownRefresh(function () {
        station.page = 1;
        station.list = [];
        getData();
    });

    _g.setLoadmore(function () {
        var length = station.list.length
        if (station.page * station.size === length) {
            station.page++;
            getData();
        }
    });

    module.exports = {};

});