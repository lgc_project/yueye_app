define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var broadCastId = api && api.pageParam.broadCastId;
    var applier = new Vue({
        el: '#applier',
        template: _g.getTemplate('station/applier-main-V'),
        data: {
            list: [{
                nickname: '未知',
                id: -1,
                avatar: '',
                isDated: 0
            }],
            hasDated: 0
        },
        created: function () {},
        methods: {
            onOtherTap: function (item) {
                _g.openWin({
                    header: {
                        data: {
                            title: item.nickname + '的详情',
                        },
                        template: 'common/header-other-V',
                    },
                    name: 'other-index',
                    url: '../other/index.html',
                    bounces: false,
                    slidBackEnabled: false,
                    pageParam: {
                        checkUserId: item.id
                    }
                });
            },
            onLineOneTap: function (index) {
                winUrl(index);
            },
            dateConfirm(item) {
                api && api.confirm({
                    msg: '确定约TA吗？',
                    buttons: ['确定', '取消']
                }, function (ret, err) {
                    if (ret.buttonIndex === 1) confirm2Date(item);
                })
            }
        },
    });

    function confirm2Date(item) {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                signerId: item.id
            },
            isSync: false,
            url: '/broadCast/dateSomeone',
            success: function (ret) {
                if (ret.code == 200) {
                    item.isDated = 1
                    applier.hasDated = 1
                }
            },
            error: function (err) {
                _g.toast('网络异常');
            }
        });
    }

    // 获取个人资料接口
    function getData() {
        Http.ajax({
            data: {
                broadCastId: broadCastId,
                page: 1,
                size: 999
            },
            method: 'GET',
            isSync: false,
            url: '/broadCast/findSignerList',
            success: function (ret) {
                if (ret.code == 200) {
                    if (ret.data.length) {
                        ret.data.forEach(function (ele) {
                            if (ele.isDated) {
                                applier.hasDated = 1
                            }
                        });
                    }
                    applier.list = ret.data
                }
            },
            error: function (err) {
                _g.toast(err.msg);
            }
        });
    };
    getData();
    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getData();
    });

    module.exports = {};

});