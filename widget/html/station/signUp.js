define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var UIActionSelector = api.require('UIActionSelector');
    var weiXin = api.require('weiXin');
    var UIMediaScanner = api.require('UIMediaScanner');
    var signUp = new Vue({
        el: '#signUp',
        template: _g.getTemplate('station/signUp-main-V'),
        data: {
            city: '',
            cityTemp: '点击选择城市',
            date: '',
            time: '',
            description: '',
            images: [],
            type: 1,
            selectImageNum: 1,
            money: 1,
        },
        created: function () {

        },
        methods: {
            onCityTab: function () {
                UIActionSelector.open({
                    datas: 'widget://res/Regions.json', //拿到APP原本省市县数据
                    layout: {
                        row: 5,
                        col: 2,
                        height: 32,
                        size: 12,
                        sizeActive: 14,
                        rowSpacing: 5,
                        colSpacing: 10,
                        maskBg: 'rgba(0,0,0,0.2)',
                        bg: '#fff',
                        color: '#888',
                        colorActive: '#ea39b9',
                        colorSelected: '#ea39b9'
                    },
                    animation: true,
                    cancel: {
                        text: '取消',
                        size: 12,
                        w: 90,
                        h: 35,
                        bg: '#ccc',
                        bgActive: '#ccc',
                        color: '#fff',
                        colorActive: '#fff'
                    },
                    ok: {
                        text: '确定',
                        size: 12,
                        w: 90,
                        h: 35,
                        bg: '#571A92',
                        bgActive: '#ccc',
                        color: '#fff',
                        colorActive: '#fff'
                    },
                    title: {
                        text: '请选择省市',
                        size: 12,
                        h: 53,
                        bg: '#eee',
                        color: '#571A92'
                    },
                }, function (ret, err) {
                    if (ret && ret.eventType == 'ok') {
                        signUp.city = ret.level2;
                    } else {
                        UIActionSelector.close();
                    }
                    if (err) {
                        _g.toast('系统出错,请稍后再试!');
                    }
                });
            },
            onTimeTap: function () {
                api && api.actionSheet({
                    title: '时间',
                    cancelTitle: '取消',
                    buttons: ['上午', '中午', '晚上', '通宵', '一整天']
                }, function (ret, err) {
                    switch (ret.buttonIndex) {
                        case 1:
                            signUp.time = '上午'
                            break;
                        case 2:
                            signUp.time = '中午'
                            break;
                        case 3:
                            signUp.time = '晚上'
                            break;
                        case 4:
                            signUp.time = '通宵'
                            break;
                        case 5:
                            signUp.time = '一整天'
                            break;
                        default:
                            break;
                    }
                })
            },
            deleImg: function (index) {
                this.images.splice(index, 1);
            },
            addImg: function () {
                if (this.images.length >= 6) {
                    _g.toast('最多6张');
                    return
                }
                UIMediaScanner.open({
                    max: 6 - this.images.length,
                    type: 'picture',
                    bg: '#000',
                    texts: {
                        stateText: '已选择*项', //（可选项）字符串类型；状态文字内容；*号会被替换为已选择个数；默认：'已选择*项'
                        cancelText: '取消', //（可选项）字符串类型；取消按钮文字内容；默认：'取消'
                        finishText: '确定', //（可选项）字符串类型；完成按钮文字内容；默认：'完成'
                        selectedMaxText: '只能选择*个了！', //（可选项）字符串类型；最多显示提示语，*号会被替换为已选择个数;默认：'最多显示*个资源'
                        classifyTitle: '系统相册'
                    },
                    styles: {
                        bg: '#fff',
                        mark: {
                            icon: '',
                            position: 'bottom_left',
                            size: 26
                        },
                        nav: {
                            bg: '#000',
                            stateColor: '#fff',
                            stateSize: 18,
                            cancelBg: 'rgba(0,0,0,0)',
                            cancelColor: '#fff',
                            cancelSize: 18,
                            finishBg: 'rgba(0,0,0,0)',
                            finishColor: '#fff',
                            finishSize: 18
                        }
                    },
                    exchange: true,
                }, function (ret) {
                    if (ret.eventType === 'confirm') {
                        if (ret.list.length) {
                            var list = ret.list;
                            signUp.selectImageNum = list.length
                            list.forEach(function (ele) {
                                _g.showProgress();
                                postAvatar(ele.path)
                            })
                        }
                    } else if (ret.eventType === 'albumError') _g.toast('访问相册失败')
                })
            },
            handleSend: function () {
                if (!this.city || !this.date || !this.time) {
                    _g.toast('请完善信息');
                    return
                }
                if (UserInfo.sex === 0 && UserInfo.identity === 1 || UserInfo.sex === 1 && UserInfo.identity === 2) {
                    handleSend()
                } else {
                    var identity = UserInfo.sex === 1 ? '未认证' : '非会员';
                    api && api.confirm({
                        msg: identity + '发布需要支付金额',
                        buttons: ['确定', '取消']
                    }, function (ret, err) {
                        if (ret.buttonIndex === 1) {
                            getWechatOrder();
                        }
                    })
                }
            }
        },
    });

    function handleSend() {
        var images = '';
        if (signUp.images.length) images = signUp.images.join(',');
        Http.ajax({
            data: {
                userId: UserInfo.id,
                type: signUp.type,
                city: signUp.city,
                date: signUp.date,
                time: signUp.time,
                images: images,
                description: signUp.description
            },
            isSync: true,
            url: '/broadCast/saveBroadCast',
            success: function (ret) {
                if (ret.code == 200 || ret.code == '200') {
                    _g.toast('发布成功');
                    api.sendEvent({
                        name: 'station-index-main',
                    });
                    setTimeout(function () {
                        api && api.closeWin();
                    }, 300);
                } else {
                    _g.toast(ret.codeDesc);
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    }

    function getWechatOrder() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                payMoney: Number(signUp.money) * 100,
                type: 3
            },
            isSync: true,
            url: '/pay/getWxPayOrder',
            success: function (ret) {
                if (ret.code == 200) {
                    postWechatRecharge(ret.data);
                } else {
                    alert(_g.j2s(ret));
                }
            }
        });
    };

    function postWechatRecharge(data, redImg) {
        weiXin.payOrder({
            key: data.appid,
            orderId: data.prepayid,
            partnerId: data.partnerid,
            nonceStr: data.noncestr,
            timeStamp: data.timestamp,
            sign: data.sign,
            package: data.package
        }, function (ret, err) {
            if (ret.status) {
                handleSend()
            } else {
                api.alert('支付失败');
            }
        });
    };

    function getBroadcastAmout() {
        Http.ajax({
            data: {
                key: 'BROADCAST_AMOUNT'
            },
            method: 'get',
            isSync: true,
            url: '/sys/config/findByKey',
            success: function (ret) {
                if (ret.code === 200 || ret.code === '200') {
                    signUp.money = ret.data || 10
                }
            }
        });
    }

    getBroadcastAmout();

    function postAvatar(img) {
        api.ajax({
            url: CONFIG.HOST + '/common/uploadPhoto',
            method: 'post',
            isSync: true,
            lock: false,
            data: {
                values: {
                    token: UserInfo.token
                },
                files: {
                    image: img,
                }
            }
        }, function (ret, err) {
            if (ret.code == 200) {
                _g.hideProgress();
                signUp.images.push(ret.data);
            } else {
                _g.toast(ret.msg);
                _g.hideProgress();
            }
        });
    }

    module.exports = {};

});