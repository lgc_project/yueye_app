define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    if(!UserInfo) return false;
    var page = 1;
    var pageSize = 10;
    var Http = require('U/http');
    var boyHot = new Vue({
        el: '#boyHot',
        template: _g.getTemplate('people/boyHot-main-V'),
        data: {
            boyList: [{
                avatar: '',
                tap: 'VIP',
                like: false,
                name: '郑李陆',
                area: '广州市',
                year: '23',
                occupation: '厨师',
                distance: '2.2km',
                height: '',
                online: '当前在线',
                avatarStatus: true
            }],
            nodata: false
        },
        created: function() {
            this.boyList= []
        },
        methods: {
            onPoepleTap: function(index) {
                var otherId = this.boyList[index].id;
                if($(event.target).closest('.ui-boy-heart').length > 0) {
                    if(UserInfo.sex == 0) {
                        _g.toast('同性不能关注哦');
                        return;
                    }
                    followOthers(otherId, index)
                } else {
                    if(UserInfo.sex == 0) {
                        _g.toast('同性不能查看哦');
                        return;
                    }
                    accessOthers(otherId)
                }
            }
        },
    });
    // 获取个人资料接口
    function getData() {
        if(page == 1) {
            _g.showProgress();
        }
        boyHot.nodata = false;
        var peopleCity = _g.getLS('peopleCity');
        Http.ajax({
            data: {
                userId: UserInfo.id,
                province: peopleCity.province,
                city: peopleCity.city,
                sex: 0,
                type: 1,
                page: page,
                pageSize: pageSize
            },
            isSync: false,
            lock: false,
            url: '/user/getAllUserInfo',
            success: function(ret) {
                if(ret.code == 200) {
                    // alert(_g.j2s(ret));
                    var data = ret.data;
                    if(data.length == 0) {
                        if(page == 1) {
                            boyHot.boyList = [];
                            boyHot.nodata = true;
                        } else {
                            _g.toast('没有更多热门男士哦');
                        }
                    } else {
                        setTimeout(function() {
                            if(page == 1) {
                                boyHot.boyList = getDetail(data);
                            } else {
                                boyHot.boyList = boyHot.boyList.concat(getDetail(data));
                            }
                        }, 0);
                    }
                } else {
                    _g.toast('获取用户信息失败')
                }
                setTimeout(function() {
                    _g.hideProgress();
                }, 500);
            }
        });
    };
    function getDetail(result) {
        var list = result ? result : [];
        return _.map(list, function(item) {
            return {
                id: item.id || '',
                sex: item.sex || 1,
                avatar: item.avatar || '../../image/station/station-img.png',
                tap: item.relationship || '',
                name: item.nickname || '',
                area: item.city || '',
                year: item.age || '',
                occupation: item.occupation || '',
                distance: item.distance > 0 ? (item.distance / 1000).toFixed(2) + 'km' : '',
                online: item.online || '',
                height: item.height > 100 ? item.height + 'cm' :  item.height + 'm' || '',
                isfollow: item.isfollow || false,
                isVip: item.identity == 1 ? true : false,
                avatarStatus: item.avatarStatus
            }
        });
    };
    function followOthers(followId, index) {
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: followId,
                follow: !boyHot.boyList[index].isfollow
            },
            isSync: false,
            url: '/relationship/follow',
            success: function(ret) {
                if(ret.code == 200) {
                    // api && api.sendEvent({
                    //     name: 'people-people-reload'
                    // });
                    boyHot.boyList[index].isfollow = !boyHot.boyList[index].isfollow
                    if(boyHot.boyList[index].isfollow) {
                        _g.toast('关注用户成功')
                    } else {
                        _g.toast('取消关注用户成功')
                    }
                    // var data = ret.data;
                } else {
                    _g.toast('关注用户失败')
                }
            }
        });
    };
    // 记录历史访客
    function accessOthers(otherId) {
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: otherId
            },
            isSync: false,
            url: '/relationship/access',
            success: function(ret) {
                if(ret.code == 200) {
                    _g.openWin({
                        header: {
                            data: {
                                title: '男士详情'
                            },
                            template: 'common/header-other-V',
                        },
                        name: 'other-index',
                        url: '../other/index.html',
                        bounces: false,
                        slidBackEnabled: false,
                        pageParam: {
                            checkUserId: otherId
                        }
                    });
                }
            }
        });
    }
    // 上拉翻页
    _g.setLoadmore(function(ret, err) {
        page++;
        getData();
    });
    // 下拉刷新
    _g.setPullDownRefresh(function() {
        getData();
    });
    // 选择城市更新用户
    api && api.addEventListener({
        name: 'people-people-reload',
    }, function (ret, err) {
        getData();
    });
    getData();
    
    module.exports = {};

});
