define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var searchData =api && api.pageParam.searchData;
    var Http = require('U/http');
    var searchResult = new Vue({
        el: '#searchResult',
        template: _g.getTemplate('people/searchResult-main-V'),
        data: {
            girlList: [{
                tap: '真实',
                name: '李陆姑娘',
                area: '广州市',
                year: '23',
                charact: '厨师',
                distance: '2.2km',
                online: '当前在线',
                avatarStatus: true
            }],
            nodata: false
        },
        created: function () {
            this.girlList= []
        },
        methods: {
            onPoepleTap: function(index) {
                var otherId = this.girlList[index].id;
                if($(event.target).closest('.ui-heart').length > 0) {
                    followOthers(otherId, index)
                } else {
                    accessOthers(otherId)
                }
            }
        },
    });
    // 获取用户列表接口
    function getData() {
        // alert(_g.j2s(searchData));
        searchResult.nodata = false;
        Http.ajax({
            data: searchData,
            isSync: true,
            url: '/user/searchUserInfo',
            success: function(ret) {
                // alert(_g.j2s(ret));
                if(ret.code == 200) {
                    var data = ret.data;
                    if(data.length == 0) {
                        searchResult.girlList = [];
                        searchResult.nodata = true;
                        // _g.toast('热门女士没有数据哦');
                    } else {
                        searchResult.girlList = getDetail(data)
                    }
                } else {
                    _g.toast('搜索女性用户信息失败')
                }
            }
        });
    };
    function getDetail(result) {
        var list = result ? result : [];
        return _.map(list, function(item) {
            return {
                id: item.id || '',
                sex: item.sex || 1,
                image: item.avatar || '',
                tap: item.relationship || '',
                name: item.nickname || '',
                area: item.city || '',
                year: item.age || '',
                charact: item.style || '',
                distance: item.distance || '',
                online: item.online || '',
                isfollow: item.isfollow || false,
                avatarStatus: item.avatarStatus
            }
        });
    };
    function followOthers(followId, index) {
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: followId,
                follow: !searchResult.girlList[index].isfollow
            },
            isSync: false,
            url: '/relationship/follow',
            success: function(ret) {
                if(ret.code == 200) {
                    api && api.sendEvent({
                        name: 'people-people-reload'
                    });
                    searchResult.girlList[index].isfollow = !searchResult.girlList[index].isfollow
                    if(searchResult.girlList[index].isfollow) {
                        _g.toast('关注用户成功')
                    } else {
                        _g.toast('取消关注用户成功')
                    }
                    // var data = ret.data;
                } else {
                    _g.toast('关注用户失败')
                }
            }
        });
    };
    // 记录历史访客
    function accessOthers(otherId) {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                passiveId: otherId
            },
            isSync: false,
            url: '/relationship/access',
            success: function(ret) {
                if(ret.code == 200) {
                    _g.openWin({
                        header: {
                            data: {
                                title: '女士详情'
                            },
                            template: 'common/header-other-V',
                        },
                        name: 'other-index',
                        url: '../other/index.html',
                        bounces: false,
                        slidBackEnabled: false,
                        pageParam: {
                            checkUserId: otherId
                        }
                    });
                }
            }
        });
    };
    getData();

    module.exports = {};

});