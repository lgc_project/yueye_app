define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var windowHeight = window.innerHeight;
    var WinParam = _g.getLS('appH');
    var topHeight = 0;
    var UIActionSelector = api.require('UIActionSelector');
    var Http = require('U/http');
    var hear = new Vue({
        el: '#hear',
        template: _g.getTemplate('people/hear-main-V'),
        data: {
            isGirl: true,
            girlIndex: 0,
            boyIndex: 0,
            girlTop: [{
                title: '热门',
                select: true
            }, {
                title: '新来',
                select: false
            }, {
                title: '认证',
                select: false
            }],
            boyTop: [{
                title: '热门男士',
                select: true
            }, {
                title: '付费会员',
                select: false
            }]
        },
        created: function() {
            topHeight = $('.ui-top').height();
        },
        ready: function() {
            topHeight = $('.ui-top').height();
        },
        methods: {
            onGirlTopTap: function(index) {
                this.girlTop[this.girlIndex].select = false;
                this.girlTop[index].select = true;
                this.girlIndex = index;
                api.setFrameGroupIndex({
                    name: 'girlList',
                    index: index,
                    scroll: true
                });
            },
            onBoyTopTap: function(index) {
                this.boyTop[this.boyIndex].select = false;
                this.boyTop[index].select = true;
                this.boyIndex = index;
                api.setFrameGroupIndex({
                    name: 'boyList',
                    index: index,
                    scroll: true
                });
            },
            onLikeBoyTap: function(index) {
                this.boyList[index].like = !this.boyList[index].like;
            },
            onLineOneTap: function(index) {
                winUrl(index);
            }
        },
    });
    // 打开女士列表
    function openGirlFrame() {
        var param = _g.getLS('peopleCity');
        if(!param) {
            var data = {
                province: '',
                city: ''
            };
            _g.setLS('peopleCity', data);
        }
        api.openFrameGroup({
            name: 'girlList',
            rect: {
                x: 0,
                y: WinParam.header + topHeight,
                w: 'auto',
                h: WinParam.win - WinParam.header - WinParam.footer - topHeight
            },
            frames: [{
                name: 'people-girlHot',
                url: './girlHot.html',
                bounces: true,  // 是否弹动
            }, {
                name: 'people-girlNew',
                url: './girlNew.html',
                bounces: true,  // 是否弹动
            }, {
                name: 'people-girlAdmit',
                url: './girlAdmit.html',
                bounces: true,  // 是否弹动
            } ]
        }, function(ret, err) {
            UIActionSelector.close();
            var index = ret.index;
            // 头部一起更换
            hear.girlTop[hear.girlIndex].select = false;
            hear.girlTop[index].select = true;
            hear.girlIndex = index;
        });
    }
    // 打开男士列表
    function openBoyFrame() {
        var param = _g.getLS('peopleCity');
        if(!param) {
            var data = {
                province: '',
                city: ''
            };
            _g.setLS('peopleCity', data);
        }
        api.openFrameGroup({
            name: 'boyList',
            rect: {
                x: 0,
                y: WinParam.header + topHeight,
                w: 'auto',
                h: WinParam.win - WinParam.header - WinParam.footer - topHeight
            },
            frames: [{
                name: 'people-boyHot',
                url: './boyHot.html',
                bounces: true,  // 是否弹动
            }, {
                name: 'people-boyVip',
                url: './boyVip.html',
                bounces: true,  // 是否弹动
            } ]
        }, function(ret, err) {
            UIActionSelector.close();
            var index = ret.index;
            // 头部一起更换
            hear.boyTop[hear.boyIndex].select = false;
            hear.boyTop[index].select = true;
            hear.boyIndex = index;
        });
    };
    function selectCity() {
        var frameName = '';
        if(hear.isGirl) {
            if(hear.girlIndex == 0) {
                frameName = 'people-girlHot';
            }
            if(hear.girlIndex == 1) {
                frameName = 'people-girlNew';
            }
            if(hear.girlIndex == 2) {
                frameName = 'people-girlAdmit';
            }
        } else {
            if(hear.boyIndex == 0) {
                frameName = 'people-boyHot';
            }
            if(hear.boyIndex == 1) {
                frameName = 'people-boyVip';
            }
        }
        UIActionSelector.open({
            datas: 'widget://res/Regions-all.json', //拿到APP原本省市县数据
            layout: {
                row: 5,
                col: 2,
                height: 32,
                size: 12,
                sizeActive: 14,
                rowSpacing: 5,
                colSpacing: 10,
                maskBg: 'rgba(0,0,0,0.2)',
                bg: '#fff',
                color: '#888',
                colorActive: '#ea39b9',
                colorSelected: '#ea39b9'
            },
            iPhoneXBottomHeight: 0,
            animation: true,
            cancel: {
                text: '取消',
                size: 12,
                w: 90,
                h: 35,
                bg: '#ddd',
                bgActive: '#ccc',
                color: '#fff',
                colorActive: '#fff'
            },
            ok: {
                text: '确定',
                size: 12,
                w: 90,
                h: 35,
                bg: '#571A92',
                bgActive: '#ccc',
                color: '#fff',
                colorActive: '#fff'
            },
            title: {
                text: '请选择省市',
                size: 12,
                h: 53,
                bg: '#eee',
                color: '#571A92'
            },
            fixedOn: frameName,
        }, function (ret, err) {
            if (ret && ret.eventType == 'ok') {
                var data = {};
                if(ret.level1 == '全部') {
                    data = {
                        province: '',
                        city: ''
                    };
                } else {
                    hear.province = ret.level1;
                    hear.city = ret.level2;
                    data = {
                        province: ret.level1,
                        city: ret.level2
                    };
                }
                _g.setLS('peopleCity', data);
                api && api.sendEvent({
                    name: 'people-people-reload'
                });
                // 监听性别按钮事件
                api && api.sendEvent({
                    name: 'main-index-position',
                    extra: {
                        city: ret.level2
                    }
                });
            } else {
                UIActionSelector.close();
            }
            if (err) {
                _g.toast('系统出错,请稍后再试!');
                UIActionSelector.close();
            }
        });
    };
    // 监听性别按钮事件
    api && api.addEventListener({
        name: 'people-people-sex'
    }, function(ret, err) {
        hear.isGirl = ret.value.isBoy;
        if(hear.isGirl) {
            api.closeFrameGroup({
                name: 'boyList'
            });
            openGirlFrame();
        } else {
            api.closeFrameGroup({
                name: 'girlList'
            });
            openBoyFrame();
        }
    });
    // 监听底部按钮事件打开弹窗
    api && api.addEventListener({
        name: 'people-hear-open'
    }, function(ret, err) {
        if(hear.isGirl) {
            openGirlFrame();
        } else {
            openBoyFrame();
        }
    });
    // 监听底部按钮事件关闭弹窗
    api && api.addEventListener({
        name: 'people-hear-close'
    }, function(ret, err) {
        if(hear.isGirl) {
            api.closeFrameGroup({
                name: 'girlList'
            });
        } else {
            api.closeFrameGroup({
                name: 'boyList'
            });
        }
    });
    // 监听获取附近的人列表
    api && api.addEventListener({
        name: 'people-people-near',
    }, function (ret, err) {
        selectCity();
        // _g.toast('附近');
    });
    // 监听获取搜索用户列表
    api && api.addEventListener({
        name: 'people-people-search',
    }, function (ret, err) {
        // searchPeople();
        var UserInfo = _g.getLS('UserInfo');
        if(UserInfo.identity == 1) {
            _g.openWin({
                header: {
                    data: {
                        title: '条件搜索'
                    },
                    template: 'common/header-base-V',
                },
                name: 'people-search',
                url: '../people/search.html',
                bounces: false,
                slidBackEnabled: false,
                pageParam: {
                }
            });
        } else {
            _g.toast('会员专享哦');
        }
    });

    if (UserInfo.sex) {
        // 监听性别按钮事件
        api && api.sendEvent({
            name: 'people-people-sex',
            extra: {
                isBoy: false
            }
        });
    } else {
        // 监听性别按钮事件
        api && api.sendEvent({
            name: 'people-people-sex',
            extra: {
                isBoy: true
            }
        });
    }
    
    module.exports = {};

});
