define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var UIMultiSelector = api.require('UIMultiSelector');
    var Http = require('U/http');
    var search = new Vue({
        el: '#search',
        template: _g.getTemplate('people/search-main-V'),
        data: {
            nickname: '',
            age: '',
            occupation: '',
            height: '',
            weight: '',
            style: '',
            cupSize: '',
            language: '',
            relationship: '',
            datingProgram: '',
            datingCodition: '',
            ageList: ['18以下', '18-20', '21-25', '26-30', '31-35', '36-40', '40-50', '50+'],
            occupationList: ['老板', '主播', '白领OL', '学生', '设计师', '程序员'],
            heightList: ['140以下', '141-150', '151-160', '161-170', '171-180', '180+'],
            weightList: ['40以下', '41-50', '51-60', '61-70', '71-80', '80+'],
            styleList: ['黑丝', '长筒靴', '吊带袜', '蕾丝', '超短裙', '牛仔裤'],
            languageList: ['方言', '普通话', '粤语', '英语', '日语'],
            cupList: ['A', 'B', 'C', 'D', 'E+'],
            relationshipList: ['单身', '有男朋友', '已婚', '离异', '求包养'],
            datingProgramList: ['KTV', '酒吧', '逛街', '电影', '吃饭', '运动', '其他']
        },
        created: function () {
        },
        methods: {
            onAgeListTap: function() {
                api && api.actionSheet({
                    title: '年龄选择',
                    buttons: search.ageList
                }, function (ret, err) {
                    search.age = search.ageList[ret.buttonIndex - 1]
                })
            },
            onHeightListTap: function() {
                api && api.actionSheet({
                    title: '身高选择',
                    buttons: search.heightList
                }, function (ret, err) {
                    search.height = search.heightList[ret.buttonIndex - 1]
                })
            },
            onWeightListTap: function() {
                api && api.actionSheet({
                    title: '体重选择',
                    buttons: search.weightList
                }, function (ret, err) {
                    search.weight = search.weightList[ret.buttonIndex - 1]
                })
            },
            onCupSizeListTap: function() {
                api && api.actionSheet({
                    title: '罩杯选择',
                    buttons: search.cupList
                }, function (ret, err) {
                    search.cupSize = search.cupList[ret.buttonIndex - 1]
                })
            },
            onOccupationListTap: function() {
                api && api.actionSheet({
                    title: '职业选择',
                    buttons: search.occupationList
                }, function (ret, err) {
                    search.occupation = search.occupationList[ret.buttonIndex - 1]
                })
            },
            onStyleListTap: function() {
                api && api.actionSheet({
                    title: '打扮风格选择',
                    buttons: search.styleList
                }, function (ret, err) {
                    search.style = search.styleList[ret.buttonIndex - 1]
                })
            },
            onLanguageListTap: function() {
                api && api.actionSheet({
                    title: '语言选择',
                    buttons: search.languageList
                }, function (ret, err) {
                    search.language = search.languageList[ret.buttonIndex - 1]
                })
            },
            onRelationshipListTap: function() {
                api && api.actionSheet({
                    title: '情感选择',
                    buttons: search.relationshipList
                }, function (ret, err) {
                    search.relationship = search.relationshipList[ret.buttonIndex - 1]
                })
            },
            onDatingProgramTap: function() {
                api && api.actionSheet({
                    title: '约会节目选择',
                    buttons: search.datingProgramList
                }, function (ret, err) {
                    search.datingProgram = search.datingProgramList[ret.buttonIndex - 1]
                })
            },
            onDatingConditionTap: function() {
                var bustList = ['跟钱没关系', '收费约会']
                api && api.actionSheet({
                    title: '约会条件',
                    buttons: bustList
                }, function (ret, err) {
                    if (ret.buttonIndex >= 1 || ret.buttonIndex <= 2) search.datingCodition = bustList[ret.buttonIndex - 1]
                })
            },
            onSearchTap: function() {
                if(search.nickname == '' && search.age == '' && search.height == '' && search.weight == '' && search.style == '' && search.occupation == '' && search.cupSize == '' && search.language == '' && search.relationship == '' && search.datingProgram == '' && search.datingCodition == '') {
                    _g.toast('至少选择一个搜索条件哦');
                    return;
                }
                var searchData = {
                    nickname: search.nickname,
                    age: dealWithData(search.age),
                    height: dealWithData(search.height),
                    weight: dealWithData(search.weight),
                    style: search.style,
                    occupation: search.occupation,
                    cupSize: search.cupSize,
                    language: search.language,
                    relationship: search.relationship,
                    datingProgram: search.datingProgram,
                    datingCodition: search.datingCodition
                };
                _g.openWin({
                    header: {
                        data: {
                            title: '搜索女士结果'
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'people-searchResult',
                    url: '../people/searchResult.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {
                        searchData: searchData
                    }
                });
            }
        },
    });
    function dealWithData(param) {
        switch(param) {
            case ('18以下'):
                return('0-18');
                break;
            case ('50+'):
                return('50-100');
                break;
            case ('140以下'):
                return('0-140');
                break;
            case ('180+'):
                return('180-200');
                break;
            case ('40以下'):
                return('0-40');
                break;
            case ('80+'):
                return('80-120');
                break;
            default:
                return(param);
                break;
        }
    }

    module.exports = {};

});