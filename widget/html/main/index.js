define(function (require, exports, module) {
    var headerHeight = 0;
    var footerHeight = 0;
    var windowHeight = window.innerHeight;
    var UserInfo = _g.getLS('UserInfo');
    var systemType = api.systemType; // 系统类型，字符串类型
    var bMap = api.require('bMap');
    var Http = require('U/http');
    var header = new Vue({
        el: '#header',
        template: _g.getTemplate('main/header-V'),
        data: {
            title: '个人中心',
            list: [{
                active: 0,
                title: '悦夜',
            }, {
                active: 0,
                title: '约会电台',
            }, {
                active: 0,
                title: '个人中心',
            }],
            showSearch: false,
            isBoy: true,
            sex: '男士列表',
            city: '全部',
            sortKey: '最新发布',
            broadcastSexSelected: '我的广播'
        },
        methods: {
            onNearTap: function () {
                // _g.toast('附近');
                // 监听获取附近的人列表
                api && api.sendEvent({
                    name: 'people-people-near'
                });
            },
            onSearchGirlsTap: function() {
                api && api.sendEvent({
                    name: 'people-people-search'
                });
            },
            onSexTap: function () {
                if (this.isBoy) {
                    this.sex = '女士列表';
                    this.isBoy = false;
                } else {
                    this.sex = '男士列表';
                    this.isBoy = true;
                }
                // 监听性别按钮事件
                api && api.sendEvent({
                    name: 'people-people-sex',
                    extra: {
                        isBoy: header.isBoy
                    }
                });
            },
            onNewTap: function () {
                api && api.actionSheet({
                    title: '排序',
                    cancelTitle: '取消',
                    buttons: ['最近约会', '最新发布']
                }, function (ret, err) {
                    var index = "date"
                    if (ret.buttonIndex === 1) {
                        header.sortKey = '最近约会';
                        index = 'date';
                    } else if (ret.buttonIndex === 2) {
                        header.sortKey = '最新发布';
                        index = 'create_time';
                    }
                    if (ret.buttonIndex === 1 || ret.buttonIndex === 2) {
                        api && api.sendEvent({
                            name: 'station-sortKey',
                            extra: {
                                sortKey: index
                            }
                        });
                    }

                })
            },
            onAllTap: function () {
                _g.openWin({
                    header: {
                        data: {
                            title: '我的广播',
                        },
                        template: 'common/header-base-V',
                    },
                    name: 'radio-index',
                    url: '../radio/index.html',
                    bounces: true,
                    slidBackEnabled: false,
                    pageParam: {

                    }
                });
                // api && api.actionSheet({
                //     title: '用户类型',
                //     cancelTitle: '取消',
                //     buttons: ['女士', '男士']
                // }, function (ret, err) {
                //     var index = 0;
                //     if (ret.buttonIndex === 1) {
                //         index = 1;
                //         header.broadcastSexSelected = '女士'
                //     } else if (ret.buttonIndex === 2) {
                //         index = 0;
                //         header.broadcastSexSelected = '男士'
                //     }
                //     if (ret.buttonIndex === 1 || ret.buttonIndex === 2) {
                //         api && api.sendEvent({
                //             name: 'station-sex',
                //             extra: {
                //                 sex: index
                //             }
                //         });
                //     }
                // })
            },
            onShareTap: function () {
                _g.toast('分享');
            }
        }
    });

    var footer = new Vue({
        el: '#footer',
        template: _g.getTemplate('main/footer-V'),
        data: {
            show: true,
            active: 2,
            list: [{
                title: '悦夜',
                tag: 'people'
            }, {
                title: '约会电台',
                tag: 'station'
            }, {
                title: '个人中心',
                tag: 'me'
            }]
        },
        created: function () {

        },
        methods: {
            onItemTap: function (index) {
                if (this.active == index) return;
                // var isNotValidation = UserInfo.sex !== 1 && !UserInfo.identity
                // if (isNotValidation && index !== 2) {
                //     _g.toast('请填写邀请码验证身份！')
                //     index = 2;
                // }
                this.active = index;
                setFrameGroupIndex(index);
                // if(index == 0) {
                //     setFrameGroupIndex(index);
                // } else if(index == 4) {
                //     if(_g.checkUser()) {
                //         setFrameGroupIndex(index);
                //     }
                // }
                if (index == 2) {
                    api.setStatusBarStyle({
                        style: 'light',
                        color: 'rgba(0,0,0,0.0)'
                    });
                } else {
                    api.setStatusBarStyle({
                        style: 'dark',
                        color: 'rgba(0,0,0,0.0)'
                    });
                }
            },
        },
    });

    function openFrameGroup() {
        var mainPage = [{
            name: 'people-hear-frame',
            url: '../people/hear.html',
            bounces: false,
            bgColor: '',
        }, {
            name: 'station-index-frame',
            url: '../station/index.html',
            bounces: true,
            bgColor: '',
        }, {
            name: 'me-index-frame',
            url: '../me/index.html',
            bounces: false,
            bgColor: '',
        }];
        headerHeight = $('#header').offset().height;
        headerHeight = $('#header').height();
        footerHeight = $('#footer').height();
        _g.setLS('appH', {
            'header': headerHeight,
            'footer': footerHeight,
            'win': windowHeight
        });
        api && api.openFrameGroup({
            name: 'main-group',
            scrollEnabled: false,
            rect: {
                x: 0,
                y: headerHeight,
                w: 'auto',
                h: windowHeight - headerHeight - footerHeight
            },
            index: 2,
            preload: 0,
            frames: mainPage
        }, function (ret, err) {
            api.removeLaunchView();
            var index = ret.index;
            header.title = header.list[index].title;
            footer.active = ret.index;
            if (index == 0) {
                api && api.sendEvent({
                    name: 'people-hear-open',
                });
            } else if (index == 1) {
                api && api.sendEvent({
                    name: 'people-hear-close',
                });
                api && api.sendEvent({
                    name: 'station-index-main',
                });
            } else if (index == 2) {
                api && api.sendEvent({
                    name: 'people-hear-close',
                });
                api && api.sendEvent({
                    name: 'me-index-main',
                });
            }
        });
    };
    // 打开百度地图
    function getLocationServices() {
        setTimeout(function() {
            bMap.getLocationServices(function(ret, err) {
                if (ret.enable) {
                    if(systemType == 'ios') {
                        bMap.initMapSDK(function(ret) {
                            if (ret.status) {
                                getLocation();
                            }
                        });
                    } else {
                        getLocation();
                    }
                }
            });
        }, 50);
    };
    // 获取定位啦
    function getLocation() {
        bMap.getLocation({
            accuracy: '100m',
            autoStop: true,
            filter: 1
        }, function (ret, err) {
            if (ret.status) {
                var lon = ret.lon;
                var lat = ret.lat;
                setTimeout(function () {
                    bMap.getNameFromCoords({
                        lon: lon,
                        lat: lat
                    }, function (ret, err) {
                        if (ret.status) {
                            var data = {
                                lon: ret.lon,
                                lat: ret.lat,
                                province: ret.province,
                                city: ret.city,
                                district: ret.district
                            };
                        }
                    });
                }, 0);
            } else {
                _g.toast('获取不了定位：' + err.code);
            }
        });
    };
    // 获取新消息
    function getNewsFn() {
        Http.ajax({
            data: {},
            isSync: false,
            lock: false,
            url: '/message/getTotalUncheckedMsgNum',
            success: function (ret) {
                // 10秒检查是否有新通知
                setTimeout(function () {
                    getNewsFn();
                }, 10000);
                // todo
                if (ret.code === '200' || ret.code === 200) {
                    var nowUnread = ret.data;
                    var lastUnread = _g.getLS('lastUnread') || 0;
                    _g.setLS('lastUnread', nowUnread);
                    if (nowUnread > lastUnread) {
                        // 新消息在状态栏显示
                        api.notification({
                            vibrate: [500, 600],
                            // sound: sound,
                            light: true,
                            notify: {
                                title: '',
                                updateCurrent: true,
                                content: "您有新的消息啦！",
                                extra: {
                                    // pageStyle: "willRec"
                                }
                            },
                        }, function (ret, err) {
                            // cleanUpMessage();
                            // alert(_g.j2s(ret));
                        });
                    }
                }
            },
            error: function (err) {}
        })
    }
    function setFrameGroupIndex(index) {
        api && api.setFrameGroupIndex({
            name: 'main-group',
            index: index,
            scroll: false
        });
    };
    // 监听安卓返回按钮事件
    api && api.addEventListener({
        name: 'keyback'
    }, function (ret, err) {
        api.closeWidget();
    });

    // 监听下面3个tab
    api && api.addEventListener({
        name: 'check-own-info'
    }, function (ret, err) {
        footer.onItemTap(2);
    });
    // 选择城市更新用户
    api && api.addEventListener({
        name: 'people-people-reload',
    }, function (ret, err) {
        var peopleCity = _g.getLS('peopleCity');
        header.city = peopleCity.city;
    });
    // 监听头部变黑色或白色
    api && api.addEventListener({
        name: 'me-index-title',
    }, function (ret, err) {
        if(footer.active == 2) {
            api.setStatusBarStyle({
                style: 'light',
                color: 'rgba(0,0,0,0.0)'
            });
        } else {
            api.setStatusBarStyle({
                style: 'dark',
                color: 'rgba(0,0,0,0.0)'
            });
        }
    });
    // 监听用户列表城市选择
    api && api.addEventListener({
        name: 'main-index-position'
    }, function (ret, err) {
        header.city = ret.value.city;
    });

    setTimeout(function () {
        openFrameGroup();
        getNewsFn();
        getLocationServices();
    }, 0);

    setTimeout(function () {
        if (UserInfo.sex) {
            header.sex = '女士列表';
            header.isBoy = false;
            header.showSearch = false;
        } else {
            header.sex = '男士列表';
            header.isBoy = true;
            header.showSearch = true;
        }
    }, 1000);

    _g.viewAppear(function () {
        _g.closeWins(['account-login-win']);
    });

    module.exports = {};

});