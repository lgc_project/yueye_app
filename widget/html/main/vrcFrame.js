define(function(require, exports, module) {
	api.removeLaunchView();
    var headerHeight = 0;
    var footerHeight = 0;
    var windowHeight = window.innerHeight;
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var header = new Vue({
        el: '#header',
        template: _g.getTemplate('main/header-vrc-V'),
        data: {
            title: '应用',
            list: [{
                active: 0,
                title: '机市',
            }, {
                active: 0,
                title: '矿机',
            }, {
                active: 0,
                title: '矿池',
            }, {
                active: 0,
                title: '矿市',
            }], 
        },
        methods: {

        }
    });
    function openFrameGroup() {
        headerHeight = $('#header').offset().height;
        // footerHeight = $('#footer').height();
        _g.setLS('appH', {
            'header': headerHeight,
            // 'footer': footerHeight,
            'win': windowHeight
        });
        api && api.openFrameGroup({
            name: 'vrc-group',
            scrollEnabled: false,
            rect: {
                x: 0,
                y: headerHeight,
                w: 'auto',
                h: windowHeight - headerHeight
            },
            index: 0,
            preload: 1,
            frames: [{
                name: 'vrc-vrc-frame',
                url: '../vrc/vrc.html',
                bounces: false,
                bgColor: '',
            }, {
                name: 'work-index-frame',
                url: '../work/index.html',
                bounces: false,
                bgColor: '',
            }, {
                name: 'pool-pool-frame',
                url: '../pool/pool.html',
                bounces: false,
                bgColor: '',
            }, {
                name: 'market-index-frame',
                url: '../market/index.html',
                bounces: false,
                bgColor: '',
            }, ],
        }, function(ret, err) {
            // footer.active = ret.index;
            var index = ret.index;
            header.title = header.list[index].title;
        });
    }
    setTimeout(function() {
        openFrameGroup();
        api && api.setFrameGroupAttr({
            name: 'vrc-group',
            hidden:false
        });
    }, 0);
    module.exports = {};

});