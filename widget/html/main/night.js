define(function (require, exports, module) {
    var headerHeight = 0;
    var footerHeight = 0;
    var windowHeight = window.innerHeight;
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    api.setStatusBarStyle({
        style: 'dark',
        color: 'rgba(0,0,0,0.0)'
    });
    var header = new Vue({
        el: '#header',
        template: _g.getTemplate('main/header-night-V'),
        data: {
            title: '运势电台',
            list: [{
                active: 0,
                title: '每日一笑',
            }, {
                active: 0,
                title: '运势电台',
            }, {
                active: 0,
                title: '星座商城',
            }, {
                active: 0,
                title: '个人中心',
            }],
        },
        methods: {

        }
    });

    var footer = new Vue({
        el: '#footer',
        template: _g.getTemplate('main/footer-night-V'),
        data: {
            active: 1,
            list: [{
                title: '咨询',
                tag: 'people'
            }, {
                title: '运势电台',
                tag: 'station'
            }, {
                title: '星座商城',
                tag: 'market'
            }, {
                title: '个人中心',
                tag: 'me'
            }]
        },
        created: function () {

        },
        methods: {
            onItemTap: function (index) {
                if (this.active == index) return;
                this.active = index;
                setFrameGroupIndex(index);
                if (index == 3) {
                    api.setStatusBarStyle({
                        style: 'light',
                        color: 'rgba(0,0,0,0.0)'
                    });
                } else {
                    api.setStatusBarStyle({
                        style: 'dark',
                        color: 'rgba(0,0,0,0.0)'
                    });
                }
            },
        },
    });

    function openFrameGroup() {
        // api.setStatusBarStyle({
        //     style: 'light',
        //     color: 'rgba(0,0,0,0.0)'
        // });
        var mainPage = [{
            name: 'other-consult-frame',
            url: '../other/consult.html',
            bounces: false,
            bgColor: '',
        }, {
            name: 'other-night-frame',
            url: '../other/night.html',
            bounces: false,
            bgColor: '',
        }, {
            name: 'other-market-frame',
            url: '../other/market.html',
            bounces: false,
            bgColor: '',
        }, {
            name: 'me-index-frame',
            url: '../me/index.html',
            bounces: false,
            bgColor: '',
        }];
        headerHeight = $('#header').offset().height;
        headerHeight = $('#header').height();
        footerHeight = $('#footer').height();
        _g.setLS('appH', {
            'header': headerHeight,
            'footer': footerHeight,
            'win': windowHeight
        });
        api && api.openFrameGroup({
            name: 'main-group',
            scrollEnabled: false,
            rect: {
                x: 0,
                y: headerHeight,
                w: 'auto',
                h: windowHeight - headerHeight - footerHeight
            },
            index: 1,
            preload: 0,
            frames: mainPage
        }, function (ret, err) {
            api.removeLaunchView();
            var index = ret.index;
            header.title = header.list[index].title;
            footer.active = ret.index;
        });
    };

    function setFrameGroupIndex(index) {
        api && api.setFrameGroupIndex({
            name: 'main-group',
            index: index,
            scroll: false
        });
    };
    // 监听头部变黑色或白色
    api && api.addEventListener({
        name: 'me-index-title',
    }, function (ret, err) {
        if(footer.active == 3) {
            api.setStatusBarStyle({
                style: 'light',
                color: 'rgba(0,0,0,0.0)'
            });
        } else {
            api.setStatusBarStyle({
                style: 'dark',
                color: 'rgba(0,0,0,0.0)'
            });
        }
    });
    // 监听安卓返回按钮事件
    api && api.addEventListener({
        name: 'keyback'
    }, function (ret, err) {
        api.closeWidget();
    });

    setTimeout(function () {
        openFrameGroup();
    }, 0);

    _g.viewAppear(function () {
        _g.closeWins(['account-login-win']);
    });

    module.exports = {};

});