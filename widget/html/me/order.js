define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var order = new Vue({
        el: '#order',
        template: _g.getTemplate('me/order-main-V'),
        data: {
            list: [{
                id: '100033',
                model: '../../image/other/chunv.png',
                name: '菠菜',
                address: '广东省广州市',
                price: '3.2',
            }, ],
            goodsID: '',
            address: '',
            phone: '',
            receiver: '',
            showPopup: false
        },
        created: function() {
            this.list = [];
        },
        methods: {
        },
    });
    // 获取所有商品
    function getData() {
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: true,
            url: '/commodity/getDealList',
            success: function(ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    var data = ret.data;
                    order.list = listData(data);
                }
            }
        });
    };
    // 数据转换
    function listData(data) {
        var list = data ? data : [];
        return _.map(list, function(item) {
            return {
                id: item.id || '',
                model: item.commodity.imgPath || '',
                name: item.commodity.name || '',
                address: item.address || '',
                price: item.commodity.price || '',
            }
        });
    };
    getData();
    
    module.exports = {};

});
