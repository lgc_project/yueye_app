define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var authentication = new Vue({
        el: '#authentication',
        template: _g.getTemplate('me/authentication-main-V'),
        data: {
            identityPhoto: '',
            identityCode: UserInfo.identityCode || '000000'
        },
        ready: function () {},
        created: function () {},
        methods: {
            onImageTap: function () {
                _g.openPicActionSheet({
                    allowEdit: true,
                    suc: function (ret) {
                        postImage(ret.data);
                    }
                });
            },

        },
    });

    function postImage(path) {
        _g.showProgress();
        api.ajax({
            url: CONFIG.HOST + '/common/uploadPhoto',
            method: 'post',
            data: {
                values: {
                    token: UserInfo.token
                },
                files: {
                    image: path,
                }
            }
        }, function (ret, err) {
            if (ret.code == 200) {
                authentication.identityPhoto = ret.data;
                postIdentity();
            } else {
                _g.toast(ret.codeDesc);
            }
        });
    };
    // 提交认证图片
    function postIdentity() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                identityPhoto: authentication.identityPhoto
            },
            isSync: true,
            url: '/user/identity',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    _g.hideProgress();
                    _g.toast('提交成功，耐性等待客服审核。');
                } else {
                    _g.hideProgress();
                    _g.toast('提交失败');
                }
            },
            error: function (err) {
                _g.toast(JSON.stringify(err));
            }
        })
    }
    module.exports = {};

});