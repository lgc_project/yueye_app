define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var weiXin = api.require('weiXin');
    // var aliPay = api.require('aliPay');
    var aliPayPlus = api.require('aliPayPlus');
    var Http = require('U/http');
    var member = new Vue({
        el: '#member',
        template: _g.getTemplate('me/member-main-V'),
        data: {
            price: 0,
            lastIndex: 0,
            payStyle: 'wx',
            isTouched: true,
            nickname: '未命名',
            memberSurplusDays: '尚未开通会员',
            moneyList: [{
                id: '',
                showMoney: 90,
                money: 90,
                title: '半个月',
                select: true
            }, {
                id: '',
                showMoney: 130,
                money: 130,
                title: '1个月',
                select: false
            }, {
                id: '',
                showMoney: 180,
                money: 180,
                title: '3个月',
                select: false
            }, {
                id: '',
                showMoney: 240,
                money: 240,
                title: '6个月',
                select: false
            }, ]
        },
        created: function () {
            this.moneyList = [];
        },
        ready: function () {
            if(UserInfo) {
                this.nickname = UserInfo.nickname || '未命名';
                this.memberSurplusDays = UserInfo.memberSurplusDays || '尚未开通会员';
            }
        },
        methods: {
            onPriceTap: function (index) {
                if (this.lastIndex == index) return;
                this.moneyList[index].select = true;
                this.moneyList[this.lastIndex].select = false;
                this.lastIndex = index;
                this.price = this.moneyList[index].money;
            },
            onStyleTap: function (param) {
                this.payStyle = param;
            },
            onPayTap: function () {
                if (this.isTouched) {
                    this.isTouched = false;
                    var id = this.moneyList[this.lastIndex].id;
                    if (this.payStyle == 'ali') {
                        this.isTouched = true;
                        _g.toast('支付宝暂未支持支付哦');
                        return;
                        // getAliOrder(id);
                    }
                    if (this.payStyle == 'wx') {
                        getWechatOrder(id);
                    }
                }
            }
        },
    });
    // 获取支付宝预支付订单
    function getAliOrder() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                payMoney: 0.01
            },
            isSync: true,
            url: '/pay/getAlipayOrder',
            success: function (ret) {
                // alert(_g.j2s(ret.data));
                member.isTouched = true;
                if (ret.code == 200) {
                    var data = ret.data;
                    // var param = "partner=2088231486933302&subject=" + data.subject + "seller_id=2088231486933302"
                    postAlipayRecharge(data);
                } else {
                    alert(_g.j2s(ret));
                }
            }
        });
    };
    // partner="2088231486933302"&
    // seller_id="2088231486933302"&
    // out_trade_no="0819145412-6177"&
    // subject="测试"&
    // body="测试测试"&
    // total_fee="0.01"&
    // notify_url="http://notify.msp.hk/notify.htm"&
    // service="mobile.securitypay.pay"&
    // payment_type="1"&
    // _input_charset="utf-8"&
    // it_b_pay="30m"&
    // sign="lBBK%2F0w5LOajrMrji7DUgEqNjIhQbidR13GovA5r3TgIbNqv231yC1NksLdw%2Ba3JnfHXoXuet6XNNHtn7VE%2BeCoRO1O%2BR1KugLrQEZMtG5jmJIe2pbjm%2F3kb%2FuGkpG%2BwYQYI51%2BhA3YBbvZHVQBYveBqK%2Bh8mUyb7GM1HxWs9k4%3D"&
    // sign_type="RSA"'
    //
    // aliPayPlus
    // app_id=2015052600090779&
    // biz_content=%7B%22timeout_express%22%3A%2230m%22%2C%22seller_id%22%3A%22%22%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%2C%22total_amount%22%3A%220.01%22%2C%22subject%22%3A%221%22%2C%22body%22%3A%22%E6%88%91%E6%98%AF%E6%B5%8B%E8%AF%95%E6%95%B0%E6%8D%AE%22%2C%22out_trade_no%22%3A%22IQJZSRC1YMQB5HU%22%7D&
    // charset=utf-8&
    // format=json&
    // method=alipay.trade.app.pay&
    // notify_url=http%3A%2F%2Fdomain.merchant.com%2Fpayment_notify&
    // sign_type=RSA2&
    // timestamp=2016-08-25%2020%3A26%3A31&
    // version=1.0&
    // sign=cYmuUnKi5QdBsoZEAbMXVMmRWjsuUj%2By48A2DvWAVVBuYkiBj13CFDHu2vZQvmOfkjE0YqCUQE04kqm9Xg3tIX8tPeIGIFtsIyp%2FM45w1ZsDOiduBbduGfRo1XRsvAyVAv2hCrBLLrDI5Vi7uZZ77Lo5J0PpUUWwyQGt0M4cj8g%3D'
    function postAlipayRecharge(data) {
        // alert(data);
        aliPayPlus.payOrder({
            orderInfo: data
        }, function (ret, err) {
            api.alert({
                title: '支付结果',
                msg: ret,
                buttons: ['确定']
            });
            // alert(_g.j2s(ret));
        });
    }
    // 获取微信预支付订单
    function getWechatOrder(id) {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                collecterId: '',
                type: 4,
                rechargeId: id,
                payMoney: member.price * 100
            },
            isSync: true,
            url: '/pay/getWxPayOrder',
            success: function (ret) {
                member.isTouched = true;
                if (ret.code == 200) {
                    postWechatRecharge(ret.data);
                } else {
                    alert(_g.j2s(ret));
                }
            }
        });
    };
    // 调起微信支付
    function postWechatRecharge(data) {
        weiXin.payOrder({
            key: data.appid,
            orderId: data.prepayid,
            partnerId: data.partnerid,
            nonceStr: data.noncestr,
            timeStamp: data.timestamp,
            sign: data.sign,
            package: data.package
        }, function (ret, err) {
            if (ret.status) {
                if(UserInfo.sex == 0 && UserInfo.identity == 0) {
                    UserInfo.identity = 1;
                    _g.setLS('UserInfo', UserInfo);
                    _g.toast("会员开通成功，请重新登录");
                    setTimeout(function () {
                        setTimeout(function () {
                            _g.closeWins(['account-verificate-win', 'me-member-win']);
                        }, 1000);
                    }, 3000);
                } else {
                    api && api.sendEvent({
                        name: 'work-index-changeInfo',
                    });
                    _g.toast('充值成功');
                }
            } else {
                api.alert('支付失败');
            }
        });
    };
    // 获取套餐
    function getMealsData() {
        Http.ajax({
            data: {},
            isSync: true,
            url: '/member/rechargeTypeList',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    var data = ret.data;
                    member.moneyList = getDetail(data);
                    member.moneyList[0].select = true;
                    member.price = member.moneyList[0].money;
                    // postWechatRecharge(ret.data);
                } else {
                    alert(_g.j2s(ret));
                }
            }
        });
    };
    function getDetail(result) {
        var list = result ? result : [];
        return _.map(list, function(item) {
            return {
                id: item.id || '',
                showMoney: item.rechargeAmount / (item.rechargeDays / 30) || '',
                title: dayNumChange(item.rechargeDays) || '',
                money: item.rechargeAmount,
                select: false
            }
        });
    };
    // 天数转换
    function dayNumChange(param) {
        var data = 0;
        if(param < 30) {
            return(param + '天');
        } else if(param / 365 >= 1) {
            return(param / 365 + '年');
        } else if(param / 30 >= 1) {
            return(param / 30 + '个月');
        }
    }
    getMealsData();
    // // 监听刷新页面
    // api && api.addEventListener({
    //     name: 'work-index-changeInfo',
    // }, function (ret, err) {
    //     getInfoData();
    // });
    // // 监听刷新页面
    // api && api.addEventListener({
    //     name: 'me-index-main',
    // }, function (ret, err) {
    //     getInfoData();
    // });

    module.exports = {};

});