define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var Vcode = require('U/vcode');
    var bindingPhone = new Vue({
        el: '#bindingPhone',
        template: _g.getTemplate('me/bindingPhone-main-V'),
        data: {
            telephone: '',
            checkcode: '',
            codeText: '获取验证码',
        },
        created: function() {

        },
        ready: function() {
            Vcode.init({
                onInit: this.onInit,
                onAction: this.onAction,
            });
        },
        methods: {
            onInit: function(nowText) {
                this.codeText = nowText;
            },
            onAction: function(nowText) {
                this.codeText = nowText;
            },
            //  获取验证码
            onGetCodeTap: function() {
                // _g.toast('获取验证码!');
                if (Vcode.isAllow()) {
                    var mobileReg = /^1[0-9]{10}$/;
                    if(this.telephone == '') {
                        _g.toast('手机号不能为空');
                        return false;
                    }
                    if (!mobileReg.test(this.telephone)) {
                        _g.toast('请输入11位数的手机号码');
                        return
                    }
                    Vcode.start();
                    this.vcode = Vcode.getRandom(6);
                    Http.ajax({
                        data: {
                            phone: bindingPhone.telephone,
                            type: 1
                        },
                        lock: false,
                        url: '/common/sendVerfiCode',
                        success: function(ret) {
                            if (ret.code == 200) {
                                // _g.toast(ret.msg);
                            } else {
                                _g.toast(ret.msg);
                            }
                        },
                        error: function(err) {},

                    });
                } else {
                    _g.toast('请在倒计时结束再点击获取新的验证码!');
                }
            },
            onBindTap: function() {
                var mobileReg = /^1[0-9]{10}$/;
                if(this.telephone == '') {
                    _g.toast('手机号不能为空');
                    return false;
                }
                if (!mobileReg.test(this.telephone)) {
                    _g.toast('请输入11位数的手机号码');
                    return
                }
                if(this.checkcode == '') {
                    _g.toast('验证码不能为空');
                    return false;
                }
                postData()
            }
        },
    });
    // 提交绑定手机号码接口
    function postData() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                phone: bindingPhone.telephone,
            },
            isSync: true,
            url: '/user/bindPhone',
            success: function(ret) {
                if (ret.code == 200) {
                    _g.toast('绑定成功');
                    api.sendEvent({
                        name: 'me-index-editInfo',
                    });
                    setTimeout(function() {
                        api && api.closeWin();
                    }, 200);
                } else {
                    _g.toast('绑定失败');
                }
            }
        });
    };
    
    module.exports = {};

});
