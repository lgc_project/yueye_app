define(function(require, exports, module) {
    var Http = require('U/http');
    var service = new Vue({
        el: '#service',
        template: _g.getTemplate('me/service-main-V'),
        data: {
            content: ''
        },
        created: function() {
        },
        methods: {

        },
    });
    // 获取客服列表
    function getMsgList() {
        Http.ajax({
            data: {},
            isSync: true,
            url: '/notice/getNotice',
            success: function(ret) {
                // alert(_g.j2s(ret));
                if(ret.code == 200) {
                    service.content = ret.data.content;
                } 
            }
        });
    };
    getMsgList();
    // 下拉刷新
    _g.setPullDownRefresh(function() {
        getMsgList();
    });
    
    module.exports = {};

});
