define(function(require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var blackName = new Vue({
        el: '#blackName',
        template: _g.getTemplate('me/blackName-main-V'),
        data: {
            list: [{
                id: '',
                avatar: '',
                nickname: '',
            }]
        },
        created: function() {
            this.list = []
        },
        methods: {
            onCancelTap: function(index) {
                var passiveId = this.list[index].id
                moveOutBlack(passiveId);
            }
        },
    });
    // 获取拉黑名单接口
    function getData() {
        var _token = UserInfo.token;
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: false,
            url: '/relationship/defriendList',
            success: function(ret) {
                // alert(_g.j2s(ret));
                if(ret.code == 200) {
                    var data = ret.data;
                    blackName.list = getDetail(data);
                } else {
                    _g.toast('获取黑名单数据失败');
                }
            }
        });
    };
    function getDetail(result) {
        var list = result ? result : [];
        return _.map(list, function(item) {
            return {
                id: item.id || '',
                avatar: item.avatar || '',
                nickname: item.nickname || '',
            }
        });
    };
    // 取消拉黑
    function moveOutBlack(passiveId) {
        Http.ajax({
            data: {
                activeId: UserInfo.id,
                passiveId: passiveId,
                defriend: false
            },
            isSync: false,
            url: '/relationship/defriend',
            success: function(ret) {
                if(ret.code == 200) {
                    getData()
                } else {
                    _g.toast('黑名单操作失败');
                }
            }
        });
    }
    // 下拉刷新
    _g.setPullDownRefresh(function() {
        getData();
    });
    getData();
    
    module.exports = {};

});
