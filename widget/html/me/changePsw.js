define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var changePsw = new Vue({
        el: '#changePsw',
        template: _g.getTemplate('me/changePsw-main-V'),
        data: {
            oldPassword: '',
            newPassword: ''
        },
        created: function () {

        },
        methods: {
            onDelOldTap: function() {
                this.oldPassword = '';
                this.newPassword = '';
                return false;
            },
            onDelTap: function() {
                this.newPassword = '';
                return false;
            },
            onConfirmTap: function () {
                var mobileReg = /^1[0-9]{10}$/;
                if (this.oldPassword == '') {
                    _g.toast('密码不能为空');
                    return false;
                }
                if (this.oldPassword != '') {
                    var reg = /^[A-Za-z0-9]{6,16}$/;
                    isok = reg.test(this.oldPassword);
                }
                if (!isok) {
                    _g.toast('原密码请输入6~16位数字/字母组合的密码');
                    return
                }
                if (this.newPassword == '') {
                    _g.toast('密码不能为空');
                    return false;
                }
                if (this.newPassword != '') {
                    var reg = /^[A-Za-z0-9]{6,16}$/;
                    isok = reg.test(this.newPassword);
                }
                if (!isok) {
                    _g.toast('新密码请输入6~16位数字/字母组合的密码');
                    return
                }
                if (this.newPassword === this.oldPassword) {
                    _g.toast('新密码与旧密码相同，请输入不同的旧密码');
                    return
                }
                changePassword();
            }
        },
    });
    // 获取个人资料接口
    function changePassword() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                oldPassword: changePsw.oldPassword,
                newPassword: changePsw.newPassword
            },
            isSync: true,
            url: '/user/updatePassword',
            success: function (ret) {
                if (ret.code == 200) {
                    _g.toast('修改密码成功');
                    _g.rmLS('UserInfo');
                    api && api.setStatusBarStyle({
                        style: 'dark'
                    });
                    api && api.openWin({
                        name: 'account-login-win',
                        url: '../account/login.html',
                        pageParam: {
                            from: 'root'
                        },
                        bounces: false,
                        slidBackEnabled: false,
                        animation: {
                            type: 'none'
                        }
                    });
                    setTimeout(function () {
                        _g.closeWins(['main-index-win', 'me-changePsw-win'])
                    }, 1000);
                } else {
                    _g.toast('修改密码失败');
                }
            }
        });
    };
    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getInfoData();
    });
    // getInfoData();

    module.exports = {};

});