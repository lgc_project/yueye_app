define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo') || '';
    var noGPS = _g.getLS('noGPS') || 0;
    var bMap = api.require('bMap');
    var Http = require('U/http');
    var UIMediaScanner = api.require('UIMediaScanner');
    var photoBrowser = api.require('photoBrowser');
    
    var proportion = 1;
    var img_show = new Image();
    var winWidth = api.winWidth;

    var user = new Vue({
        el: '#user',
        template: _g.getTemplate('me/index-main-V'),
        data: {
            member: {
                nickname: '',
                sex: 0,
                identity: 0,
                tap: '',
                city: '',
                age: '',
                occupation: '',
                apoint: '',
                phone: '',
                avatar: '',
                introduce: '小白脸 高智商 走肾不走心',
                checkLimit: 0,
                avatarStatus: false
            },
            femaleCommentList: [{
                number: 0,
                title: '漂亮',
                select: false,
            }, {
                number: 0,
                title: '身材好',
                select: false,
            }, {
                number: 0,
                title: '好玩',
                select: false,
            }, {
                number: 0,
                title: '难约',
                select: false,
            }, {
                number: 0,
                title: '要红包',
                select: false,
            }, {
                number: 0,
                title: '照骗',
                select: false,
            }, ],
            maleCommentList: [{
                number: 0,
                title: '帅气',
                select: false,
            }, {
                number: 0,
                title: '身材好',
                select: false,
            }, {
                number: 0,
                title: '好玩',
                select: false,
            }, {
                number: 0,
                title: '大方',
                select: false,
            }, {
                number: 0,
                title: '不拖拉',
                select: false,
            }, {
                number: 0,
                title: '口嗨',
                select: false,
            }, ],
            currentList: [],
            showComment: false,
            unread: 0,
            size: 0,
            visitors: 0,
            seeImages: 0,
            identityMark: '',
            images: [],
            uploadPhotoTemp: [],
            currentUploadLevel: 0,
            selectImageNum: 1,
            signerNum: 0,
            imgList: [new Image(), new Image(), new Image(), new Image(), new Image(), new Image(), new Image(), new Image(), new Image()],
            noGPS: true
        },
        computed: {
            checkLimitMark() {
                if (this.member.checkLimit === 0) return '公开'
                else if (this.member.checkLimit === 1) return '查看相册付费'
                else if (this.member.checkLimit === 2) return '查看前需要通过我的验证'
                else if (this.member.checkLimit === 3) return '隐身'
                else if (this.member.checkLimit === null) return '公开'
                else return '公开'
            }
        },
        created: function () {
            if (UserInfo) {
                this.member = {
                    nickname: UserInfo.nickname || '',
                    // tap: UserInfo.tap || '',
                    sex: UserInfo.sex || 0,
                    identity: UserInfo.identity || 0,
                    city: UserInfo.city || '',
                    age: UserInfo.age || '未设置',
                    occupation: UserInfo.occupation || '',
                    apoint: UserInfo.province + ' ' + UserInfo.city || '',
                    phone: UserInfo.username || '',
                    avatar: UserInfo.avatar || '',
                }
                this.currentList = UserInfo.sex ? this.femaleCommentList : this.maleCommentList
            }
            this.noGPS = noGPS == 1 ? true : false;
        },
        methods: {
            onWinTap: function (param) {
                winUrl(param);
            },
            onLogoutTap: function () {
                api.confirm({
                    title: '注意',
                    msg: '确定退出登录？',
                    buttons: ['取消', '确定']
                }, function (ret, err) {
                    if (ret.buttonIndex == 2) {
                        _g.rmLS('UserInfo');
                        _g.rmLS('peopleCity');
                        api && api.setStatusBarStyle({
                            style: 'dark'
                        });
                        api && api.openWin({
                            name: 'account-login-win',
                            url: '../account/login.html',
                            pageParam: {
                                from: 'root'
                            },
                            bounces: false,
                            slidBackEnabled: false,
                            animation: {
                                type: 'none'
                            }
                        });
                        setTimeout(function () {
                            _g.closeWins(['people-hear-win', 'station-index-win', 'me-index-win', 'main-night-win', 'main-index-win'])
                        }, 1000);
                    }
                });
            },
            checkPermissions: function () {
                var maleList = ['公开', '隐身']
                var femaleList = ['公开', '查看相册付费', '查看前需通过我验证', '隐身']
                api && api.actionSheet({
                    title: '查看权限',
                    cancelTitle: '取消',
                    buttons: UserInfo.sex === 1 ? femaleList : maleList
                }, function (ret, err) {
                    var index = ret.buttonIndex;
                    if (index <= 4 && index >= 1 && UserInfo.sex === 1) {
                        if (UserInfo.sex === 1 && UserInfo.identity !== 2) {
                            if (index === 2) {
                                _g.toast('设置收费相册需要开通验证喔~');
                                return
                            } else if (index === 3) {
                                _g.toast('设置查看申请需要开通验证喔~');
                                return
                            } else {
                                updateCheckLimit(index);                                
                            }
                        } else if (UserInfo.sex === 1 && UserInfo.identity === 2) {
                            if (UserInfo.sex === 1 && index === 2) {
                                api && api.prompt({
                                    title: '请输入付费相册金额',
                                    msg: '只能输入0~100之间的金额',
                                    type: 'number',
                                    buttons: ['确定', '取消']
                                }, function (ret, err) {
                                    if (ret.buttonIndex === 1 && Number(ret.text) === 0 && Number(ret.text) > 100) {
                                        _g.toast('金额设置不在允许范围内');
                                        return
                                    } else if (ret.buttonIndex === 1 && Number(ret.text) > 0 && Number(ret.text) < 100) {
                                        Http.ajax({
                                            data: {
                                                userId: UserInfo.id,
                                                albumAmount: Number(ret.text)
                                            },
                                            isSync: false,
                                            url: '/user/updateAlbumAmount',
                                            success: function (ret) {
                                                if (ret.code == 200) {
                                                    _g.toast('付费相册金额设置成功')
                                                    updateCheckLimit(index);
                                                }
                                            },
                                            error(err) {
                                                _g.toast('网络异常')
                                            }
                                        });
                                    }

                                })
                            } else {
                                updateCheckLimit(index);
                            }
                        }
                        // updateCheckLimit(index);
                    } else if (index <= 2 && index >= 1 && UserInfo.sex === 0) {
                        updateCheckLimit(index);
                    }
                })
            },
            onCodeTap: function () {
                api.confirm({
                    msg: '申请邀请码是免费的，为了维持悅夜的氛围，请一定把邀请码赠与靠谱的朋友',
                    buttons: ['确认申请', '取消']
                }, function (ret, err) {
                    var index = ret.buttonIndex;
                    if (index == 1) {
                        applyInviteCode();
                    }
                });
            },
            onCleanCacheTap: function () {
                _g.showProgress();
                api.clearCache({
                    // timeThreshold:30
                }, function () {
                    _g.hideProgress();
                    _g.toast('缓存清理成功');
                });
                user.size = 0;
            },
            verifyTap: function () {
                if (UserInfo.identity === 0 && UserInfo.sex === 1) {
                    _g.openWin({
                        header: {
                            data: {
                                title: '身份验证',
                            },
                            template: 'common/header-base-V',
                        },
                        name: 'me-authentication-win',
                        url: '../me/authentication.html',
                        bounces: false,
                        slidBackEnabled: false,
                        animation: {
                            type: 'none'
                        }
                    });
                }
            },
            myComment: function () {
                this.showComment = true;
            },
            hideComment: function () {
                this.showComment = false;
            },
            onCancelTap: function () {
                api && api.confirm({
                    msg: '确定要一键恢复阅后即焚图片？',
                    buttons: ['确定', '取消']
                }, function (ret, err) {
                    if (ret.buttonIndex === 1) cancelHideMyImg();
                })
            },
            addImg() {
                var maleBtns = ['普通图片']
                var femaleBtns = ['红包图片', '阅后即焚图片', '普通图片']
                api && api.actionSheet({
                    title: '添加图片',
                    cancelTitle: '取消',
                    buttons: UserInfo.sex === 1 ? femaleBtns : maleBtns
                }, function (ret, err) {
                    var index = ret.buttonIndex;
                    _g.showProgress();
                    if (UserInfo.sex === 1) {
                        if (index === 1) user.currentUploadLevel = 2
                        else if (index === 2) user.currentUploadLevel = 1
                        else if (index === 3) user.currentUploadLevel = 0
                        if (index >= 1 && index <= 3) selectLocalImages(index)
                    } else if (UserInfo.sex === 0) {
                        if (index === 1) {
                            user.currentUploadLevel = 0;
                            selectLocalImages(3);
                        }
                    }
                    _g.hideProgress();
                })
            },
            previewImage(images, index, event) {
                if ($(event.target).closest('.spot').length > 0) {
                    // 图片删除
                    api.confirm({
                        msg: '确认删除图片？',
                        buttons: ['确认', '取消']
                    }, function (ret, err) {
                        var buttonIndex = ret.buttonIndex;
                        if (buttonIndex == 1) {
                            Http.ajax({
                                data: {
                                    photoId: user.images[index].id
                                },
                                isSync: false,
                                url: '/userPhoto/delete',
                                success: function (ret) {
                                    if (ret.code == 200) {
                                        getImagesData();
                                    } else _g.toast('删除失败：' + ret.msg)
                                },
                                error(err) {
                                    _g.toast('删除失败：' + err.msg)
                                }
                            });
                        }
                    });
                } else {
                    // 图片预览
                    // if (images[index].status) return;
                    setTimeout(function() {
                        openPhotoSwipe(images, index);
                    }, 200);
                }
            },
            openAvatar(image) {
                photoBrowser.open({
                    images: [image],
                    placeholderImg: 'widget://res/img/apicloud.png',
                    bgColor: '#000',
                    mode: 2
                }, function (ret, err) {
                    if (ret) {
                        if (ret.eventType === 'click') {
                            photoBrowser.close()
                        }
                    } else {
                        _g.toast('图片打开失败')
                    }
                })
            }
        },
    });

    // 选择图片
    function selectLocalImages(level) {
        if (user.images.length === 9) return
        var num = 1;
        if (level === 1) num = 1;
        else num = 9 - user.images.length;
        user.uploadPhotoTemp = [];
        UIMediaScanner.open({
            max: num,
            type: 'picture',
            bg: '#000',
            texts: {
                stateText: '已选择*项', //（可选项）字符串类型；状态文字内容；*号会被替换为已选择个数；默认：'已选择*项'
                cancelText: '取消', //（可选项）字符串类型；取消按钮文字内容；默认：'取消'
                finishText: '确定', //（可选项）字符串类型；完成按钮文字内容；默认：'完成'
                selectedMaxText: '只能选择*个了！', //（可选项）字符串类型；最多显示提示语，*号会被替换为已选择个数;默认：'最多显示*个资源'
                classifyTitle: '系统相册'
            },
            styles: {
                bg: '#fff',
                mark: {
                    icon: '',
                    position: 'bottom_left',
                    size: 26
                },
                nav: {
                    bg: '#000',
                    stateColor: '#fff',
                    stateSize: 18,
                    cancelBg: 'rgba(0,0,0,0)',
                    cancelColor: '#fff',
                    cancelSize: 18,
                    finishBg: 'rgba(0,0,0,0)',
                    finishColor: '#fff',
                    finishSize: 18
                }
            },
            exchange: true,
        }, function (ret) {
            if (ret.eventType === 'confirm') {
                if (ret.list.length) {
                    _g.hideProgress();
                    var list = ret.list;
                    var path = [];
                    user.selectImageNum = list.length
                    list.forEach(function (ele) {
                        path.push(ele.path)
                    })
                    postAvatar(path)
                }
            } else if (ret.eventType === 'albumError') _g.toast('访问相册失败')
        })
    }

    function postAvatar(path) {
        path.forEach(function (ele) {
            uploadAndSave(ele);
        })
    }

    // 图片上传
    function uploadAndSave(path) {
        api.ajax({
            url: CONFIG.HOST + '/common/uploadPhoto',
            method: 'post',
            isSync: true,
            data: {
                values: {
                    token: UserInfo.token
                },
                files: {
                    image: path,
                }
            }
        }, function (ret, err) {
            if (ret.code == 200) {
                user.uploadPhotoTemp.push(ret.data)
                if (user.selectImageNum === user.uploadPhotoTemp.length) saveImage();
            } else {
                _g.toast(ret.msg);
            }
        });
    }

    // 保存图库 
    function saveImage() {
        var data = {
            userId: UserInfo.id,
            level: 0,
            checkPay: 0,
            imagePath: user.uploadPhotoTemp.join(',')
        }
        if (user.currentUploadLevel !== 2) {
            user.images.forEach(function (ele) {
                if (ele.level === user.currentUploadLevel) {
                    data.imagePath = data.imagePath + ',' + ele.imagePath
                }
            })
        }
        data.level = user.currentUploadLevel;
        if (user.currentUploadLevel === 2) data.checkPay === 3;
        Http.ajax({
            data: data,
            isSync: false,
            lock: false,
            url: '/userPhoto/saveUserPhoto',
            success: function (ret) {
                if (ret.code == 200) {
                    getImagesData();
                } else _g.toast('保存失败：' + ret.msg)
            },
            error(err) {
                _g.toast('保存普通图片失败：' + err.msg)
            }
        });
    }
    // 图片预览插件
    function openPhotoSwipe(images, index) {
        var items = [];
        for (var i = 0; i < images.length; i++) {
            // var width = user.imgList[i].width || 1;
            // var height = user.imgList[i].height || 1;
            items.push({
                src: images[i].imagePath,
                w: 0,
                h: 0
            })

        }
        var pswpElement = document.querySelectorAll('.pswp')[0];
        var options = {
            index: index,
            mainClass: 'pswp--minimal--dark',
            barsSize: {
                top: 0,
                bottom: 0
            },
            captionEl: false,
            fullscreenEl: false,
            shareEl: false,
            showAnimationDuration: 200,
            hideAnimationDuration: 200,
            bgOpacity: 0.85,
            showHideOpacity: true,
            tapToToggleControls: false,
            closeOnVerticalDrag: true,
            tapToClose: true,
            closeEl: true
        };
        var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        var realViewportWidth,
            useLargeImages = false,
            firstResize = true,
            imageSrcWillChange;
        gallery.listen('beforeResize', function () {
            var dpiRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
            dpiRatio = Math.min(dpiRatio, 2.5);
            realViewportWidth = gallery.viewportSize.x * dpiRatio;
            if (realViewportWidth >= 1200 || (!gallery.likelyTouchDevice && realViewportWidth > 800) || screen.width > 1200) {
                if (!useLargeImages) {
                    useLargeImages = true;
                    imageSrcWillChange = true;
                }
            } else {
                if (useLargeImages) {
                    useLargeImages = false;
                    imageSrcWillChange = true;
                }
            }
            if (imageSrcWillChange && !firstResize) {
                gallery.invalidateCurrItems();
            }
            if (firstResize) {
                firstResize = false;
            }
            imageSrcWillChange = false;
        });
        gallery.listen('gettingData', function (index, item) {
            if (item.html === undefined && item.onloading === undefined && (item.w < 1 || item.h < 1)) { // unknown size
                var img = new Image();
                item.onloading = true;
                img.onload = function () { // will get size after load
                    if (user.images[index].newImage.width > winWidth) {
                        item.w = winWidth;
                        item.h = winWidth * user.images[index].newImage.height / user.images[index].newImage.width;
                    } else {
                        item.w = user.images[index].newImage.width; // set image width
                        item.h = user.images[index].newImage.height; // set image height
                    }
                    gallery.updateSize(true); // reinit Items
                }
                img.src = item.src; // let's download image
            }
        });
        gallery.init();
    };

    // 更新权限
    function updateCheckLimit(index) {
        if (UserInfo.sex === 0 && index === 2) index = 4;
        Http.ajax({
            data: {
                userId: UserInfo.id,
                checkLimit: index - 1
            },
            isSync: true,
            lock: false,
            url: '/user/updateCheckLimit',
            success: function (ret) {
                if (ret.code == 200) {
                    user.member.checkLimit = index - 1
                }
            }
        });
    }

    function winUrl(param) {
        switch (param) {
            case 'msg':
                openWin('消息中心', 'message-index', 'message/index');
                break;
            case 'images':
                openWin('我的图库', 'me-images', 'me/images');
                break;
            case 'member':
                openWin('会员中心', 'me-member', 'me/member');
                break;
            case 'info':
                openWin('完善个人信息', 'account-editInfo', 'account/editInfo');
                break;
            case 'blackName':
                openWin('黑名单', 'me-blackName', 'me/blackName');
                break;
            case 'radio':
                openWin('我的广播', 'radio-index', 'radio/index');
                break;
            case 'mylove':
                openWin('我喜欢的', 'me-favor', 'me/favor');
                break;
            case 'phone':
                openWin('绑定手机号码', 'me-bindingPhone', 'me/bindingPhone');
                break;
            case 'changePsw':
                openWin('修改密码', 'me-changePsw', 'me/changePsw');
                break;
            case 'verificate':
                openWin('邀请码验证', 'account-verificate', 'account/verificate');
                break;
            case 'msgSend':
                openWin('消息推送设置', 'message-set', 'message/set');
                break;
            case 'agreement':
                openWin('服务协议', 'me-agreement', 'me/agreement');
                break;
            case 'service':
                openWin('客服', 'me-service', 'me/service');
                break;
            case 'wallet':
                openWin('钱包', 'me-wallet', 'me/wallet');
                break;
            case 'order':
                openWin('我的订单', 'me-order', 'me/order');
                break;
            default:
                break;
        }
    };

    function openWin(_title, _name, _url) {
        api.setStatusBarStyle({
            style: 'dark',
            color: 'rgba(0,0,0,0.0)'
        });
        UserInfo = _g.getLS('UserInfo');
        if (_title == '绑定手机号码' && user.member.phone != '') {
            _g.toast('手机号码已经绑定');
            return;
        }
        var _bounces = true;
        if (_name == 'me-agreement') {
            _bounces = false;
        }
        if (_name == 'me-member') {
            _bounces = false;
        }
        if (_name == 'radio-index') {
            _bounces = false;
        }
        var _template = 'common/header-me-V';
        if (_title == '完善个人信息') {
            _template = 'common/header-save-V';
        }
        var data = {
            title: _title,
        }
        if (_title === '我的广播') data.rightText = '我要广播'
        _g.openWin({
            header: {
                data: data,
                template: _template,
            },
            name: _name,
            url: '../' + _url + '.html',
            bounces: _bounces,
            slidBackEnabled: false,
            pageParam: {

            }
        });
    };
    // 获取个人资料接口
    function getInfoData() {
        var _token = '';
        if (UserInfo) {
            _token = UserInfo.token;
        }
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: true,
            lock: false,
            url: '/user/getUserInfo',
            success: function (ret) {
                getUnreadCount();
                signerNum();
                getImagesData();
                getVisitors();
                // 获取系统缓存数据
                getCacheSize();
                getReadMyImg();
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    var data = ret.data;
                    data.token = _token;
                    _g.setLS('UserInfo', data);
                    UserInfo = _g.getLS('UserInfo');
                    user.member = data;
                    if (user.member.introduce != '') {
                        user.member.introduce = user.member.introduce.split(",");
                        user.member.introduce = user.member.introduce.join(" ");
                    }
                    user.identityMark = formatIdentityMark(user.member)
                }
            }
        });
    };

    // 获取图片
    function getImagesData() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                viewerId: UserInfo.id
            },
            isSync: false,
            url: '/userPhoto/getUserPhoto',
            success: function (ret) {
                if (ret.code == 200) {
                    var data = ret.data;
                    user.images = data;
                    for (var i = 0; i < data.length; i++) {
                        user.images[i].newImage = new Image();
                        user.images[i].newImage.src = data[i].imagePath;
                    }
                }
            }
        });
    };

    function formatIdentityMark(data) {
        var res = '';
        if (data.sex == 0) {
            if (data.identity == 1) {
                res = '会员'
            } else {
                res = '非会员'
            }
        }
        if (data.sex == 1) {
            if (data.identity == 2) {
                res = '真实'
            } else {
                res = '未认证'
            }
        }
        return res
    }

    // 获取未读消息总数
    function getUnreadCount() {
        Http.ajax({
            data: {},
            isSync: false,
            lock: false,
            method: 'get',
            url: '/message/getTotalUncheckedMsgNum/?userId=' + UserInfo.id,
            success: function (ret) {
                getMyComment();
                // todo
                if (ret.code === 200) {
                    user.unread = ret.data;
                }
            },
            error: function (err) {
                _g.toast('获取消息失败：' + JSON.stringify(err));
            }
        })
    };
    // 获取焚毁用户照片人数统计
    function getReadMyImg() {
        Http.ajax({
            data: {},
            isSync: false,
            lock: false,
            url: '/photoViewer/countViewerByUserId/' + UserInfo.id,
            success: function (ret) {
                if (ret.code === 200) user.seeImages = ret.data;
                setTimeout(function () {
                    getReadMyImg();
                }, 10000);
            },
            error: function (err) {
                _g.toast('获取消息失败：' + JSON.stringify(err));
            }
        })
    };
    // 复原焚毁用户照片
    function cancelHideMyImg() {
        Http.ajax({
            data: {
                // token: UserInfo.token
            },
            isSync: false,
            lock: false,
            method: 'delete',
            url: '/photoViewer/recoveryPhoto/' + UserInfo.id + '?token=' + UserInfo.token,
            success: function (ret) {
                if (ret.code === 200) {
                    _g.toast('恢复成功')
                    user.seeImages = 0;
                }
            },
            error: function (err) {
                _g.toast('恢复失败：' + JSON.stringify(err));
            }
        })
    };
    // 申请邀请码
    function applyInviteCode() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                type: 2
            },
            isSync: false,
            lock: false,
            url: '/inviteCode/applyCode',
            success: function (ret) {
                if (ret.code == 200) {
                    _g.toast('提交成功，请耐性等候客服人员审批');
                } else {
                    _g.toast('提交失败');
                }
            }
        });
    };
    // 获取历史访客
    function getVisitors() {
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: false,
            lock: false,
            url: '/relationship/accessList',
            success: function (ret) {
                if (ret.code == 200) {
                    user.visitors = ret.data.length || 0;
                }
            }
        });
    };
    // 获取个人评价
    function getMyComment() {
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: false,
            lock: false,
            url: '/relationship/evaluateList',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    var data = ret.data;
                    for (var index in data) {
                        for (var i = 0; i < user.currentList.length; i++) {
                            if (index == user.currentList[i].title) {
                                user.currentList[i].select = true;
                                user.currentList[i].number = data[index];
                            }
                        }
                    }
                }
            }
        });
    };
    // 打开百度地图
    function getLocation() {
        var isPass = _g.getLS('ISPASS');
        if(isPass == 0) return false;
        bMap.getLocation({
            accuracy: '100m',
            autoStop: true,
            filter: 1
        }, function (ret, err) {
            if (ret.status) {
                var lon = ret.lon;
                var lat = ret.lat;
                user.noGPS = false;
                bMap.getNameFromCoords({
                    lon: lon,
                    lat: lat
                }, function (ret, err) {
                    if (ret.status) {
                        var data = {
                            lon: ret.lon,
                            lat: ret.lat,
                            province: ret.province,
                            city: ret.city,
                            district: ret.district
                        }
                    }
                });
            } else {
                user.noGPS = true;
                // alert('定位不行：' + err.code);
            }
        });
    };
    // 更新用户位置
    function getUserLocation() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                lon: UserLocation.lon || 113.23,
                lat: UserLocation.lat || 23.16
            },
            isSync: false,
            lock: false,
            url: '/user/updateUserLocation',
            success: function (ret) {
                if (ret.code == 200) {
                    // 获取用户信息
                    getInfoData();
                }
            }
        });
    };

    function signerNum() {
        Http.ajax({
            data: {},
            method: 'get',
            isSync: false,
            lock: false,
            url: '/broadCast/uncheckedSignerNum/?userId=' + UserInfo.id,
            success: function (ret) {
                if (ret.code == 200) {
                    // 获取用户信息
                    user.signerNum = ret.uncheckedSignerNum
                }
            }
        });
    }

    function getCacheSize() {
        var size = api.getCacheSize({
            sync: true
        });
        user.size = Math.ceil(size / (1024 * 1024));
    }

    // getLocation();
    getInfoData();
    // 监听编辑按钮
    api && api.addEventListener({
        name: 'me-editInfo-edit',
    }, function (ret, err) {
        _g.openWin({
            header: {
                data: {
                    title: '个人认证',
                },
                template: 'common/header-base-V',
            },
            name: 'me-editInfo-win',
            url: '../me/editInfo.html',
            bounces: true,
            slidBackEnabled: false,
            pageParam: {

            }
        });
    });
    // 监听刷新页面
    api && api.addEventListener({
        name: 'work-index-changeInfo',
    }, function (ret, err) {
        getInfoData();
        getUnreadCount();
    });

    api && api.addEventListener({
        name: 'message-refresh'
    }, function (ret, err) {
        getUnreadCount();
    });

    // 监听刷新页面
    api && api.addEventListener({
        name: 'me-index-main',
    }, function (ret, err) {
        getInfoData();
        getUnreadCount();
    });
    // 监听刷新页面
    api && api.addEventListener({
        name: 'me-index-editInfo',
    }, function (ret, err) {
        getInfoData();
    });
    module.exports = {};

});