define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var wallet = new Vue({
        el: '#wallet',
        template: _g.getTemplate('me/wallet-main-V'),
        data: {
            wallet: UserInfo.wallet || 0,
            amount: '',
            account: '',
            nickname: '',
            memo: '',
            serviceCharge: 10,
        },
        created: function () {
        },
        methods: {
            onPostTap: function() {
                if(this.amount == 0 || this.amount == '') {
                    _g.toast('提现金额不能为空或者不能为零');
                    return
                }
                if(this.amount > this.wallet) {
                    _g.toast('提现金额不能大于账户余额');
                    return
                }
                if(this.account == '') {
                    _g.toast('支付宝账户不能为空');
                    return
                }
                if(this.nickname == '') {
                    _g.toast('账户名不能为空');
                    return
                }
                postData();
            }
        },
    });
    // 获取手续费接口
    function getData() {
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: false,
            url: '/relationship/followList',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    // var data = ret.data;
                    wallet.serviceCharge = ret.data;
                }
            }
        });
    };
    // 提交接口
    function postData() {
        Http.ajax({
            data: {
                userId: UserInfo.id,
                amount: wallet.amount,
                account: wallet.account,
                name: wallet.nickname,
                memo: wallet.memo
            },
            isSync: true,
            url: '/userWithdraw/save',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    wallet.wallet = Number(wallet.wallet - wallet.amount).toFixed(2);
                    _g.toast('提现成功，等待客服转账哦');
                    api && api.sendEvent({
                        name: 'me-index-main',
                    });
                }
            }
        });
    };
    // getData();

    module.exports = {};

});