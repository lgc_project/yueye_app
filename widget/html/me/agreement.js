define(function (require, exports, module) {
    var Http = require('U/http');
    var agreement = new Vue({
        el: '#agreement',
        template: _g.getTemplate('me/agreement-main-V'),
        data: {
            sex: 1
        },
        ready: function () {

        },
        created: function () {

        },
        methods: {
            onCloseTap: function() {
                api && api.closeWin();
            }
        },
    });
    module.exports = {};

});