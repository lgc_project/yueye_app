define(function (require, exports, module) {
    var UserInfo = _g.getLS('UserInfo');
    var Http = require('U/http');
    var favor = new Vue({
        el: '#favor',
        template: _g.getTemplate('me/favor-main-V'),
        data: {
            list: [{
                id: '',
                avatar: '../../image/me/avatar.png',
                username: 'Viky',
                city: '广州市',
                age: '21',
                occupation: '学生',
                distance: '105',
                identity: 0,
                status: false,
            }]
        },
        created: function () {
            this.list = []
        },
        methods: {},
    });
    // 获取个人资料接口
    function getData() {
        Http.ajax({
            data: {
                userId: UserInfo.id
            },
            isSync: false,
            url: '/relationship/followList',
            success: function (ret) {
                // alert(_g.j2s(ret));
                if (ret.code == 200) {
                    var data = ret.data;
                    favor.list = getDetail(data);
                }
            }
        });
    };

    function getDetail(result) {
        var list = result ? result : [];
        return _.map(list, function (item) {
            return {
                id: item.id || '',
                avatar: item.avatar || '',
                tap: item.relationship || '',
                nickname: item.nickname || '未命名',
                city: item.city || '',
                age: item.age || 0,
                charact: item.style || '',
                distance: item.distance || '',
                status: item.status || '',
                isfollow: item.isfollow || false,
            }
        });
    };


    // 下拉刷新
    _g.setPullDownRefresh(function () {
        getData();
    });
    getData();

    module.exports = {};

});