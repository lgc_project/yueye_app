define(function(require, exports, module) {
    
    var systemType = api.systemType; // 系统类型，字符串类型
    var bMap = api.require('bMap');
    var toNight = 1;
    var myDate = new Date();
    var nowYear = myDate.getFullYear();
    var nowMonth = myDate.getMonth();
    var nowDay = myDate.getDate();
    var nowDate = nowYear * 10000 + (nowMonth + 1) * 100 + nowDay;
    var limitDay = 20190129 // 苹果上线限制时间
    var IOS_CHECK = nowDate < limitDay ? 1 : 0;
    _g.setLS('IOS_CHECK', IOS_CHECK);

    api && api.setFullScreen({
        fullScreen: false
    });

    api && api.setStatusBarStyle({
        style: 'light'
    });

    if (window.APPMODE == 'dev' && !window.location.host) {
        api.clearCache();
        api.removeLaunchView();

        var path = _g.getLS('DEV_PATH');

        if (path) {
            api.confirm({
                title: '提示',
                msg: '使用历史记录地址',
                buttons: ['确定', '取消']
            }, function(ret, err) {
                if (ret.buttonIndex == 1) {
                    openDev(path);
                } else {
                    inputPath();
                }
            });
        } else {
            inputPath();
        }

        // var config = require('config');
        // var url = 'http://' + config.host + ':' + config.port;

        function openDev(path) {
            api.openWin({
                name: 'dev-win',
                url: path + '/index.html?isApp=1',
                bounces: false,
                slidBackEnabled: false,
                pageParam: { key: 'value' },
                animation: { type: 'none' }
            });
        }

        function inputPath() {
            api.prompt({
                buttons: ['确定', '取消']
            }, function(ret, err) {
                if (ret.buttonIndex == 1) {
                    path = 'http://' + ret.text;
                    _g.setLS('DEV_PATH', path);
                    openDev(path);
                } else {
                    api.closeWidget({
                        silent: true
                    });
                }
            });
        }

        return
    }

    // _g.rmLS('isFirstStart');
    // _g.rmLS('UserInfo');

    // 如果是第一次打开app, 启动引导页
    // if (!_g.getLS('isFirstStart')) {
    //     api && api.openWin({
    //         name: 'leading-index-win',
    //         url: './html/leading/index.html',
    //         bounces: false,
    //         slidBackEnabled: false
    //     });
    //     return
    // }

    var startTime = new Date().getTime();
    var LastTime = _g.getLS('LastTime');
    if (!LastTime) _g.setLS('LastTime', startTime);
    // 退出APP 6小时，清除用户信息，需重新登录
    if (startTime - LastTime > 7 * 24 * 60 * 60 * 1000 - 6) {
        _g.rmLS('UserInfo');
    }
    _g.setLS('LastTime', startTime);
    // 定时更新最后登录时间
    setInterval(function () {
        var nowTime = new Date().getTime();
        _g.setLS('LastTime', nowTime);
    }, 5000);
    

    api.removeLaunchView();

    if(systemType == 'ios') {
        getLocationPower();
    }
    ckeckLogin();

    function getLocationPower() {
        bMap.getLocationServices(function(ret, err) {
            if (ret.enable) {
                if(systemType == 'ios') {
                    bMap.initMapSDK(function(ret) {
                        if (ret.status) {
                            bMap.getLocation({
                                accuracy: '100m',
                                autoStop: true,
                                filter: 1
                            }, function(ret, err) {

                            });
                        }
                    });
                }
            }
        });
    };

    function ckeckLogin() {
        _g.rmLS('peopleCity');
        if(!_g.getLS('UserLocation')) {
            var data = {
                province: '广东省',
                city: '广州市',
            }
            _g.setLS('UserLocation', data);
        }
        if(_g.getLS('UserInfo')) {
            openMainPage();
        } else {
            openLoginPage();
        }
    };

    function openLoginPage() {
        api && api.setStatusBarStyle({
            style: 'dark'
        });
        if(IOS_CHECK == 1) {
            toNight = 1;
            _g.setLS('noGPS', toNight);
            setTimeout(function() {
                api && api.openWin({
                    name: 'main-night-win',
                    url: './html/main/night.html',
                    bounces: false,
                    slidBackEnabled: false,
                    bgColor: '#eee',
                    animation: { type: 'none' }
                });
            }, 0);
        } else {
            api && api.openWin({
                name: 'account-login-win',
                url: './html/account/login.html',
                bgColor: '#eee',
                bounces: false,
                slidBackEnabled: false,
                animation: { type: 'none' }
            });
        }
    };

    function openMainPage() {
        var UserInfo = _g.getLS('UserInfo');
        // 男性用户是否认证或者购买会员
        if((UserInfo.identity == 0 && UserInfo.sex == 0) || UserInfo.nickname == '') {
            api && api.openWin({
                name: 'account-login-win',
                url: './html/account/login.html',
                bgColor: '#eee',
                bounces: false,
                slidBackEnabled: false,
                animation: { type: 'none' }
            });
        } else {
            setTimeout(function() {
                if(systemType == 'ios') {
                    bMap.getLocationServices(function(ret, err) {
                        if (ret.enable) {
                            if(systemType == 'ios') {
                                bMap.initMapSDK(function(ret) {
                                    if (ret.status) {
                                        getLocation();
                                    }
                                });
                            } else {
                                getLocation();
                            }
        
                        } else {
                            toNight = 1;
                            _g.setLS('noGPS', toNight);
                            api && api.openWin({
                                name: 'main-night-win',
                                url: './html/main/night.html',
                                bounces: false,
                                slidBackEnabled: false,
                                bgColor: '#eee',
                                animation: { type: 'none' }
                            });
                        }
                    });
                } else {
                    // 安卓登录
                    toNight = 0;
                    _g.setLS('noGPS', toNight);
                    api && api.openWin({
                        name: 'main-index-win',
                        url: './html/main/index.html',
                        bounces: false,
                        slidBackEnabled: false,
                        bgColor: '#eee',
                        animation: { type: 'none' }
                    });
                }
            }, 0);
        }
    };
    
    function getLocation() {
        var UserInfo = _g.getLS('UserInfo');
        bMap.getLocation({
            accuracy: '100m',
            autoStop: true,
            filter: 1
        }, function(ret, err) {
            if (ret.status) {
                // alert(JSON.stringify(ret));
                var lon = ret.lon.toFixed(6);
                var lat = ret.lat.toFixed(6);
                setTimeout(function() {
                    bMap.getNameFromCoords({
                        lon: lon,
                        lat: lat
                    }, function(ret, err) {
                        // alert(JSON.stringify(ret));
                        if (ret.status) {
                            var data = {
                                lon: ret.lon,
                                lat: ret.lat,
                                province: ret.province,
                                city: ret.city,
                                district: ret.district
                            }
                            UserLocation = data;
                            _g.setLS('UserLocation', data);
                            if(UserLocation.lon > 73 && UserLocation.lon < 135 && UserLocation.lat > 4.25 && UserLocation.lat < 53.5) {
                                if(IOS_CHECK == 1) {
                                    toNight = 1;
                                    _g.setLS('noGPS', toNight);
                                    setTimeout(function() {
                                        api && api.openWin({
                                            name: 'main-night-win',
                                            url: './html/main/night.html',
                                            bounces: false,
                                            slidBackEnabled: false,
                                            bgColor: '#eee',
                                            animation: { type: 'none' }
                                        });
                                    }, 0);
                                } else if(UserInfo.phone == 18578790007) {
                                    toNight = 1;
                                    _g.setLS('noGPS', toNight);
                                    setTimeout(function() {
                                        api && api.openWin({
                                            name: 'main-night-win',
                                            url: './html/main/night.html',
                                            bounces: false,
                                            slidBackEnabled: false,
                                            bgColor: '#eee',
                                            animation: { type: 'none' }
                                        });
                                    }, 0);
                                } else {
                                    // 在中国境内
                                    toNight = 0;
                                    _g.setLS('noGPS', toNight);
                                    setTimeout(function() {
                                        api && api.openWin({
                                            name: 'main-index-win',
                                            url: './html/main/index.html',
                                            bounces: false,
                                            slidBackEnabled: false,
                                            bgColor: '#eee',
                                            animation: { type: 'none' }
                                        });
                                    }, 0);
                                }
                            } else {
                                // 不在中国境内
                                toNight = 1;
                                _g.setLS('noGPS', toNight);
                                setTimeout(function() {
                                    api && api.openWin({
                                        name: 'main-night-win',
                                        url: './html/main/night.html',
                                        bounces: false,
                                        slidBackEnabled: false,
                                        bgColor: '#eee',
                                        animation: { type: 'none' }
                                    });
                                }, 0);
                            }
                            // alert(JSON.stringify(ret));
                        }
                    });
                }, 0);
            } else {
                toNight = 1;
                _g.setLS('noGPS', toNight);
                setTimeout(function() {
                    api && api.openWin({
                        name: 'main-night-win',
                        url: './html/main/night.html',
                        bounces: false,
                        slidBackEnabled: false,
                        bgColor: '#eee',
                        animation: { type: 'none' }
                    });
                }, 0);
            }
        });
    };

    // function getLocation() {
    //     bMap.getLocation({
    //         accuracy: '100m',
    //         autoStop: true,
    //         filter: 1,
    //     }, function(ret, err) {
    //         if (ret.status) {
    //             // alert(JSON.stringify(ret));
    //             var lon = ret.lon;
    //             var lat = ret.lat;
    //             setTimeout(function() {
    //                 bMap.getNameFromCoords({
    //                     lon: lon,
    //                     lat: lat,
    //                 }, function(ret, err) {
    //                     // alert(JSON.stringify(ret));
    //                     if (ret.status) {
    //                         var data = {
    //                             lon: ret.lon,
    //                             lat: ret.lat,
    //                             province: ret.province,
    //                             city: ret.city,
    //                             district: ret.district,
    //                         }
    //                         _g.setLS('UserLocation', data);
    //                         api && api.openWin({
    //                             name: 'main-index-win',
    //                             url: './html/main/index.html',
    //                             bounces: false,
    //                             slidBackEnabled: false,
    //                             bgColor: '#eee',
    //                             animation: { type: 'none' }
    //                         });
    //                     }
    //                 });
    //             }, 0);
    //         } else {
    //             api && api.openWin({
    //                 name: 'main-night-win',
    //                 url: './html/main/night.html',
    //                 bounces: false,
    //                 slidBackEnabled: false,
    //                 bgColor: '#eee',
    //                 animation: { type: 'none' }
    //             });
    //         }
    //     });
    // }


    // api.addEventListener({
    //     name: 'shake'
    // }, function(ret, err) {
    //     if (window.APPMODE == 'pub') return;
    //     api.alert({
    //         title: '当前代码版本为:',
    //         msg: window.VERSION,
    //     }, function(ret, err) {

    //     });
    // });

    module.exports = {};

});
