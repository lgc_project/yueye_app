define(function(require, exports, module) {

    // baseWin头部操作事件

    function Func() {
        this['account-editInfo'] = {
            onTapRightBtn: function() {
                api.sendEvent({
                    name: 'account-editInfo-save',
                });
            }
        },
        this['me-qrcode'] = {
            onTapRightBtn: function() {
                api.sendEvent({
                    name: 'me-qrcode-save',
                });
            }
        },
        this['radio-index'] = {
            onTapRightBtn: function() {
                api.sendEvent({
                    name: 'radio-index-save',
                });
            }
        },
        this['other-index'] = {
            handleMore: function() {
                api.sendEvent({
                    name: 'other-index-more',
                });
            },
            onShareTap: function() {
                api.sendEvent({
                    name: 'other-index-share',
                });
            }
        }
    }

    Func.prototype = {
        get: function(page) {
            return this[page] || {}
        }
    };

    Func.prototype.constructor = Func;

    module.exports = new Func();

});
